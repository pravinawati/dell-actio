package com.properties.mapping;


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.utility.library.FileUtil;
import com.utility.library.Global;

/**
 * Contains method to load all project configuration files and all object repository property files.
 * @author raghavk
 *
 */
public class LoadProp {

	public FileInputStream input;
	public List<String> actionClassNameLst = new ArrayList<String>();
	public static Map<String, String> nbeproxibidprop = new HashMap<String, String>();
	public static Map<String, String> mnbeproxibidprop = new HashMap<String, String>();
	public static Map<String, String> dellprop = new HashMap<String, String>();
	public static Map<String, String> loginpageprop = new HashMap<String, String>();
	public static Map<String, String> homepageprop = new HashMap<String, String>();
	public static Map<String, String> domainsprop = new HashMap<String, String>();
	public static Map<String, String> adminsprop = new HashMap<String, String>();
	public static Map<String, String> enterpriseprop = new HashMap<String, String>();
	public static Map<String, String> dashboardprop = new HashMap<String, String>();
	public static Map<String, String> usergroupsprop = new HashMap<String, String>();
	public static Map<String, String> usersprop = new HashMap<String, String>();
	public static Map<String, String> endpointgroupsprop = new HashMap<String, String>();
	public static Map<String, String> endpointsprop = new HashMap<String, String>();
	public static Map<String, String> commitprop = new HashMap<String, String>();
	public static Map<String, String> loganalyzerprop = new HashMap<String, String>();
	public static Map<String, String> recoverdataprop = new HashMap<String, String>();
	public static Map<String, String> recoverendpointprop = new HashMap<String, String>();
	public static Map<String, String> licensemngmtprop = new HashMap<String, String>();
	public static Map<String, String> servicesmngmtprop = new HashMap<String, String>();
	public static Map<String, String> notificationsmngmtprop = new HashMap<String, String>();
	public static Map<String, String> externalusermngmtprop = new HashMap<String, String>();
	public static Map<String, String> managereportsprop = new HashMap<String, String>();
	public static Map<String, String> auditeventsprop = new HashMap<String, String>();
	public static Map<String, String> compliancereporterprop = new HashMap<String, String>();
	public static Map<String, String> policyprop = new HashMap<String, String>();
	public static Map<String, String> androidprop = new HashMap<String, String>();
	public static Map<String, String> androidbrowserprop = new HashMap<String, String>();
	public static Properties pro;  

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public LoadProp() {
		// load project properties 
		loadPropertiesFile();
		// load individual page properties
		nbeproxibidprop.putAll((Map)FileUtil.loadPropertyFile(Global.PROJECT_LOCATION + "src\\com\\object\\repository\\nbeproxibid.properties"));
		mnbeproxibidprop.putAll((Map)FileUtil.loadPropertyFile(Global.PROJECT_LOCATION + "src\\com\\object\\repository\\mnbeproxibid.properties"));
		dellprop.putAll((Map)FileUtil.loadPropertyFile(Global.PROJECT_LOCATION + "src\\com\\object\\repository\\dell.properties"));
		loginpageprop.putAll((Map)FileUtil.loadPropertyFile(Global.PROJECT_LOCATION + "src\\com\\object\\repository\\loginpage.properties"));
		homepageprop.putAll((Map)FileUtil.loadPropertyFile(Global.PROJECT_LOCATION + "src\\com\\object\\repository\\homepage.properties"));
		dashboardprop.putAll((Map)FileUtil.loadPropertyFile(Global.PROJECT_LOCATION + "src\\com\\object\\repository\\dashboardpage.properties"));
		enterpriseprop.putAll((Map)FileUtil.loadPropertyFile(Global.PROJECT_LOCATION + "src\\com\\object\\repository\\enterprisepage.properties"));
		domainsprop.putAll((Map)FileUtil.loadPropertyFile(Global.PROJECT_LOCATION + "src\\com\\object\\repository\\domainspage.properties"));
		usergroupsprop.putAll((Map)FileUtil.loadPropertyFile(Global.PROJECT_LOCATION + "src\\com\\object\\repository\\usergroupspage.properties"));
		usersprop.putAll((Map)FileUtil.loadPropertyFile(Global.PROJECT_LOCATION + "src\\com\\object\\repository\\userspage.properties"));
		endpointgroupsprop.putAll((Map)FileUtil.loadPropertyFile(Global.PROJECT_LOCATION + "src\\com\\object\\repository\\endpointgroupspage.properties"));
		endpointsprop.putAll((Map)FileUtil.loadPropertyFile(Global.PROJECT_LOCATION + "src\\com\\object\\repository\\endpointspage.properties"));
		adminsprop.putAll((Map)FileUtil.loadPropertyFile(Global.PROJECT_LOCATION + "src\\com\\object\\repository\\administratorspage.properties"));
		managereportsprop.putAll((Map)FileUtil.loadPropertyFile(Global.PROJECT_LOCATION + "src\\com\\object\\repository\\managereportspage.properties"));
		auditeventsprop.putAll((Map)FileUtil.loadPropertyFile(Global.PROJECT_LOCATION + "src\\com\\object\\repository\\auditeventspage.properties"));
		compliancereporterprop.putAll((Map)FileUtil.loadPropertyFile(Global.PROJECT_LOCATION + "src\\com\\object\\repository\\compliancereporterpage.properties"));
		commitprop.putAll((Map)FileUtil.loadPropertyFile(Global.PROJECT_LOCATION + "src\\com\\object\\repository\\commitpage.properties"));
		loganalyzerprop.putAll((Map)FileUtil.loadPropertyFile(Global.PROJECT_LOCATION + "src\\com\\object\\repository\\loganalyzerpage.properties"));
		recoverdataprop.putAll((Map)FileUtil.loadPropertyFile(Global.PROJECT_LOCATION + "src\\com\\object\\repository\\recoverdatapage.properties"));
		recoverendpointprop.putAll((Map)FileUtil.loadPropertyFile(Global.PROJECT_LOCATION + "src\\com\\object\\repository\\recoverendpointpage.properties"));
		licensemngmtprop.putAll((Map)FileUtil.loadPropertyFile(Global.PROJECT_LOCATION + "src\\com\\object\\repository\\licensemanagementpage.properties"));
		servicesmngmtprop.putAll((Map)FileUtil.loadPropertyFile(Global.PROJECT_LOCATION + "src\\com\\object\\repository\\servicesmanagementpage.properties"));
		externalusermngmtprop.putAll((Map)FileUtil.loadPropertyFile(Global.PROJECT_LOCATION + "src\\com\\object\\repository\\externalusermanagementpage.properties"));
		notificationsmngmtprop.putAll((Map)FileUtil.loadPropertyFile(Global.PROJECT_LOCATION + "src\\com\\object\\repository\\notificationsmanagementpage.properties"));
		policyprop.putAll((Map)FileUtil.loadPropertyFile(Global.PROJECT_LOCATION + "src\\com\\object\\repository\\policypage.properties"));
		androidprop.putAll((Map)FileUtil.loadPropertyFile(Global.PROJECT_LOCATION + "src\\com\\object\\repository\\androidtest.properties"));
		androidbrowserprop.putAll((Map)FileUtil.loadPropertyFile(Global.PROJECT_LOCATION + "src\\com\\object\\repository\\androidbrowsertest.properties"));
	}

	/**
	 * Method to load the project configuration file and then platform specific configuration files 
	 */
	public void loadPropertiesFile(){
		
		pro = new Properties();
		try {
			// load project configuration file
			input = new FileInputStream(Global.PROJECT_LOCATION +  "src\\com\\config\\environmentConfig.properties");
			pro.load(input);
			
			// load web configuration file
			if (!pro.getProperty("total_web_instance").trim().equalsIgnoreCase("") && !pro.getProperty("total_web_instance").trim().equalsIgnoreCase("0")){
				input = new FileInputStream(Global.PROJECT_LOCATION +  pro.getProperty("web_conf_file_path").trim());
				pro.load(input);
			}
			
			// load android configuration file
			if (!pro.getProperty("total_android_instance").trim().equalsIgnoreCase("") && !pro.getProperty("total_android_instance").trim().equalsIgnoreCase("0")){
				input = new FileInputStream(Global.PROJECT_LOCATION +  pro.getProperty("android_conf_file_path").trim());
				pro.load(input);
			}
			
			// load android browser configuration file0
			if (!pro.getProperty("total_android_browser_instance").trim().equalsIgnoreCase("") && !pro.getProperty("total_android_browser_instance").trim().equalsIgnoreCase("0")){
				input = new FileInputStream(Global.PROJECT_LOCATION +  pro.getProperty("android_browser_conf_file_path").trim());
				pro.load(input);
			}
			
			// load ios configuration file
			if (!pro.getProperty("total_ios_instance").trim().equalsIgnoreCase("") && !pro.getProperty("total_ios_instance").trim().equalsIgnoreCase("0")){
				input = new FileInputStream(Global.PROJECT_LOCATION +  pro.getProperty("ios_conf_file_path").trim());
				pro.load(input);
			}
			
			// load ios browser configuration file
			if (!pro.getProperty("total_ios_browser_nodes").trim().equalsIgnoreCase("") && !pro.getProperty("total_ios_browser_nodes").trim().equalsIgnoreCase("0")){
				input = new FileInputStream(Global.PROJECT_LOCATION +  pro.getProperty("ios_browser_conf_file_path").trim());
				pro.load(input);
			}
			
			// load ios browser configuration file
			if (!pro.getProperty("total_api_instance").trim().equalsIgnoreCase("") && !pro.getProperty("total_api_instance").trim().equalsIgnoreCase("0")){
				input = new FileInputStream(Global.PROJECT_LOCATION +  pro.getProperty("api_conf_file_path").trim());
				pro.load(input);
			}
			
		}catch (FileNotFoundException e) {
			e.printStackTrace();
		}catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Method to return the requested properties
	 * @param key property key
	 * @return properties value
	 */
	public static String getProperty(String key){
		return pro.getProperty(key).trim();
	}

}
