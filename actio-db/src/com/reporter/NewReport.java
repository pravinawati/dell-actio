package com.reporter;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileOutputStream;
import java.util.Dictionary;
import java.util.Hashtable;

	/**
	 * Class to generate the PDf report.
	 * Attach pie chart image to the report.
	 * @author raghavk
	 *
	 */
	public class NewReport {
		
		public static Dictionary<String, String> summarydetails  = new Hashtable<String, String>();;
		
		/**
		 * Create PDF summary report
		 * @param summaryPDF Dictionary for summary
		 * @param testResultDetails Dictionary for test result details
		 */
	    public void createPDFSummary(Dictionary<String, String> summaryPDF,Dictionary<String, String> testResultDetails)
	    {
	    	Document document = new Document();
	    	summarydetails = summaryPDF;

	    	try {
	            PdfWriter.getInstance(document, new FileOutputStream("test-output/Reports.pdf"));

	            document.open();
	            PdfPTable headingTbl = new PdfPTable(1); // 3 columns.
	            PdfPCell cellHeading = new PdfPCell(new Paragraph(""));
	            headingTbl.addCell(cellHeading);
	            document.add(headingTbl);
	            
	            PdfPTable table1 = new PdfPTable(1); // 3 columns.
//	            Image image = Image.getInstance("test-output/pie_Chart3D.png");
	            Image image = Image.getInstance("test-output/my_pie_chart.png");
	        	            
	            PdfPCell cel2 = new PdfPCell(image, true);
	            table1.addCell(cel2);
	            document.add(table1);
	            
	            PdfPTable table = new PdfPTable(5); // 3 columns.
	            float[] columnWidths = new float[] {35f, 20f, 18f, 18f, 18f};
	            table.setWidths(columnWidths);
	            
	            PdfPCell cell1 = new PdfPCell(new Paragraph("Suite Name"));
	            PdfPCell cell2 = new PdfPCell(new Paragraph("Total TCs"));
	            PdfPCell cell3 = new PdfPCell(new Paragraph("Passed"));
	            
	            cell3.setBackgroundColor(BaseColor.GREEN);
	            PdfPCell cell4 = new PdfPCell(new Paragraph("Failed"));
	            cell4.setBackgroundColor(BaseColor.RED);
	            PdfPCell cell5 = new PdfPCell(new Paragraph("Skipped"));
	            cell5.setBackgroundColor(BaseColor.BLUE);
	            
	            table.addCell(cell1);
	            table.addCell(cell2);
	            table.addCell(cell3);
	            table.addCell(cell4);
	            table.addCell(cell5);
	            
	            Font titleFont = new Font(Font.getFamily("HELVETICA"), 16, Font.BOLD);
	            
	            
	            PdfPTable summaryTbl = new PdfPTable(1); // 3 columns.
	            PdfPCell summarHeading = new PdfPCell(new Paragraph("Summary",titleFont));
	            summaryTbl.addCell(summarHeading);
	            document.add(summaryTbl);
	            
	            
	            if(summaryPDF.size()!=0)
	            {
	            	
	            	PdfPCell cell01 = new PdfPCell(new Paragraph(summaryPDF.get("c1")));
		            PdfPCell cell02 = new PdfPCell(new Paragraph(summaryPDF.get("c2")));
		            PdfPCell cell03 = new PdfPCell(new Paragraph(summaryPDF.get("c3")));
		            PdfPCell cell04 = new PdfPCell(new Paragraph(summaryPDF.get("c4")));
		            PdfPCell cell05 = new PdfPCell(new Paragraph(summaryPDF.get("c5")));

		            table.addCell(cell01);
		            table.addCell(cell02);
		            table.addCell(cell03);
		            table.addCell(cell04);
		            table.addCell(cell05);
	            }

	            document.add(table);
	            
	           
	            PdfPTable detailsTbl = new PdfPTable(1); // 3 columns.
	            PdfPCell detailHeading = new PdfPCell(new Paragraph("Failed Test Details",titleFont));
	            detailsTbl.addCell(detailHeading);
	            document.add(detailsTbl);
	            
	            
	            
	            
	            PdfPTable detailValueTbl = new PdfPTable(3); // 3 columns.
	            
	            float[] columnWidth = new float[] {18f, 50f, 25f};
	            detailValueTbl.setWidths(columnWidth);
	            
	            
	            PdfPCell celldet0 = new PdfPCell(new Paragraph("Sr No."));
	            PdfPCell celldet1 = new PdfPCell(new Paragraph("Test Name"));
	            PdfPCell celldet2 = new PdfPCell(new Paragraph("Test Result"));
	            
	            detailValueTbl.addCell(celldet0);
	            detailValueTbl.addCell(celldet1);
	            detailValueTbl.addCell(celldet2);
	            
	            document.add(detailValueTbl);
	            
	            if(testResultDetails.size()!=0)
	            {
	            	PdfPTable detailValueTblDyna = new PdfPTable(3); // 3 columns.
	            	PdfPCell celldetValueSrNo = null;
	            	PdfPCell celldetValueDynaTN = null;
	            	PdfPCell celldetValueDynaST = null;
	            	detailValueTblDyna.setWidths(columnWidth);
	            	
	            	int srNo =1;
	            	String testcaseName= null;
	            	String result =null;
	            	for (int i =1; i<testResultDetails.size(); i++)
	     	       	{
	            	testcaseName = testResultDetails.get("TN"+i);
	            	result = testResultDetails.get("ST"+i);
	            	
	            	if(testcaseName !=null){// && !result.equalsIgnoreCase("Pass")
	            	celldetValueSrNo  = new PdfPCell(new Paragraph(srNo+""));
	            	celldetValueDynaTN = new PdfPCell(new Paragraph(testcaseName));
	            	celldetValueDynaST = new PdfPCell(new Paragraph(result));
	            	
	            	detailValueTblDyna.addCell(celldetValueSrNo);
	            	detailValueTblDyna.addCell(celldetValueDynaTN);
	            	detailValueTblDyna.addCell(celldetValueDynaST);
					srNo ++;
	            	}
	               }
	            	document.add(detailValueTblDyna);
	            }
	
	            document.close();
	            
	        } catch(Exception e){
	        	e.printStackTrace();
	        }
	        
	    }
	    
	    /**
	     * Method to create summary for email sent on execution
	     * @return summary details dictionary
	     */
		public Dictionary<String, String> createSummaryForEmail() {
			return summarydetails;
		}
	    
	}


