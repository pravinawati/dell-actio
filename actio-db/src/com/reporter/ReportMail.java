package com.reporter;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Dictionary;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.properties.mapping.LoadProp;

/**
 *  Class to report/email the test result
 */
public class ReportMail {

	public static Dictionary<String, String> summaryemail = null;

	public ReportMail() {
		NewReport newReport = new NewReport();
		summaryemail = newReport.createSummaryForEmail();
	}

	/**
	 * Method to email test result
	 * 
	 * @throws IOException
	 */
	public void SendEmail() throws IOException {
		String to;
		String from;
		final String username;
		final String password;
		String project;
		String testsuit;

		try {
			String host = "smtp.gmail.com";
			String port = "587";
			to = LoadProp.getProperty("to"); // Recipient's email ID
			from = LoadProp.getProperty("from"); // Sender's email ID
			username = LoadProp.getProperty("username");
			password = LoadProp.getProperty("password");
			
			System.out.println(username);
			System.out.println(password);

			Properties properties = new Properties();
			properties.put("mail.smtp.host", host);
			properties.put("mail.smtp.port", port);
			properties.put("mail.smtp.auth", "true");
			properties.put("mail.smtp.starttls.enable", "true");
			properties.put("mail.user", username);
			properties.put("mail.password", password);

			// Get the Session object.
			Session session = Session.getInstance(properties,
					new javax.mail.Authenticator() {
						protected PasswordAuthentication getPasswordAuthentication() {
							return new PasswordAuthentication(username,
									password);
						}
					});

			// Create a default MimeMessage object.
			Message message = new MimeMessage(session);
			// Set From: header field of the header.
			message.setFrom(new InternetAddress(from));
			// Set To: header field of the header.
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));

			// Set Subject: header field
			project = LoadProp.getProperty("project_name");
			testsuit = LoadProp.getProperty("test_suite_name");

			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
			Date date = new Date();
			String date1 = dateFormat.format(date);

			message.setSubject(project + ": " + testsuit + " run summary report on" + " " + date1);

			// Create the message part

			BodyPart messageBodyPart = new MimeBodyPart();

			// Now set the actual message

			String htmlText = "<p>Hi,<br> <br>Following is the summary for "
					+ LoadProp.getProperty("project_name")
					+ " "
					+ LoadProp.getProperty("test_suite_name")
					+ " "
					+ "run on"
					+ " "
					+ dateFormat.format(date)
					+ "</p><table border=1 table id=1 style=\"width: 730px\"  ><caption><b>Summary</caption></b>"
					+ "<tr> <td><b> Suite Name  </b></td>"
					+ "<td><b> Total TCs </b></td>" + "<td><b> Passed</b></td>"
					+ "<td><b> Failed</b></td>" + "<td><b> Skipped </b></td>"
					/*+ "<td><b> Duration </b></td>" + "<td><b> Status </b></td>"*/
					+ "</tr><tr>";

			String html = "<p>Please find attached pdf report for more details.<br> <br><b>"
					+ LoadProp.getProperty("text")
					+ "</b><br> <br>Regards,</p>";
			String html1 = "<p>Team " + LoadProp.getProperty("project_name")
					+ "</p>";

			String abc = null;

			for (int i = 1; i <= summaryemail.size(); i++) {
				abc = summaryemail.get("c" + i);
				htmlText = htmlText + "<td><b> " + abc + "</b></td>";
			}

			htmlText = htmlText + "</tr></table></table></body>" + html + html1;

			messageBodyPart.setContent(htmlText, "text/html");
			MimeMultipart multipart = new MimeMultipart("related");
			multipart.addBodyPart(messageBodyPart);

			messageBodyPart = new MimeBodyPart();

			String file = "test-output//Reports.pdf";
			String filename = "Reports.pdf";

			DataSource source = new FileDataSource(file);

			messageBodyPart.setDataHandler(new DataHandler(source));

			messageBodyPart.setFileName(filename);

			multipart.addBodyPart(messageBodyPart);

			// Send the complete message parts

			message.setContent(multipart);

			// Send message

			Transport.send(message);

			System.out.println("Sent mail successfully....");

		} catch (Exception e) {
			e.printStackTrace();

		}
	}
}
