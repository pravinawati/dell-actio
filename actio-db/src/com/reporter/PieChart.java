package com.reporter;


import java.io.*;
import org.jfree.data.general.DefaultPieDataset; /* This class is required to define the data for our Pie Chart */
import org.jfree.chart.ChartFactory; /* This class creates the Pie Chart for us */
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot; /* To change pie chart colors */
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.chart.ChartUtils; /* To write the Chart as JPG image file */
import java.awt.Color; /* To specify the color coordinates for the pie chart */
public class PieChart {
	
	
	public void createPieChart(int passed, int failed, int skipped) throws IOException
	{
		/* A data range is required to create a Pie Chart in JFreeChart. So, we will begin with defining a data range using the code below */
        DefaultPieDataset myColoredPieChart = new DefaultPieDataset();
        /* Define Values for the Pie Chart - Passed, Failed, Skipped */
        myColoredPieChart.setValue("SKIPPED "+ skipped, skipped);
        myColoredPieChart.setValue("FAILED "+ failed , failed);
        myColoredPieChart.setValue("PASSED "+ passed,passed);
        
        /* With the dataset defined for Pie Chart, we can invoke a method in ChartFactory object to create Pie Chart and Return a JFreeChart object*/
        /* This method returns a JFreeChart object back to us */                
        JFreeChart myColoredChart=ChartFactory.createPieChart(" ",myColoredPieChart,true,true,false);                
        /* Once we have got the JFreeChart object, it is time to change the default colors for the Pie Chart */
        /* PiePlot class helps to change the colors for us, defined in org.jfree.chart.plot.PiePlot */
        /* getPlot method of JFreeChart class returns the PiePlot object back to us */                
        PiePlot ColorConfigurator = (PiePlot)myColoredChart.getPlot();
        /* We can now use setSectionPaint method to change the color of our chart */
        ColorConfigurator.setSectionPaint("SKIPPED" , new Color(160, 160, 255));
        ColorConfigurator.setSectionPaint("FAILED" , Color.RED);
        ColorConfigurator.setSectionPaint("PASSED", Color.GREEN);
        /* We will now write the chart as a JPEG file. TO do this we will use the ChartUtilities class. org.jfree.chart.ChartUtilities */                
        int width=640; /* Width of the image */
        int height=480; /* Height of the image */
        /* Note on Chart Quality Factor : java.lang.IllegalArgumentException: The 'quality' must be in the range 0.0f to 1.0f */
        // float quality=1; /* Quality factor */
        File PieChart=new File("test-output/my_pie_chart.png");
        /* Convert JFreeChart to JPEG File Using Code below */
        ChartUtils.saveChartAsPNG(PieChart, myColoredChart,width,height); 

	}
	
	
	/**
	 * Method to create 2D PieChart
	 * @param passed pass test count
	 * @param failed failed test count
	 * @param skipped skipped test count
	 * @throws IOException Exception
	 */
	public void create3DPieChart(int passed, int failed, int skipped) throws IOException
	{
		DefaultPieDataset dataset = new DefaultPieDataset( );             
		 
		  dataset.setValue( "Failed "  +  getPercentageCorrect(failed,passed+failed+skipped) + " %", failed);             
	      dataset.setValue( "Skipped " + getPercentageCorrect(skipped,passed+failed+skipped) + " %",skipped );             
	      dataset.setValue( "Passed " + getPercentageCorrect(passed,passed+failed+skipped) +" %" , passed );             
	     // dataset.setValue( "" , 0); 

	      JFreeChart chart = ChartFactory.createPieChart3D( 
	         "" ,  // chart title                   
	         dataset ,         // data 
	         true ,            // include legend                   
	         true, 
	         false);

	      final PiePlot3D plot = ( PiePlot3D ) chart.getPlot();             
	      plot.setStartAngle( 270 );             
	      plot.setForegroundAlpha( 0.60f );             
	      plot.setInteriorGap( 0.02 );             
	      int width = 640; /* Width of the image */             
	      int height = 480; /* Height of the image */                             
	      File pieChart3D = new File( "test-output/pie_Chart3D.png" );                           
	      ChartUtils.saveChartAsJPEG( pieChart3D , chart , width , height );   
	}
	
	/**
	 * Method to calculate the percentage
	 * @param n count of the criteria for which percentage is to be calculated
	 * @param total total test count
	 * @return return the %
	 */
	public  float getPercentageCorrect(int n, int total) {
	    float proportionCorrect = ((float) n) / ((float) total);
	    return proportionCorrect * 100;
	}
	
  }