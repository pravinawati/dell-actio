package com.reporter;

import java.io.IOException;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;

import org.testng.IReporter;
import org.testng.ISuite;
import org.testng.xml.XmlSuite;

import com.utility.library.Global;

/**
 * Class responsible to create PDF summary report on completion of execution.
 * @author raghavk
 *
 */
public class CustomReportListener implements IReporter{

	@Override
	public void generateReport(List<XmlSuite> xmlSuites, List<ISuite> suites, String outputDirectory) {

		Dictionary<String, String> summaryPDF = new Hashtable<String, String>();
		PieChart pichart = new PieChart(); 
		NewReport newRpt = new NewReport();
		
		// count of total PASSED test cases.
    	String pass = Integer.toString(Global.totalPassCount);
		// count of total FAILED test cases.
    	String fail = Integer.toString(Global.totalFailCount);
		// count of total SKIPPED test cases.
    	String skip = Integer.toString(Global.totalSkipCount);
		// count of total test cases.
    	String total = Integer.toString(Global.totalPassCount+Global.totalFailCount+Global.totalSkipCount);
		
    	try {
    		// create pie chart
			pichart.createPieChart(Global.totalPassCount, Global.totalFailCount, Global.totalSkipCount);
		} catch (IOException e) {
			e.printStackTrace();
		}
    	
    	// create PDf summary
    	for (ISuite suite : suites) {
				summaryPDF.put("c1", suite.getName());
				summaryPDF.put("c2", total);
				summaryPDF.put("c3", pass);
				summaryPDF.put("c4", fail);
				summaryPDF.put("c5", skip);
				newRpt.createPDFSummary(summaryPDF, Global.testResultDetails);
		}
	}
}
