package com.testsuiterunner;

import java.io.BufferedWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.UUID;
import java.util.Date;

import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestContext;
import org.testng.ITestNGMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.itextpdf.text.log.SysoLogger;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.utility.library.ExtentManager;
import com.utility.library.Global;
import com.utility.library.LoggerUtil;
import com.utility.library.Util;

public class RegressionXls {
	
	ExtentReports extent = ExtentManager.getInstance();
	RemoteWebDriver maindriver;
	BufferedWriter logger = null;
	XlsTestsMapping mapping = new XlsTestsMapping();
	ExtentTest test;
	
	@BeforeTest(alwaysRun = true)
	@Parameters({ "platformType", "platformName", "platformPort", "platformId", "platformVersion", "instanceNo"})
	public void setupBeforeSuite(String platformType, String platformName, String platformPort, String platformId, String platformVersion, String instanceNo, ITestContext context) throws MalformedURLException, InterruptedException, IOException
	{
		// Get count of test cases to be executed on Current Instance
		int currentInstanceTestCount = Util.getCurrentInstanceTestCount(Integer.parseInt(instanceNo), platformType);
		System.out.println(platformType + "_" + instanceNo + " : Total test cases =  " + currentInstanceTestCount);
		
		//Set invocation count as per currentInstanceTestCount
		ITestNGMethod currentTestNGMethod = null;
		for (ITestNGMethod testNGMethod : context.getAllTestMethods()) {
		      if (testNGMethod.getInstance() == this) {
		        currentTestNGMethod = testNGMethod;
		        System.out.println(currentTestNGMethod.getCurrentInvocationCount());
		        break;
		      }
		    }
		    currentTestNGMethod.setInvocationCount(currentInstanceTestCount);
		    System.out.println(currentInstanceTestCount);
		    
		 //Launch driver based on platform-type
		try{
			switch(platformType){
			case "iOS":
				//get iOS driver
				maindriver = (RemoteWebDriver)Ignitor.getIOSAppiumDriver(platformName, platformPort, platformId, platformVersion);
				break;
			case "Web":
				if (platformName.equalsIgnoreCase(Global.CHROME_BROWSER)) {
					//get WebDriver (CHROME)
					maindriver = (RemoteWebDriver) Ignitor.getChromeDriver(platformPort);
					break;
				} else if (platformName.equalsIgnoreCase(Global.FIREFOX_BROWSER)) {
					//get WebDriver(FIREFOX)
					maindriver = (RemoteWebDriver) Ignitor.getFireFoxDriver(platformPort);
					break;
				} else if  (platformName.equalsIgnoreCase(Global.IE_BROWSER)) {
					//get WebDriver (IE)
					maindriver = (RemoteWebDriver) Ignitor.getIEDriver(platformPort);
					break;
				}
			case "Android":
				//get AppiumDriver (Android)
				maindriver = (RemoteWebDriver)Ignitor.getAndroidAppiumDriver(platformName, platformPort, platformId, platformVersion);
				break;
			case "AndroidBrowser":
				//get AppiumDriver (Android)
			    maindriver = (RemoteWebDriver)Ignitor.getAndroidWebDriver(platformName, platformPort, platformId, platformVersion);
			    break;	
			    // get iOS WebDriver
			case "iOSBrowser":
				//get iOS driver
			    maindriver = (RemoteWebDriver)Ignitor.getIOSWebDriver(platformName, platformPort, platformId, platformVersion);
			    break;
			case "API":
				// Run tests on API. No driver required.
				System.out.println("Running API Tests");
				break;
			default:
				System.out.println("No such Driver found. Please correct the conf.properties file!!!");
				break;
		}
		}catch(Exception e){
			System.out.println("Exception in setupBeforeSuite Method");
			System.out.println(e.getMessage());
		}
		
		// Get logger and log file name 
		logger = LoggerUtil.generateLogFile("RegressionXls");
		Global.filename = LoggerUtil.getLogFile();
		Global.runid = instanceNo+"-"+UUID.randomUUID();
		System.out.println(Global.runid);
	}
	
	/*@BeforeMethod(alwaysRun = true)
	public void beforeTestcase() throws IOException {
		//System.out.println("Time-1: "+Util.getTimestamp().toString());
		//Global.startTestExecutionTime = null;
		Global.startTestExecutionTime = Util.getTimestamp();
		
		try{
			String startTime = test.getTest().getStartedTime().toString();
			System.out.println(startTime);
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}*/
	/*@Parameters({"instanceNo"})
	@BeforeMethod(firstTimeOnly = true)
	public void beforeTestcase(int instanceNo) throws IOException {
		if(instanceNo == 1){
			System.out.println("Instance Number ::: "+instanceNo);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}*/
	
	@Parameters({ "platformName", "instanceNo", "platformType"})
	@Test(singleThreaded = true, invocationCount = 1)
	public void ExcelTest(String platformName, int instanceNo, String platformType) throws Exception{
		try{
			
			//map and execute function depending on the execution flag
			if(Global.continueExecutionFlag){
				test = mapping.mapnExeFunctions(maindriver,logger,extent,platformName, instanceNo,platformType);
			}
		}catch(Exception e){
			System.out.println("exceptionn ***************");
			if(e.getMessage() !=null) {
				System.out.println(e.getMessage());
			}
		}
	}
	
	/*@Parameters({"platformName", "platformType"})
	@AfterMethod(lastTimeOnly = true)
	public void afterTestcase(String platformName, String platformType) throws IOException, SQLException {
		Global.endTestExecutionTime = Util.getTimestamp();
		long date_diff = Util.timediff(Global.startTestExecutionTime, Global.endTestExecutionTime);
		String timeDuration = String.valueOf(date_diff);
		String Date = java.time.LocalDate.now().toString();
		//System.out.println(Global.testCounter);
		//Global.timeDetails.put("EndTime"+ Global.testCounter, Util.getTimestamp());
		//Global.timeDetails.put("Start"+Global.testCounter, Global.startTestExecutionTime);
		//Global.timeDetails.put("End"+Global.testCounter, Global.endTestExecutionTime);
		//test.log(LogStatus.INFO, Global.currentTestCaseName+" - "+platformType+" - "+platformName);
		//String executionResult = Global.testResultDetails.get("ST"+ Global.testCounter);
		//String testcasename = Global.testResultDetails.get("TN"+ Global.testCounter);
		//test.log(LogStatus.INFO, "Updating history for == "+testcasename+" == "+Global.currentTestCaseName);
		//String executionResult = Global.testExecutionResult;
		//String testcasename = Global.currentTestCaseName;
		//Global.stmt.executeUpdate("Insert Into HISTORY Values ('"+Global.runid+"', '"+Date+"', '"+Global.testid+"', '"+Global.testSuite+"', '"+Global.releaseCycle+"', '"+Global.buildVersion+"', '"+Global.testExecutionResult+"', '"+Global.startTestExecutionTime+"', '"+Global.endTestExecutionTime+"', '"+timeDuration+"')");
		//Global.stmt.executeUpdate("Insert Into HISTORY Values ('"+Global.runid+"', '"+Date+"', '"+testcasename+"', '"+Global.testSuite+"', '"+Global.releaseCycle+"', '"+Global.buildVersion+"', '"+executionResult+"', '"+Global.startTestExecutionTime+"', '"+Global.endTestExecutionTime+"', '"+timeDuration+"')");
		
	}*/
	
	@AfterSuite
	public void setupAfterSuite() throws IOException, SQLException {
		//Global.timeDetails.put("EndTime"+ Global.testCounter, Util.getTimestamp());
		//Global.testidHistory.put("EndTime"+ Global.testCounter, Util.getTimestamp().toString());
		//Global.timeDetails.put("EndTime"+ Global.testCounter, Util.getTimestamp());
		/*System.out.println(Global.testResultDetails.elements());
		System.out.println(Global.testResultDetails);
		System.out.println(Global.testResultDetails.size());
		System.out.println(Global.testResultDetails.keys());
		System.out.println(Global.testResultDetails.toString());
		System.out.println(Global.testidHistory);
		System.out.println(Global.testidHistory.size());*/
		/*System.out.println(Global.timeDetails);
		System.out.println(Global.timeDetails.size());*/
		/*String Date = java.time.LocalDate.now().toString();
		long date_diff = Util.timediff(Global.startTestExecutionTime, Global.endTestExecutionTime);
		String timeDuration = String.valueOf(date_diff);
		int i = 1;
		while(i >= 1){
			if(Global.testResultDetails.get("TN"+i)!= null){
				Global.stmt.executeUpdate("Insert Into HISTORY Values ('"+Global.runid+"', '"+Date+"', '"+Global.testResultDetails.get("TN"+i)+"', '"+Global.testidHistory.get("TestSuite"+i)+"', '"+Global.testidHistory.get("ReleaseCycle"+i)+"', '"+Global.testidHistory.get("BuildNumber"+i)+"', '"+Global.testResultDetails.get("ST"+i)+"', '"+Global.testidHistory.get("StartTime"+i)+"', '"+Global.testidHistory.get("EndTime"+i)+"', '"+Date+"')");
			}else{
				break;
			}
			i++;
		}*/
		//System.out.println(test.getEndedTime().toString());
		/*String Date = java.time.LocalDate.now().toString();
		int i = 1;
		while(i >= 1){
			if(Global.testResultDetails.get("TN"+i)!= null){
				long date_diff = Util.timediff(Global.timeDetails.get("StartTime"+i), Global.timeDetails.get("EndTime"+i));
				String diff = String.valueOf(date_diff);
				String startTime = Global.timeDetails.get("StartTime"+i).toString();
				String endTime = Global.timeDetails.get("EndTime"+i).toString();
				System.out.println("Start Time = "+startTime);
				System.out.println("End Time = "+endTime);
				System.out.println("Time Difference = "+diff);
				//Global.stmt.executeUpdate("Insert Into HISTORY Values ('"+Global.runid+"', '"+Date+"', '"+Global.testResultDetails.get("TN"+i)+"', '"+Global.testidHistory.get("TestSuite"+i)+"', '"+Global.testidHistory.get("ReleaseCycle"+i)+"', '"+Global.testidHistory.get("BuildNumber"+i)+"', '"+Global.testResultDetails.get("ST"+i)+"', '"+Global.testidHistory.get("StartTime"+i)+"', '"+Global.testidHistory.get("EndTime"+i)+"', '"+Date+"')");
				Global.stmt.executeUpdate("Insert Into HISTORY Values ('"+Global.runid+"', '"+Date+"', '"+Global.testResultDetails.get("TN"+i)+"', '"+Global.testidHistory.get("TestSuite"+i)+"', '"+Global.testidHistory.get("ReleaseCycle"+i)+"', '"+Global.testidHistory.get("BuildNumber"+i)+"', '"+Global.testResultDetails.get("ST"+i)+"', '"+startTime+"', '"+endTime+"', '"+diff+"')");
			}else{
				break;
			}
			i++;
		}*/

		//quite driver and safely end current session
		if(maindriver != null){
			maindriver.quit();
		}
		//close logger BufferedWriter
		logger.close();
		
		//close db connection
		Global.db.close();
		
		// end the test and flush the extent instant to generate the report
		if(extent!=null) {
			extent.endTest(test);
			extent.flush();
		}
	}

}
