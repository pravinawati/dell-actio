package com.testsuiterunner;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.nio.file.NoSuchFileException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.SkipException;

import com.properties.mapping.LoadProp;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.utility.library.Global;
import com.utility.library.LoggerUtil;
import com.utility.library.Util;

public class XlsTestsMapping{
	ExtentTest test;
	

	/**
	 * Load parameter dictionary
	 * @param param_rowIt row iterator
	 * @return Dictionary of parameters
	 * @throws SQLException 
	 */
	public Dictionary<String, String> loadParameterDictionary() throws SQLException{
		Dictionary<String, String> paramDict = new Hashtable<String, String>();
		String testId = null;
		String param_key = null;
		String param_value = null;
		
		String readTD = "select * from TESTDATA"; 
		Connection db = Util.getDBConnection();
		Statement stmt = db.createStatement(
            ResultSet.TYPE_SCROLL_INSENSITIVE, //or ResultSet.TYPE_FORWARD_ONLY
            ResultSet.CONCUR_READ_ONLY);
		ResultSet rs = stmt.executeQuery(readTD);

		//Read step_parameters sheet
		while(rs.next()){
			String tcid = rs.getString(1);
			if(tcid != null)
				if(!tcid.equals(""))
					testId = rs.getString(1).replace(" ", "_");
				
			param_key = (rs.getString(2)==null) ? "" : rs.getString(2).toLowerCase().replace(" ", "_");
			param_value = (rs.getString(3)==null) ? "" : rs.getString(3);
			String key = testId+"."+param_key;

			paramDict.put(key, param_value);
		}
		return paramDict;
	}

	/**
	 * Method to find and invoke the action method
	 * @param driver Driver instance
	 * @param logger Logger instance
	 * @param xlsStep Action step name to execute
	 * @param paramDict parameter dictionary
	 * @param test Extent test instance
	 * @throws Exception exception
	 */
	public void runSteps(RemoteWebDriver driver, BufferedWriter logger, String xlsStep, Dictionary<String, String> paramDict, String testId, ExtentTest test) throws Exception{    
	
		// get all class names from the directory
		String directoryName = Global.PROJECT_LOCATION + LoadProp.getProperty("action_folder_path");
		Iterator<String> classList = Util.listFilesAndFolders(directoryName).iterator();
		// flag to be set to true when method is found
		boolean exitWhenMethodFound = false;
	
		// iterate through the list to find the selected method in the class
				
		while(classList.hasNext()){
			String clName = classList.next().toString();
			// append the page path to the class
			String strclassName = LoadProp.getProperty("action_package")+"."+clName;

			// get the class
			Class<?> cls = Class.forName(strclassName);
			// get all methods from the selected class. Iterate to match with the current action step
			Method[] allMethods = cls.getDeclaredMethods();
			for (Method m : allMethods) {
				String mname = m.getName();
				// if found, invoke the method based on it parameter count.
				if (mname.equalsIgnoreCase(xlsStep)){
				    Object obj1 = cls.newInstance();
					int mParameterCount = m.getParameterCount();
					if(mParameterCount == 5) {
						m.invoke(obj1, driver, logger, paramDict, testId, test);
					} else if( mParameterCount == 4){
						m.invoke(obj1, driver, paramDict, testId, test);
					}else if( mParameterCount == 3){
						 m.invoke(obj1, paramDict, testId, test);
					}
					exitWhenMethodFound=true;
					Global.testresult = true;
					break;
				}
			}
			// break out of the loop if method is found
			if(exitWhenMethodFound){
				break;
			}
		}
		// Throw exception if the action method is not found.
		// TODO : know issue if the test is quit due to this then the next test also fails as driver is still on some page other than the login/landing screen.
		if(!exitWhenMethodFound){
			Assert.fail("Action method " + xlsStep + " is not present. Correct the Action in the testcase");
		}
	}

	/**
	 * Method to RUN Tests
	 * @param driver Driver instance
	 * @param logger logger instance 
	 * @param extent Extent test instance
	 * @throws Exception exception
	 */
	 
	public void runTests(RemoteWebDriver driver, BufferedWriter logger, ExtentReports extent, String deviceName, int instanceNo, String platformType) throws Exception{

		String testSuite, module, priority, testData, testCaseName = null, xlsStep = null, IsEnabled, testId= null;
		Dictionary<String, String> paramDict = null;
		int counter;
		
		String readTC = "select * from TESTCASE WHERE PLATFORM = '"+platformType+"'";
		Global.stmt = Global.db.createStatement(
            ResultSet.TYPE_SCROLL_INSENSITIVE, //or ResultSet.TYPE_FORWARD_ONLY
            ResultSet.CONCUR_READ_ONLY);
		ResultSet rs = Global.stmt.executeQuery(readTC);

		int testExecutionAt = Integer.parseInt(Global.platformTestBag.get("testExecutionAt"+ platformType+instanceNo));

		while(rs.next()){
			rs.absolute(testExecutionAt);

			IsEnabled = rs.getString(1);
			testCaseName = rs.getString(5);
			LoggerUtil.print(logger,"====================================================X=====================================================");
			LoggerUtil.logInfo(logger, "# Started running tests: \""+testCaseName+"\"... on : " + deviceName);
			test = extent.startTest(deviceName + " - " + testCaseName);
			test.log(LogStatus.INFO, "# Started running tests : ===> " + testCaseName.toUpperCase());
								
			// increment the test execution at value and update the platform bag
			testExecutionAt++;
			Global.platformTestBag.put("testExecutionAt"+ platformType+instanceNo, Integer.toString(testExecutionAt));
								
			// Set current test case name , increment the test counter and test pass counter
			// test pass counter is incremented by default. It will set decrement if the test eventually is skipped/failed
			Global.currentTestCaseName = testCaseName;
			counter = Global.testCounter;
			System.out.println("Global Counter = "+Global.counter);
			Global.testCounter++;
			Global.totalPassCount++;	      
			Global.testidHistory.put("TestID"+ Global.testCounter, Global.currentTestCaseName);
			Global.testidHistory.put("TestSuite"+ Global.testCounter, rs.getString(2));
			Global.testidHistory.put("ReleaseCycle"+ Global.testCounter, rs.getString(9));
			Global.testidHistory.put("BuildNumber"+ Global.testCounter, rs.getString(10));
			
			int endCounter = Global.testCounter - 1;
			Global.timeDetails.put("StartTime"+ Global.testCounter, Util.getTimestamp());
			//Global.timeDetails.put("EndTime"+ endCounter, Global.timeDetails.get("StartTime"+ Global.testCounter));
			
			//Global.timeDetails.put("StartTime"+ Global.testCounter, Util.getTimestamp());
			/*if(counter>0){
				Global.timeDetails.put("EndTime"+ counter, Global.timeDetails.get("StartTime"+ Global.testCounter));
			}*/
								
			// add current test name and status to global test result dictionary
			Global.testResultDetails.put("TN"+ Global.testCounter, Global.currentTestCaseName );
			Global.testResultDetails.put("ST"+ Global.testCounter, "Pass" );
							
			LoggerUtil.logInfo(logger, "# Is Enabled: \""+IsEnabled+"\"...");
			LoggerUtil.logSummary("INFO: # Is Enabled: \""+IsEnabled+"\"...");
			test.log(LogStatus.INFO, "# Is Enabled : "+ IsEnabled.toUpperCase());
							
			// Skip test if IS ENABLE is set to 'N'
			if(IsEnabled.equalsIgnoreCase("N")){	
				// increment the skip counter and decrement the total pass counter
				Global.totalSkipCount++;
				Global.totalPassCount--;
								
				// add current test name and status to global test result dictionary
				Global.testResultDetails.put("TN"+ Global.testCounter, Global.currentTestCaseName );
				Global.testResultDetails.put("ST"+ Global.testCounter, "Skip" );
				//Global.timeDetails.put("EndTime"+ Global.testCounter, Util.getTimestamp());
				Global.testresult = false;
				test.log(LogStatus.SKIP, "Skipping the test as Is Enabled is NO");
				throw new SkipException("Skipping the test as Is Enabled is NO");
			}

			// check the Release Cycle column
			String releaseCycle = rs.getString(9);
			Global.releaseCycle = releaseCycle;
			LoggerUtil.logInfo(logger, "# Running tests for Release Cycle: \""+Global.releaseCycle+"\"...");
			LoggerUtil.logSummary("INFO: # Running tests for Release Cycle: \""+Global.releaseCycle+"\"...");
			test.log(LogStatus.INFO, "# Running tests for Release Cycle : ===> " +Global.releaseCycle.toUpperCase());
						
			// check the Build version column
			String buildVersion = rs.getString(10);
			Global.buildVersion = buildVersion;
			LoggerUtil.logInfo(logger, "# Running tests on Build: \""+Global.buildVersion+"\"...");
			LoggerUtil.logSummary("INFO: # Running tests on Build: \""+Global.buildVersion+"\"...");
			test.log(LogStatus.INFO, "# Running tests on Build : " + Global.buildVersion.toUpperCase());
					
			// check the TEST SUITE column
			testSuite = rs.getString(2);
			Global.testSuite = testSuite;
			LoggerUtil.logInfo(logger, "# Running tests in Test Suite: \""+Global.testSuite+"\"...");
			LoggerUtil.logSummary("INFO: # Running tests in Test Suite: \""+Global.testSuite+"\"...");
			test.log(LogStatus.INFO, "# Running tests in Test Suite : ===> " +Global.testSuite.toUpperCase());
					
			// check the MODULE column
			module = rs.getString(3);
			LoggerUtil.logInfo(logger, "# Running tests in Module: \""+module+"\"...");
			LoggerUtil.logSummary("INFO: # Running tests in Module: \""+module+"\"...");
			test.log(LogStatus.INFO, "# Running tests in Module : " + module.toUpperCase());
						
			// check the PRIORITY column
			priority = rs.getString(4).toString();
			LoggerUtil.logInfo(logger, "# Running tests having Priority: \""+priority+"\"...");
			LoggerUtil.logSummary("INFO: Running tests having Priority: \""+priority+"\"...");
			test.log(LogStatus.INFO, " Running tests having Priority : ===>  " + priority.toUpperCase());
						
			// check the testcase id column
			testId = rs.getString(5).toString();
			LoggerUtil.logInfo(logger, "# Running tests having Testcase ID: \""+testId+"\"...");
			LoggerUtil.logSummary("INFO: Running tests having Testcase ID: \""+testId+"\"...");
			test.log(LogStatus.INFO, " Running tests having Testcase ID : ===>  " + testId.toUpperCase());
								
			// Check the name of TEST DATA File
			testData = rs.getString(7); 
			LoggerUtil.logInfo(logger, "# Running for Test Data: \""+testData+"\"...");
			LoggerUtil.logSummary("INFO: # Running for Test Data: \""+testData+"\"...");
			test.log(LogStatus.INFO, "# Running for Test Data : " + testData.toUpperCase());

			//load dictionary
			paramDict = loadParameterDictionary();
						
			// get TEST STEP to execute 
			String tcid = rs.getString(5);
			Global.tcid = tcid;
			String readTST = "select * from TESTSTEP WHERE TEST_ID = '"+Global.tcid+"'"+" ORDER BY Test_Step_Number";
			//Connection db1 = Util.getDBConnection();
			Statement stmtm = Global.db.createStatement(
			ResultSet.TYPE_SCROLL_INSENSITIVE, //or ResultSet.TYPE_FORWARD_ONLY
			ResultSet.CONCUR_READ_ONLY);
			ResultSet rst = stmtm.executeQuery(readTST);
			while(rst.next()){
				xlsStep = rst.getString(4);
				LoggerUtil.logInfo(logger, "# Running step Number: \""+rst.getString(2)+" == "+xlsStep+"\"...");
				test.log(LogStatus.INFO, "# ===> Running step Number : " +rst.getString(2)+" == "+ xlsStep.toUpperCase());
				runSteps(driver, logger, xlsStep, paramDict,testId, test);
				System.out.println("@@@ teststep "+xlsStep+" ran @@@");
			}
			test.log(LogStatus.INFO, "*** testcase "+testCaseName+" ran ***");
			//Global.timeDetails.put("EndTime"+ Global.testCounter, Util.getTimestamp());
			break;
		}
	}

	/**
	 * Method to map the action steps and execute
	 * @param driver driver instance
	 * @param logger logger instance
	 * @param extent extent Report instance
	 * @return ExtentTest extent test instance
	 * @throws Exception exception
	 */
	public ExtentTest mapnExeFunctions(RemoteWebDriver driver, BufferedWriter logger, ExtentReports extent, String deviceName, int instanceNo, String platformtype) throws Exception{

		try{
			runTests(driver, logger, extent, deviceName, instanceNo, platformtype);
		} catch(SkipException e){
			Global.continueExecutionFlag = true;
			LoggerUtil.print(logger, "------------------------------------------------------------------------------------------------------");
			LoggerUtil.logError(logger, "-- TEST Skipped!! --");
			System.out.println("Skipped");
			LoggerUtil.logError(logger, e.getMessage());
		} catch(Exception|AssertionError  e){
			//e.printStackTrace();
			// increment the total fail counter and decrement the pass counter.
			Global.totalFailCount++;
			Global.totalPassCount--;
			// add current test name and status to global test result dictionary
			Global.testResultDetails.put("TN"+ Global.testCounter,Global.currentTestCaseName );
			Global.testResultDetails.put("ST"+ Global.testCounter, "Fail" );
			//Global.timeDetails.put("EndTime"+ Global.testCounter, Util.getTimestamp());
			Global.continueExecutionFlag = true;
			Global.testresult = false;
			LoggerUtil.print(logger, "------------------------------------------------------------------------------------------------------");
			LoggerUtil.logError(logger, "-- TEST FAILED! --");
			Global.testExecutionResult = "FAIL";

			if(e.getMessage() !=null) {
				LoggerUtil.logError(logger, e.getMessage());
				test.log(LogStatus.FAIL,e.getMessage());
				//test.log(LogStatus.FATAL, e);
			}
			if(e.getCause() !=null) {
				LoggerUtil.logError(logger, e.getCause().getMessage());
				test.log(LogStatus.FAIL,e.getCause().getMessage());
				//test.log(LogStatus.FATAL, e);
			}
			//test.log(LogStatus.FATAL, e);
			Assert.fail();
		}
		
		//Log test result
		if(Global.testresult){
			Global.continueExecutionFlag = true;
			LoggerUtil.print(logger, "------------------------------------------------------------------------------------------------------");
			LoggerUtil.logInfo(logger, "-- TEST PASSED! --");
		}
		return test;
	}

}