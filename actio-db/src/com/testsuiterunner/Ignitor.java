package com.testsuiterunner;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.properties.mapping.LoadProp;
import com.utility.library.Global;

/**
 * This class will responsible for launching desired driver 
 */
public class Ignitor {
	private static AppiumDriver<WebElement> appiumdriver;
	private static WebDriver firefoxdriver;
	private static WebDriver chromedriver;
	private static WebDriver iedriver;
	private static WebDriver androidWebDriver;
	private static WebDriver iOSWebDriver;
	private Ignitor() throws IOException{

	}
	
	/**
	 * Method to launch iOS driver
	 * @param platformname Platform Name
	 * @param platformport Platform Port
	 * @param platformid Platform ID
	 * @param platformversion Platform Version
	 * @return Appium Driver
	 * @throws Exception exception
	 */
	public static AppiumDriver<WebElement> getIOSAppiumDriver(String platformName, String platformPort, String platformId, String platformVersion) throws Exception {
		if (appiumdriver == null){
			//Assemble desired capabilities (JSON)
			File app = new File(LoadProp.getProperty("ios_app"));
			DesiredCapabilities capabilities = new DesiredCapabilities();
			capabilities.setCapability("appium-version", LoadProp.getProperty("appium-version"));
			capabilities.setCapability("platformName", platformName);
			capabilities.setCapability("platformVersion", platformVersion);
			capabilities.setCapability("deviceName", platformId);
			capabilities.setCapability("app", app);
			
			//Create new IosDriver
			appiumdriver = new IOSDriver<WebElement>(new URL("http://127.0.0.1:"+ platformPort +"/wd/hub"), capabilities);
			appiumdriver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		}
		return appiumdriver;
	}
	
	/**
	 * Method to launch Appium driver on Android device 
	 * @param platformname Platform Name
	 * @param platformport Platform Port
	 * @param platformid Platform ID
	 * @param platformversion Platform Version
	 * @return AppiumDriver
	 * @throws Exception exception
	 */
	public static AppiumDriver<WebElement> getAndroidAppiumDriver(String platformName, String platformPort, String platformId, String platformVersion) throws Exception {
		if (appiumdriver == null){
			
			//Assemble desired capabilities (JSON)
			File app = new File(LoadProp.getProperty("android_app"));
			DesiredCapabilities capabilities = new DesiredCapabilities();
			capabilities.setCapability("appium-version", LoadProp.getProperty("appium-version"));
			capabilities.setCapability("platformName", platformName);
			capabilities.setCapability("platformVersion", platformVersion);
			capabilities.setCapability("deviceName", platformId);
			capabilities.setCapability("app", app);
			//capabilities.setCapability("app", app.getAbsolutePath());
			
			//Create new AndroidDriver
			appiumdriver = new AndroidDriver<WebElement>(new URL("http://127.0.0.1:"+ platformPort +"/wd/hub"), capabilities);
			appiumdriver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		}
		return appiumdriver;
	}
	
	/**
	 * Launch Android Chrome Driver
	 * @param platformname Platform Name
	 * @param platformport Platform Port
	 * @param platformid Platform ID 
	 * @param platformversion Platform version
	 * @return WebDriver 
	 * @throws MalformedURLException
	 */
	public static WebDriver getAndroidWebDriver(String platformName, String platformPort, String platformId, String platformVersion) throws Exception{
		 
		  if(androidWebDriver == null){
		   DesiredCapabilities  capabilities = DesiredCapabilities.android();
		   capabilities.setCapability("platformName","Android");
		   capabilities.setCapability("deviceName",platformId);
		   capabilities.setCapability("browserName", "chrome");
		   capabilities.setCapability("appium-version", LoadProp.getProperty("appium-version"));
		   capabilities.setCapability("VERSION",platformVersion);
		   androidWebDriver = new RemoteWebDriver(new URL("http://127.0.0.1:" + platformPort +"/wd/hub"), capabilities);
		  }
		  return androidWebDriver;
		 }
	
	/**
	 * Launch firefox driver
	 * @param platformPort platform port
	 * @return WebDriver instance
	 * @throws Exception exception
	 */
	public static WebDriver getFireFoxDriver(String platformPort) throws Exception {
		
		DesiredCapabilities capability = new DesiredCapabilities();
        FirefoxOptions options = new FirefoxOptions();
        options.addArguments("test-type");
        capability.setCapability(ChromeOptions.CAPABILITY, options);
		firefoxdriver = new RemoteWebDriver(new URL("http://127.0.0.1:" + platformPort + "/wd/hub"), capability);
		firefoxdriver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		firefoxdriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		//firefoxdriver.manage().window().maximize();
		return firefoxdriver; 
		 
		 /*DesiredCapabilities capability = DesiredCapabilities.firefox();
			//DesiredCapabilities capability = new DesiredCapabilities();
	        FirefoxOptions options = new FirefoxOptions();
	        options.addPreference("network.proxy.type", 0);
	        options.addArguments("test-type");
	        capability.setCapability(ChromeOptions.CAPABILITY, options);
	        System.out.println("after capability");
			firefoxdriver = new RemoteWebDriver(new URL("http://127.0.0.1:" + platformPort + "/wd/hub"), capability);
			System.out.println("after remote");
			firefoxdriver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
			System.out.println("after pageload");
			firefoxdriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			System.out.println("after wait");
	        firefoxdriver.manage().window().maximize();
	        System.out.println("after maximize");
			 return firefoxdriver; */
	}
	
	/**
	 * Method to launch Chrome Driver
	 * @param platformPort Platform Port
	 * @return WebDriver instance
	 * @throws Exception exception
	 */
	public static WebDriver getChromeDriver(String platformPort) throws MalformedURLException, InterruptedException{
		
		 System.out.println(" Executing on CHROME");
         DesiredCapabilities capability = new DesiredCapabilities();
         ChromeOptions options = new ChromeOptions();
         options.addArguments("test-type");
         Map<String, Object> prefs = new HashMap<String, Object>(); 
		 prefs.put("safebrowsing.enabled", "true"); 
		 options.setExperimentalOption("prefs", prefs); 
         capability.setCapability(ChromeOptions.CAPABILITY, options);
         
         chromedriver = new RemoteWebDriver(new URL("http://127.0.0.1:" + platformPort + "/wd/hub"), capability);   
         chromedriver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
         chromedriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
 		 chromedriver.manage().window().maximize();
		return chromedriver;	
}
	/**
	 * Method to launch IE Driver instance
	 * @param platformPort platform port
	 * @return WebDriver instance
	 * @throws MalformedURLException exception
	 */
	public static WebDriver getIEDriver(String platformPort) throws MalformedURLException{
		
		/*DesiredCapabilities capability = DesiredCapabilities.internetExplorer();
        //DesiredCapabilities capability = new DesiredCapabilities();
        InternetExplorerOptions options = new InternetExplorerOptions();
        //capability.setCapability(ChromeOptions.CAPABILITY, options);
        //capability.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
        //capability.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        capability.setCapability("acceptInsecureCerts", true);
        //capability.setCapability("acceptSslCerts",true);
        capability.setAcceptInsecureCerts(true);
        capability.acceptInsecureCerts();*/
        /*capability.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        System.setProperty("webdriver.ie.driver","IEDriverServer.exe");*/
		
		DesiredCapabilities capability = DesiredCapabilities.internetExplorer();
		capability.setCapability(InternetExplorerDriver.IGNORE_ZOOM_SETTING, true);

        String Node = "http://127.0.0.1:" + platformPort + "/wd/hub";
        iedriver = new RemoteWebDriver(new URL(Node), capability);
        iedriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        iedriver.manage().window().maximize();
        return iedriver;	
	}
	
	/**
	 * Launch iOS Safari Driver
	 * @param platformname Platform Name
	 * @param platformport Platform Port
	 * @param platformid Platform ID 
	 * @param platformversion Platform version
	 * @return WebDriver 
	 * @throws MalformedURLException
	 */
	public static WebDriver getIOSWebDriver(String platformName, String platformPort, String platformId, String platformVersion) throws Exception{
		 
		  if(iOSWebDriver == null){
	/*	   DesiredCapabilities  capabilities = DesiredCapabilities.iphone();
		   capabilities.setCapability("platformName","iOS");
		   capabilities.setCapability("deviceName",platformId);
		   capabilities.setCapability("browserName", "safari");
		   capabilities.setCapability("automationName", "XCUITest");
		   capabilities.setCapability("appium-version", LoadProp.getProperty("appium-version"));
		   capabilities.setCapability("platformVersion",platformVersion);
		   capabilities.setCapability("platformName","iOS");
		   capabilities.setCapability("xcodeOrgId", "4ABMX2V53F");
		   capabilities.setCapability("xcodeSigningId", "iPhone Developer");
		   capabilities.setCapability("udid","auto");
		   iOSWebDriver = new RemoteWebDriver(new URL("http://172.25.28.30:4725/wd/hub"), capabilities);
		   
		   */
		   
		   DesiredCapabilities  capabilities = DesiredCapabilities.iphone();
		   capabilities.setCapability("platformName","iOS");
		   capabilities.setCapability("deviceName","iPhone Simulator");
		   capabilities.setCapability("browserName", "safari");
		   capabilities.setCapability("automationName", "XCUITest");
		   capabilities.setCapability("platformVersion","11.2");
		   capabilities.setCapability("platformName","iOS");
	//	   iOSWebDriver = new RemoteWebDriver(new URL("http://172.25.28.30:4725/wd/hub"), capabilities);
		   iOSWebDriver = new RemoteWebDriver(new URL("http://172.24.6.238:4725/wd/hub"), capabilities);

		   
		   
		   
		   
		  }
		  return iOSWebDriver;
		 }
}
