package com.testsuiterunner;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.*;

import org.testng.TestNG;
import org.testng.xml.XmlClass;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlSuite.ParallelMode;
import org.testng.xml.XmlTest;

import com.properties.mapping.LoadProp;
import com.reporter.ReportMail;
import com.utility.library.Global;
import com.utility.library.Util;

public class Invoke {
	 
	static XmlSuite suite = new XmlSuite();
	static Runtime run ;
	public static void main(String args[]) throws Exception{
		new LoadProp();
		/*String line = "null";
		String cmd = "adb devices";
		run = Runtime.getRuntime();
		Process pr = run.exec(cmd);
		
		pr.waitFor();
		BufferedReader buf = new BufferedReader(new InputStreamReader(pr.getInputStream()));
		while ((line=buf.readLine())!=null) {
		System.out.println(line);
		}
		Thread.sleep(2000);*/
		
		// start hub
		String[] up_hub = {"cmd.exe", "/C", "Start", Global.PROJECT_LOCATION+"json\\start_selenium_grid_hub.bat"};
        Runtime.getRuntime().exec(up_hub);
        
        // connect Android nodes
        if(Integer.parseInt(LoadProp.getProperty("total_android_instance"))!=0) {
        	 String[] up_android = {"cmd.exe", "/C", "Start", Global.PROJECT_LOCATION + "json\\dell_nexus.bat"};
        	 Runtime.getRuntime().exec(up_android);
        	 Thread.sleep(20000);
        }
        
        // connect Web nodes
        if(Integer.parseInt(LoadProp.getProperty("total_web_instance"))!=0) {
       		for(int k=1; k<=Integer.parseInt(LoadProp.getProperty("total_web_instance")); k++) {
       			if(LoadProp.getProperty("web_" + k + "_name").equalsIgnoreCase("chrome")) {
       				String[] up_chrome = {"cmd.exe", "/C", "Start", Global.PROJECT_LOCATION + "json\\chromelatest.bat"};
       				Runtime.getRuntime().exec(up_chrome);
       			}else if(LoadProp.getProperty("web_" + k + "_name").equalsIgnoreCase("firefox")) {
       		        String[] up_firefox = {"cmd.exe", "/C", "Start", Global.PROJECT_LOCATION + "json\\firefoxlatest.bat"};
       		        Runtime.getRuntime().exec(up_firefox);
       			}else if(LoadProp.getProperty("web_" + k + "_name").equalsIgnoreCase("ie")) {
       				String[] up_ie = {"cmd.exe", "/C", "Start", Global.PROJECT_LOCATION + "json\\ielatest.bat"};
       				Runtime.getRuntime().exec(up_ie);
       			}
       		}
        }
        // connect Android Browser nodes
        if(Integer.parseInt(LoadProp.getProperty("total_android_browser_instance"))!=0) {
        	String[] up_androidBrowser = {"cmd.exe", "/C", "Start", Global.PROJECT_LOCATION + "json\\androidbrowser.bat"};
       	 	Runtime.getRuntime().exec(up_androidBrowser);
       	 Thread.sleep(20000);
        }
        
        // connect iOS Browser nodes
        if(Integer.parseInt(LoadProp.getProperty("total_ios_browser_nodes"))!=0) {
       // 	String[] up_iOSBrowser = {"cmd.exe", "/C", "Start", Global.PROJECT_LOCATION + "json\\androidbrowser.bat"};
       	// 	Runtime.getRuntime().exec(up_androidBrowser);
        	//TODO
        	// launching node on remote machine. in this case mac
        }
        
        
        //Thread.sleep(30000);
        
		setupTestNGXML();
	}
	
	/**
	 * Class to setup TestNG XML
	 * @throws Exception exception
	 */
	public static void setupTestNGXML() throws Exception{
		
		// set suite properties
		suite.setName(LoadProp.getProperty("test_suite_name"));
		suite.setVerbose(1);
		suite.setParallel(ParallelMode.TESTS);
		suite.setThreadCount(3);
		suite.addListener("com.reporter.CustomReportListener");
		String testfile;
		
		//Add android tests as per the android instance count
		for(int i =1; i <= Integer.parseInt(LoadProp.getProperty("total_android_instance")); i++) {
			// populate test parameters
			loadParameters("android_"+i, Global.ANDROID);
		}
		//Add android browser tests as per the android browser instance count
		for(int i =1; i <= Integer.parseInt(LoadProp.getProperty("total_android_browser_instance")); i++) {
			// load test parameters
			loadParameters("androidBrowser_"+i, Global.ANDROID_BROWSER);
		}
		
		//Add web tests as per browser instance count
		for(int i = 1; i <= Integer.parseInt(LoadProp.getProperty("total_web_instance")); i++) {
			// load test parameters
			loadParameters("web_"+i , Global.WEB);
		}
		//Add IOS tests as per iOS instance count
		for(int i = 1; i <= Integer.parseInt(LoadProp.getProperty("total_ios_instance")); i++) {
			// load test parameters
			loadParameters("ios_"+i, Global.IOS);
		}
		//Add IOS tests as per iOS browser node count
		for(int i = 1; i <= Integer.parseInt(LoadProp.getProperty("total_ios_browser_nodes")); i++) {
			// load test parameters
			loadParameters("iosBrowser_"+i, Global.IOS_BROWSER);
		}

		//Add API tests as per 
		for(int i = 1; i <= Integer.parseInt(LoadProp.getProperty("total_api_instance")); i++) {
			// load test parameters
			loadParameters("api_"+i, Global.API);
		}
		
		
		
		//Create suites from number of test suite
		List<XmlSuite> suites = new ArrayList<XmlSuite>();
		suites.add(suite);

		TestNG tng = new TestNG();
		tng.setXmlSuites(suites);
		
		/*//Get the excel file path
		testfile = LoadProp.getProperty("test_case_file").toString();
		//Read test.xls file 
		Util.getXlsTestsMapping(testfile);*/
		
		// populate Dictionary for each Platform
		Util.populatePlatformTests();
		tng.run(); 

		// email the execution status if enabled.
		if(LoadProp.getProperty("email_reporting_enabled").toString().equalsIgnoreCase("Y")){
			ReportMail rm = new ReportMail();
			rm.SendEmail();
		}
		
		

	}
	
	
	/**
	 * Method to load test parameters
	 * @param platformName_instance platform name and instance number
	 * @param platformType platform type
	 */
	public static void loadParameters(String platformName_instance , String platformType) {
		
		// populate the test parameters for each test
		Map<String, String> testParameters = new HashMap<>();
		testParameters.put("platformType", platformType);
		testParameters.put("platformName", LoadProp.getProperty(platformName_instance + "_name"));
		testParameters.put("platformPort", LoadProp.getProperty(platformName_instance + "_port"));
		testParameters.put("platformId", LoadProp.getProperty(platformName_instance + "_id"));
		testParameters.put("platformVersion", LoadProp.getProperty(platformName_instance + "_version"));
		testParameters.put("instanceNo", LoadProp.getProperty(platformName_instance + "_instance_no"));

		XmlTest test = new XmlTest(suite);
		// set test name and parameters
		test.setName("test_" + platformName_instance + "_name");
		test.setParameters(testParameters);
		
		//Create classes and add the test classes
		List<XmlClass> classes = new ArrayList<XmlClass>();
		classes.add(new XmlClass(LoadProp.getProperty("class_name")));
		//set test classes
		test.setXmlClasses(classes) ;
	}
}
