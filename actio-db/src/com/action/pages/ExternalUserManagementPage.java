package com.action.pages;

import java.util.Dictionary;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;

import com.properties.mapping.LoadProp;
import com.relevantcodes.extentreports.ExtentTest;
import com.utility.library.Util;

public class ExternalUserManagementPage {
	
	//Add_External_User for Full Access list
	public static void add_external_user_fal(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			//Navigate to 'Management > External User Management'
			Util.clickManagement(driver, paramDict, test);
			Util.click(LoadProp.externalusermngmtprop, "External_User_Management_xpath", test, driver);
			
			//Click Add to add external user
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.externalusermngmtprop.get("ExternalUser_Add_xpath")));
			Util.click(LoadProp.externalusermngmtprop, "ExternalUser_Add_xpath", test, driver);
			Util.reportPass(driver,test,"Clicked on Add button");
			
			//Select Full accesslist from dropdown and enter domain/email
			Util.click(LoadProp.externalusermngmtprop, "Dropdown_xpath", test, driver);
			Util.click(LoadProp.externalusermngmtprop, "FullAccess_List_xpath", test, driver);
			Util.sendKeys(LoadProp.externalusermngmtprop, "Enter_Domain_Email_xpath", paramDict.get(testId+".domain_email"), test, driver);
			Util.click(LoadProp.externalusermngmtprop, "AddExternalUser_xpath", test, driver);
			Util.click(LoadProp.externalusermngmtprop, "ClickonCancel_button_xpath", test, driver);
			
			//Verify wrap message for external user added
			//Full Access List entries successfully deleted.
			WebElement element = driver.findElement(By.xpath(LoadProp.externalusermngmtprop.get("wrap_msg_xpath")));
			String email_add_msg = element.getText();
			//System.out.println(email_add_msg);
			Assert.assertEquals(email_add_msg, "The notification was successfully added.");
			Util.click(LoadProp.externalusermngmtprop, "Click_Refresh_Button_xpath", test, driver);
			Util.waitUntilElementVisibility(driver, By.xpath("//div[@class='k-grid-content k-auto-scrollable']/table/tbody/tr/td/span[contains(text(), '" + paramDict.get("addexternaluser_fal.domain_email") + "')]"));
			Util.reportPass(driver,test,"External User: " + paramDict.get(testId+".domain_email") + " has been successfully added");
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
	//Add_External_User for Blacklist
	public static void add_external_user_bl(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			//Navigate to 'Management > External User Management'
			Util.clickManagement(driver, paramDict, test);
			Util.click(LoadProp.externalusermngmtprop, "External_User_Management_xpath", test, driver);
			
			//Click Add to add external user
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.externalusermngmtprop.get("ExternalUser_Add_xpath")));
			Util.click(LoadProp.externalusermngmtprop, "ExternalUser_Add_xpath", test, driver);
			Util.reportPass(driver,test,"Clicked on Add button");
			
			//Select Full accesslist from dropdown and enter domain/email
			Util.click(LoadProp.externalusermngmtprop, "Dropdown_xpath", test, driver);
			Util.click(LoadProp.externalusermngmtprop, "Black_List_xpath", test, driver);
			Util.sendKeys(LoadProp.externalusermngmtprop, "Enter_Domain_Email_xpath", paramDict.get(testId+".domain_email"), test, driver);
			Util.click(LoadProp.externalusermngmtprop, "AddExternalUser_xpath", test, driver);
			Util.click(LoadProp.externalusermngmtprop, "ClickonCancel_button_xpath", test, driver);
			
			//Verify wrap message for external user added
			//Full Access List entries successfully deleted.
			WebElement element = driver.findElement(By.xpath(LoadProp.externalusermngmtprop.get("wrap_msg_xpath")));
			String email_add_msg = element.getText();
			//System.out.println(email_add_msg);
			Assert.assertEquals(email_add_msg, "The notification was successfully added.");
			Util.click(LoadProp.externalusermngmtprop, "Click_Refresh_Button_xpath", test, driver);
			Util.waitUntilElementVisibility(driver, By.xpath("//div[@class='k-grid-content k-auto-scrollable']/table/tbody/tr/td/span[contains(text(), '" + paramDict.get(testId+".domain_email") + "')]"));
			Util.reportPass(driver,test,"External User: " + paramDict.get(testId+".domain_email") + " has been successfully added");
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
	//Delete_External_User
	public static void delete_external_user(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			//Navigate to 'Management > External User Management'
			Util.clickManagement(driver, paramDict, test);
			Util.click(LoadProp.externalusermngmtprop, "External_User_Management_xpath", test, driver);
			
			//Select External user to be deleted
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.externalusermngmtprop.get("ExternalUser_Add_xpath")));
			driver.findElement(By.xpath("//div[@class='k-grid-content k-auto-scrollable']/table/tbody/tr/td/span[contains(text(), '" + paramDict.get(testId+".domain_email") + "')]")).click();
			Util.reportPass(driver,test,"External user selected");
			
			//Click delete button to delete external user
			Util.click(LoadProp.externalusermngmtprop, "ExternalUser_Delete_xpath", test, driver);
			Util.reportPass(driver,test,"Clicked on Delete Button");
			
			//Confirm Ok
			Util.click(LoadProp.externalusermngmtprop, "Click_Ok_xpath", test, driver);
			Util.reportPass(driver,test,"Clicked on Ok Button");
			
			//Verify wrap message for notification deleted
			WebElement element = driver.findElement(By.xpath(LoadProp.externalusermngmtprop.get("wrap_msg_xpath")));
			String externalUser_delete_msg = element.getText();
			System.out.println(externalUser_delete_msg);
			//Assert.assertEquals(externalUser_delete_msg,  " entries successfully deleted.");
			Util.waitUntilElementVisibility(driver, By.xpath("//*[contains(text(), 'entries successfully deleted.')]"));
			Util.reportPass(driver,test,"External User deleted");
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
}
	
	