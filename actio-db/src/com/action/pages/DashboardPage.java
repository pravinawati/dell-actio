package com.action.pages;

import java.io.IOException;
import java.util.Dictionary;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;

import com.properties.mapping.LoadProp;
import com.relevantcodes.extentreports.ExtentTest;
import com.utility.library.Util;

public class DashboardPage {
	
	public static void access_webui_about(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.dashboardprop.get("settingsIcon_xpath")));
			Util.click(LoadProp.dashboardprop, "settingsIcon_xpath", test, driver);
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.dashboardprop.get("WebUiAbout_xpath")));
			Util.click(LoadProp.dashboardprop, "WebUiAbout_xpath", test, driver);
			Util.reportPass(driver,test, "==> About screen accessed!!");
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
	public static void verify_about_version(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{	
			WebElement element1 = driver.findElementByXPath(LoadProp.dashboardprop.get("serverversion_xpath"));
			String serverversion= element1.getText();
			Assert.assertEquals("9.11", serverversion);
			Util.reportPass(driver,test, "==> About server version verified!!");
		
			WebElement element2 = driver.findElementByXPath(LoadProp.dashboardprop.get("WebUiCopyrightYear_xpath"));
			String WebUITrademark= element2.getText();
			Assert.assertEquals("� 2018 Dell Inc. ALL RIGHTS RESERVED.", WebUITrademark);
			Util.reportPass(driver,test, "==> About copyright verified!!");
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
	public static void access_help_file(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test){
		try{
			Util.waitUntilElementVisibility(driver,By.xpath(LoadProp.dashboardprop.get("HelpIconLink_xpath")));
			Util.click(LoadProp.dashboardprop, "HelpIconLink_xpath", test, driver);
			Util.reportPass(driver,test, "==> Help Icon clicked!!");
			driver.switchTo().frame("myDialogWindow");
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
	public static void verify_help_about_online_version(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			Util.waitUntilElementVisibility(driver,By.xpath(LoadProp.dashboardprop.get("HelpHomeLink_xpath")));
			Util.click(LoadProp.dashboardprop, "HelpHomeLink_xpath", test, driver);
			Util.reportPass(driver,test, "==> Help Home Icon clicked!!");
			Util.waitUntilElementVisibility(driver,By.xpath(LoadProp.dashboardprop.get("HelpWelcomeLink_xpath")));
			Util.click(LoadProp.dashboardprop, "HelpWelcomeLink_xpath", test, driver);
			Util.reportPass(driver,test, "==> Help Welcome Link clicked!!");
			Util.waitUntilElementVisibility(driver,By.xpath(LoadProp.dashboardprop.get("AboutOnlineHelpSystemLink_xpath")));
			Util.click(LoadProp.dashboardprop, "AboutOnlineHelpSystemLink_xpath", test, driver);
			Util.reportPass(driver,test, "==> Help About Online Help System Link clicked!!");
		
			Util.waitUntilElementVisibility(driver,By.xpath(LoadProp.dashboardprop.get("HelpFileVersion_xpath")));
		
			WebElement element = driver.findElement(By.xpath(LoadProp.dashboardprop.get("HelpFileVersion_xpath")));
			String HelpFileiVersion = element.getText();
			System.out.println(HelpFileiVersion);
			Assert.assertEquals("9.11", HelpFileiVersion);
			Util.reportPass(driver,test, "==> Online Help version verified!!");
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
	public static void close_help_file(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test){
		try{
			driver.switchTo().defaultContent();
			Util.waitUntilElementVisibility(driver,By.xpath(LoadProp.dashboardprop.get("HelpIconLink_xpath")));
			Util.click(LoadProp.dashboardprop, "closehelp_xpath", test, driver);
			Util.reportPass(driver,test, "==> Help File closed!!");
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}

	public static void verify_help_about_online_copyright(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			Util.waitUntilElementVisibility(driver,By.xpath(LoadProp.dashboardprop.get("HelpHomeLink_xpath")));
			Util.click(LoadProp.dashboardprop, "HelpHomeLink_xpath", test, driver);
			Util.reportPass(driver,test, "==> Help Home Icon clicked!!");
			Util.waitUntilElementVisibility(driver,By.xpath(LoadProp.dashboardprop.get("HelpWelcomeLink_xpath")));
			Util.click(LoadProp.dashboardprop, "HelpWelcomeLink_xpath", test, driver);
			Util.reportPass(driver,test, "==> Help Welcome Link clicked!!");
			Util.waitUntilElementVisibility(driver,By.xpath(LoadProp.dashboardprop.get("AboutOnlineHelpSystemLink_xpath")));
			Util.click(LoadProp.dashboardprop, "AboutOnlineHelpSystemLink_xpath", test, driver);
			Util.reportPass(driver,test, "==> Help About Online Help System Link clicked!!");
		
			Util.waitUntilElementVisibility(driver,By.xpath(LoadProp.dashboardprop.get("HelpFileVersion_xpath")));
		
			WebElement element = driver.findElement(By.xpath(LoadProp.dashboardprop.get("HelpFileCopyrightDate_xpath")));
			String HelpFileCopyright = element.getText();
			System.out.println(HelpFileCopyright);
			Assert.assertEquals("� 2012 - 2018 Dell Inc. All rights reserved.", HelpFileCopyright);
			Util.reportPass(driver,test, "==> Online Help copyright verified!!");
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
	public static void dashboard_summary_stats (RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		// TODO Auto-generated method stub
		try{
			//serve.access(driver, serve.getProperties("weburl")+"getDashboard");
			Thread.sleep(500);	

			String domains = driver.findElement(By.xpath(LoadProp.dashboardprop.get("dashboard_domain_xpath"))).getText();
			String userGroups = driver.findElement(By.xpath(LoadProp.dashboardprop.get("dashboard_usergroups_xpath"))).getText();
			String endpointGroups = driver.findElement(By.xpath(LoadProp.dashboardprop.get("dashboard_endpointgroups_xpath"))).getText();
			String adUsers = driver.findElement(By.xpath(LoadProp.dashboardprop.get("dashboard_adusers_xpath"))).getText();
			String extUsers = driver.findElement(By.xpath(LoadProp.dashboardprop.get("dashboard_extusers_xpath"))).getText();
			String localUsers = driver.findElement(By.xpath(LoadProp.dashboardprop.get("dashboard_localusers_xpath"))).getText();
			String endpoints = driver.findElement(By.xpath(LoadProp.dashboardprop.get("dashboard_endpoints_xpath"))).getText();
			String protected1 = driver.findElement(By.xpath(LoadProp.dashboardprop.get("dashboard_protected_xpath"))).getText();
			String notProtected = driver.findElement(By.xpath(LoadProp.dashboardprop.get("dashboard_notprotected_xpath"))).getText();
			String shields = driver.findElement(By.xpath(LoadProp.dashboardprop.get("dashboard_shields_xpath"))).getText();
			String managers = driver.findElement(By.xpath(LoadProp.dashboardprop.get("dashboard_managers_xpath"))).getText();
			String modifiedPolicy= driver.findElement(By.xpath(LoadProp.dashboardprop.get("dashboard_modifiedpolicy_xpath"))).getText();
			String windowsTotal = driver.findElement(By.xpath(LoadProp.dashboardprop.get("dashboard_windowsPlatform_xpath"))).getText();
			String macTotal = driver.findElement(By.xpath(LoadProp.dashboardprop.get("dashboard_macPlatform_xpath"))).getText();
			String mobileDevice = driver.findElement(By.xpath(LoadProp.dashboardprop.get("dashboard_mobiledeviceplatform_xpath"))).getText();
			String all = driver.findElement(By.xpath(LoadProp.dashboardprop.get("dashboard_allplatform_xpath"))).getText();

			//Navigate to Populations
			//Util.clickPopulations(driver, paramDict, test);
			
			
			//Verify Number of Domains
			//Util.click(LoadProp.dashboardprop, "domains_link_xpath", test, driver);
			Util.access_link(LoadProp.getProperty("web_weburl")+"manageDomain", test, driver);
			List<WebElement> noOfDomains = driver.findElements(By.xpath(LoadProp.dashboardprop.get("noOfDomains_xpath")));
			//String s = String.valueOf(noOfDomains.size());
			Assert.assertEquals(String.valueOf(noOfDomains.size()), domains);
			Util.reportPass(driver,test, "==> Domain counts verified successfully");
			
			//Verify Number of User Groups
			//Util.click(LoadProp.dashboardprop, "usergroup_link_xpath", test, driver);
			Util.access_link(LoadProp.getProperty("web_weburl")+"manageUserGroup", test, driver);
			List<WebElement> noOfUGS = driver.findElements(By.xpath(LoadProp.dashboardprop.get("noOfUGS_xpath")));
			Assert.assertEquals(String.valueOf(noOfUGS.size()), userGroups);
			Util.reportPass(driver,test, "==> User Group counts verified successfully");
			
			//Verify Number of AD Users
			//Util.click(LoadProp.dashboardprop, "users_link_xpath", test, driver);
			Util.access_link(LoadProp.getProperty("web_weburl")+"manageUsers", test, driver);
			List<WebElement> noOfadUsers = driver.findElements(By.xpath(LoadProp.dashboardprop.get("noOfadUsers_xpath")));
			System.out.println(noOfadUsers.size());
			int totalAdUsers = noOfadUsers.size() + 1;
			System.out.println(totalAdUsers);
			Assert.assertEquals(String.valueOf(totalAdUsers), adUsers);
			Util.reportPass(driver,test, "==> AD Users verified successfully");
			
			//Verify Number of External Users
			//Util.click(LoadProp.dashboardprop, "users_link_xpath", test, driver);
			Util.access_link(LoadProp.getProperty("web_weburl")+"manageUsers", test, driver);
			List<WebElement> noOfextUsers = driver.findElements(By.xpath(LoadProp.dashboardprop.get("noOfextUsers_xpath")));
			System.out.println(noOfextUsers.size());
			Assert.assertEquals(String.valueOf(noOfextUsers.size()), extUsers);
			Util.reportPass(driver,test, "==> External Users verified successfully");
			
			//Verify Number of Local Users
			//Util.click(LoadProp.dashboardprop, "users_link_xpath", test, driver);
			Util.access_link(LoadProp.getProperty("web_weburl")+"manageUsers", test, driver);
			List<WebElement> noOflocalUsers = driver.findElements(By.xpath(LoadProp.dashboardprop.get("noOflocalUsers_xpath")));
			System.out.println(noOflocalUsers.size());
			Assert.assertEquals(String.valueOf(noOflocalUsers.size()), localUsers);
			Util.reportPass(driver,test, "==> LOCAL Users verified successfully");
			
			//Verify Number of Endpoint Groups
			//Util.click(LoadProp.dashboardprop, "endpointgroup_link_xpath", test, driver);
			Util.access_link(LoadProp.getProperty("web_weburl")+"manageEndpointGroup", test, driver);
			Thread.sleep(1000);
			List<WebElement> noOfSysDefEGS = driver.findElements(By.xpath(LoadProp.dashboardprop.get("noOfSysDefEGS_xpath")));
			List<WebElement> noOfUserDefEGS = driver.findElements(By.xpath(LoadProp.dashboardprop.get("noOfUserDefEGS_xpath")));
			int noOfEGS = noOfSysDefEGS.size() + noOfUserDefEGS.size();
			Assert.assertEquals(String.valueOf(noOfEGS), endpointGroups);
			Util.reportPass(driver,test, "==> Endpoint Group counts verified successfully");
			
			//Verify Number of Endpoints
			//Util.click(LoadProp.dashboardprop, "endpoint_link_xpath", test, driver);
			Util.access_link(LoadProp.getProperty("web_weburl")+"manageEndpoints", test, driver);
			List<WebElement> noOfEPs = driver.findElements(By.xpath(LoadProp.dashboardprop.get("noOfEPs_xpath")));
			Assert.assertEquals(String.valueOf(noOfEPs.size()), endpoints);
			Util.reportPass(driver,test, "==> Endpoint counts verified successfully");
			
			//Verify Number of Shields
			//Util.click(LoadProp.dashboardprop, "endpoint_link_xpath", test, driver);
			Util.access_link(LoadProp.getProperty("web_weburl")+"manageEndpoints", test, driver);
			int endpointrows1 = driver.findElements(By.xpath("//tbody/tr")).size();
			int shieldCount = 0;
			int i=1;
			while(i < endpointrows1){
				String sh = driver.findElement(By.xpath("//tbody/tr["+i+"]/td[9]/span")).getText();
				if(StringUtils.isNotEmpty(sh)){
					shieldCount++;
				}
				i++;
			}
			System.out.println(shieldCount);
			Assert.assertEquals(String.valueOf(shieldCount), shields);
			Util.reportPass(driver,test, "==> Shield counts verified successfully");
			
			//Verify Number of Managers
			//Util.click(LoadProp.dashboardprop, "endpoint_link_xpath", test, driver);
			Util.access_link(LoadProp.getProperty("web_weburl")+"manageEndpoints", test, driver);
			int endpointrows2 = driver.findElements(By.xpath("//tbody/tr")).size();
			int managerCount = 0;
			int j=1;
			while(j < endpointrows2){
				String sh = driver.findElement(By.xpath("//tbody/tr["+j+"]/td[10]/span")).getText();
				if(StringUtils.isNotEmpty(sh)){
					managerCount++;
				}
				j++;
			}
			System.out.println(managerCount);
			Assert.assertEquals(String.valueOf(managerCount), managers);
			Util.reportPass(driver,test, "==> Manager counts verified successfully");
			
			//Verify Number of Windows Endpoints
			//Util.click(LoadProp.dashboardprop, "endpoint_link_xpath", test, driver);
			Util.access_link(LoadProp.getProperty("web_weburl")+"manageEndpoints", test, driver);
			Util.waitUntilElementVisibility(driver,By.xpath(LoadProp.dashboardprop.get("select_workstation_dropdown_xpath")));
			Util.click(LoadProp.dashboardprop, "select_workstation_dropdown_xpath", test, driver);
			Util.waitUntilElementVisibility(driver,By.xpath(LoadProp.dashboardprop.get("select_workstation_xpath")));
			Util.click(LoadProp.dashboardprop, "select_workstation_xpath", test, driver);
			Util.waitUntilElementVisibility(driver,By.xpath(LoadProp.dashboardprop.get("select_category_dropdown_xpath")));
			Util.click(LoadProp.dashboardprop, "select_category_dropdown_xpath", test, driver);
			Util.waitUntilElementVisibility(driver,By.xpath(LoadProp.dashboardprop.get("select_Windows_category_xpath")));
			Util.click(LoadProp.dashboardprop, "select_Windows_category_xpath", test, driver);
			Thread.sleep(500);
			List<WebElement> noOfWinEnd = driver.findElements(By.xpath(LoadProp.dashboardprop.get("noOfWinEnd_xpath")));
			//System.out.println(noOfWinEnd.size());
			Assert.assertEquals(String.valueOf(noOfWinEnd.size()), windowsTotal);
			Util.reportPass(driver,test, "==> Windows Endpoint counts verified successfully");
			
			//Verify Number of Mac Endpoints
			//Util.click(LoadProp.dashboardprop, "endpoint_link_xpath", test, driver);
			Util.access_link(LoadProp.getProperty("web_weburl")+"manageEndpoints", test, driver);
			Util.waitUntilElementVisibility(driver,By.xpath(LoadProp.dashboardprop.get("select_workstation_dropdown_xpath")));
			Util.click(LoadProp.dashboardprop, "select_workstation_dropdown_xpath", test, driver);
			Util.waitUntilElementVisibility(driver,By.xpath(LoadProp.dashboardprop.get("select_workstation_xpath")));
			Util.click(LoadProp.dashboardprop, "select_workstation_xpath", test, driver);
			Util.waitUntilElementVisibility(driver,By.xpath(LoadProp.dashboardprop.get("select_category_dropdown_xpath")));
			Util.click(LoadProp.dashboardprop, "select_category_dropdown_xpath", test, driver);
			Util.waitUntilElementVisibility(driver,By.xpath(LoadProp.dashboardprop.get("select_Mac_category_xpath")));
			Util.click(LoadProp.dashboardprop, "select_Mac_category_xpath", test, driver);
			Thread.sleep(500);
			List<WebElement> noOfMacEnd = driver.findElements(By.xpath(LoadProp.dashboardprop.get("noOfMacEnd_xpath")));
			Assert.assertEquals(String.valueOf(noOfMacEnd.size()), macTotal);
			Util.reportPass(driver,test, "==> Mac Endpoint counts verified successfully");
			
			//Verify Number of Mobile Devices
			//Util.click(LoadProp.dashboardprop, "endpoint_link_xpath", test, driver);
			Util.access_link(LoadProp.getProperty("web_weburl")+"manageEndpoints", test, driver);
			Util.waitUntilElementVisibility(driver,By.xpath(LoadProp.dashboardprop.get("select_workstation_dropdown_xpath")));
			Util.click(LoadProp.dashboardprop, "select_workstation_dropdown_xpath", test, driver);
			Util.waitUntilElementVisibility(driver,By.xpath(LoadProp.dashboardprop.get("select_mobile_device_xpath")));
			Util.click(LoadProp.dashboardprop, "select_mobile_device_xpath", test, driver);
			Thread.sleep(500);
			//serve.click(driver, By.xpath("//div[@class='grid-toolbar']/div[3]/span/span"));
			//serve.click(driver, By.xpath("//div[4]/div/div[@class='k-list-scroller']/ul/li[@data-offset-index='1']"));
			List<WebElement> noOfMobEnd = driver.findElements(By.xpath(LoadProp.dashboardprop.get("noOfMobEnd_xpath")));
			System.out.println(noOfMobEnd.size());
			Assert.assertEquals(String.valueOf(noOfMobEnd.size()), mobileDevice);
			//System.out.println(noOfMobEnd.size());
			Util.reportPass(driver,test, "==> Mobile Device Endpoint counts verified successfully");
			
			//Verify Number of Total Endpoints
			int TotalEndpoints = noOfWinEnd.size() + noOfMacEnd.size() + noOfMobEnd.size();
			Assert.assertEquals(String.valueOf(TotalEndpoints), all);
			Util.reportPass(driver,test, "==> All Endpoints verified successfully");
			
			//Verify Number of Modified Policies
			//Util.clickManagement(driver, paramDict, test);
			//Util.click(LoadProp.dashboardprop, "commit_link_xpath", test, driver);
			Util.access_link(LoadProp.getProperty("web_weburl")+"manageCommit", test, driver);
			
			String commit_policy = driver.findElement(By.xpath(LoadProp.dashboardprop.get("commit_status_check_xpath"))).getText();
			//System.out.println(commit_policy);
			if(commit_policy.equals("")){
				int count = 0;
				Assert.assertEquals(String.valueOf(count), modifiedPolicy);
				Util.reportPass(driver,test, "==> No policies to commit. Modified Policies verified successfully");
			}else{
				String modPolicy = driver.findElement(By.xpath(LoadProp.dashboardprop.get("commit_status_count_avl_xpath"))).getText();
				String count = modPolicy.substring(24);
				//System.out.println(count);
				Assert.assertEquals(count, modifiedPolicy);
				Util.reportPass(driver,test, "==> Modified Policies verified successfully");
			}

			//Verify Number of Protected Endpoints
			//Util.click(LoadProp.dashboardprop, "go_dashboard_xpath", test, driver);
			Util.access_link(LoadProp.getProperty("web_weburl")+"getDashboard", test, driver);
			//List<WebElement> noOfProtEnd = driver.findElements(By.xpath("//div[@id='grida']/div[@class='k-grid-content k-auto-scrollable']/table/tbody/tr"));
			String noOfProtEnd = driver.findElement(By.xpath(LoadProp.dashboardprop.get("noOfProtEnd_xpath"))).getText();
			Assert.assertEquals(noOfProtEnd, protected1);
			Util.reportPass(driver,test, "==> Protected Endpoint counts verified successfully");
			
			//Verify Number of Unprotected Endpoints
			//Util.click(LoadProp.dashboardprop, "go_dashboard_xpath", test, driver);
			Util.access_link(LoadProp.getProperty("web_weburl")+"getDashboard", test, driver);
			String noOfUnProtEnd = driver.findElement(By.xpath(LoadProp.dashboardprop.get("noOfUnProtEnd_xpath"))).getText();
			Assert.assertEquals(noOfUnProtEnd, notProtected);
			Util.reportPass(driver,test, "==> Not Protected Endpoint counts verified successfully");
			
			Util.reportPassWithoutScreenshot(test, "==> ###### Summary Statistics #######\n");
			Util.reportPassWithoutScreenshot(test, "==> ###### Details #######\n");
			Util.reportPassWithoutScreenshot(test, "==> Domains    	: " + domains);
			Util.reportPassWithoutScreenshot(test, "==> UserGroups 	: " + userGroups);
			Util.reportPassWithoutScreenshot(test, "==> EndpointGroups	: " + endpointGroups);
			Util.reportPassWithoutScreenshot(test, "==> AD Users		: " + adUsers);
			Util.reportPassWithoutScreenshot(test, "==> External Users	: " + extUsers);
			Util.reportPassWithoutScreenshot(test, "==> Local Users	: " + localUsers);
			Util.reportPassWithoutScreenshot(test, "==> Endpoints		: " + endpoints);
			Util.reportPassWithoutScreenshot(test, "==> Protected		: " + protected1);
			Util.reportPassWithoutScreenshot(test, "==> Not Protected	: " + notProtected);
			Util.reportPassWithoutScreenshot(test, "==> Shields		: " + shields);
			Util.reportPassWithoutScreenshot(test, "==> Managers		: " + managers);
			Util.reportPassWithoutScreenshot(test, "==> ModifiedPolicy	: " + modifiedPolicy);
			Util.reportPassWithoutScreenshot(test, "==> \n");
			Util.reportPassWithoutScreenshot(test, "==> ###### Endpoints (by Platforms) #######\n");
			Util.reportPassWithoutScreenshot(test, "==> Windows		: " + windowsTotal);
			Util.reportPassWithoutScreenshot(test, "==> Mac		: " + macTotal);
			Util.reportPassWithoutScreenshot(test, "==> Mobile Device	: " + mobileDevice);
			Util.reportPassWithoutScreenshot(test, "==> All		: " + all);
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}    
	}
}