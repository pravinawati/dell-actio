package com.action.pages;

import java.util.Dictionary;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;

import com.properties.mapping.LoadProp;
import com.relevantcodes.extentreports.ExtentTest;
import com.utility.library.Global;
import com.utility.library.Util;

public class RecoverEndpointPage {
	
	public static void search_recovery_id(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
		Util.clear(LoadProp.recoverendpointprop, "searchbar_xpath", test, driver);
		//serve.clear(driver, By.xpath(recoverendpointprop.get("searchbar")), paramDict.get("text.search"));
		Util.sendKeys(LoadProp.recoverendpointprop, "searchbar_xpath",Global.endpoint_recovery_id, test, driver);
		WebElement elem = driver.findElementByXPath(LoadProp.recoverendpointprop.get("searchbar_xpath"));
		elem.sendKeys(Keys.ENTER);
		//driver.switchTo().defaultContent();
		//Util.waitUntilElementVisibility(driver, By.xpath("//*[@class='grid-link ng-binding' and contains(text(), '" + paramDict.get(testId+".search_text") + "')]"));
		Util.reportPass(driver,test, "Entity Searched!!");

		Thread.sleep(2000);
		
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
}
		}
