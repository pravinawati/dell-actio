package com.action.pages;

import java.io.IOException;
import java.util.Dictionary;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;

import com.properties.mapping.LoadProp;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.utility.library.Util;

public class AndroidBrowserTest {
	
	public static void ab_webui_login_fail(RemoteWebDriver driver,	Dictionary<String, String> paramDict, String testId, ExtentTest test) throws IOException {
		try {
			System.out.println("App Launched");
			test.log(LogStatus.INFO, "App launched");
			
			String weburl = LoadProp.getProperty("androidBrowser_weburl");
			//String username = paramDict.get(testId+".web_username");
			//String password = paramDict.get(testId+".web_password");
			//System.out.println(username+" - "+password);
			Util.access_link(weburl, test, driver);
			//Thread.sleep(2000);
			//driver.navigate ().to("javascript:document.getElementById('overridelink').click()");
			//Thread.sleep(2000);
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.loginpageprop.get("usernamefield_xpath")));
			//Util.clear(LoadProp.loginpageprop, "usernamefield_xpath", test, driver);
			Util.sendKeys(LoadProp.loginpageprop, "usernamefield_xpath", "superadmin", test, driver);
			test.log(LogStatus.INFO, "Username Entered");
			//Util.clear(LoadProp.loginpageprop, "passwordfield_xpath", test, driver);
			Util.sendKeys(LoadProp.loginpageprop, "passwordfield_xpath", "changeit", test, driver);
			test.log(LogStatus.INFO, "Password Entered");
			Util.click(LoadProp.loginpageprop, "login_btn_xpath", test, driver);
			test.log(LogStatus.INFO, "Clicked on Sign in button");
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.loginpageprop.get("gear_icon_xpath")));
			Util.reportPass(driver,test, "==> Login successfully");			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void ab_webui_login_pass(RemoteWebDriver driver,	Dictionary<String, String> paramDict, String testId, ExtentTest test) throws IOException {
		try {
			System.out.println("App Launched");
			test.log(LogStatus.INFO, "App launched");
			String weburl = LoadProp.getProperty("androidBrowser_weburl");
			Util.access_link(weburl, test, driver);	
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
