package com.action.pages;


import java.util.Dictionary;

import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import com.properties.mapping.LoadProp;
import com.relevantcodes.extentreports.ExtentTest;
import com.utility.library.AssertUtil;
import com.utility.library.Util;

public class LoginNavigateSearchNBEMobile {

	public static void a_login_to_nbe_mobile(RemoteWebDriver driver, Dictionary<String, String> paramDict , String testId, ExtentTest test)
	 throws Exception {

		driver.manage().deleteAllCookies();
		String weburl = LoadProp.getProperty("iosBrowser_weburl").toString();
				
		driver.get(weburl);
		String username = paramDict.get(testId+".username");
		String password = paramDict.get(testId+".password");
	
		Thread.sleep(15000);
		Util.click(LoadProp.mnbeproxibidprop,"m_left_menu_xpath",test, driver);
		Util.click(LoadProp.mnbeproxibidprop,"m_login_link_xpath", test, driver );
		Util.sendKeys(LoadProp.mnbeproxibidprop,"m_username_txt_xpath", username, test, driver);
		Util.sendKeys(LoadProp.mnbeproxibidprop,"m_password_txt_xpath", password,test, driver);
		Util.click(LoadProp.mnbeproxibidprop,"m_login_btn_xpath", test, driver);
		Thread.sleep(15000);
		Util.click(LoadProp.mnbeproxibidprop, "m_left_menu_xpath", test, driver);
				
		String loggedUserName = driver.findElementByXPath(LoadProp.mnbeproxibidprop.get("m_logged_username_xpath")).getText();
		Assert.assertEquals(loggedUserName, "sachibuyer2");
		Util.reportPass(driver,test,"==> Login is successfully");

	}
		
	public static void a_search_select_item_mobile(RemoteWebDriver driver, Dictionary<String, String> paramDict ,String testId, ExtentTest test)
				throws Exception {

		String itemsearch = paramDict.get(testId+".companyitemid");
					// click on Activity
		Util.click(LoadProp.mnbeproxibidprop, "m_menu_activity_xpath",test, driver);
		Thread.sleep(5000);
					// click on Offers
		Util.click(LoadProp.mnbeproxibidprop, "m_menu_activity_offers_xpath",test, driver);
		Thread.sleep(5000);
			// clikc on Refine
		Util.click(LoadProp.mnbeproxibidprop, "m_refine_menu_xpath",test, driver);
			// enter item to search
		Util.sendKeys(LoadProp.mnbeproxibidprop, "m_refine_search_txt_xpath", itemsearch, test, driver);
		Thread.sleep(5000);
			// select option from drop down
			// TODO
			// click on first search item
		Util.click(LoadProp.mnbeproxibidprop, "m_search_item_xpath",test, driver);
		Thread.sleep(2000);
		Util.reportPass(driver,test,"==> Item searched successfully!");

	}
		
	public static void a_logout_of_nbe_mobile(RemoteWebDriver driver, Dictionary<String, String> paramDict , String testId, ExtentTest test)
				throws Exception {
		SoftAssert sAssert= new SoftAssert();
		// expected logout text
		String expectedLogoutText = LoadProp.mnbeproxibidprop.get("m_logoutmessage");
			// click on left option to view the menu
		Util.click(LoadProp.mnbeproxibidprop, "m_left_menu_xpath",test, driver);
			// click to logout
		Util.click(LoadProp.mnbeproxibidprop, "m_logout_xpath",test, driver);
		Thread.sleep(15000);
			// fetch actual logout message from screen,
		String actualLogoutText = Util.getText(LoadProp.mnbeproxibidprop, "m_logouttext_xpath",test, driver);
			// assert the logout message
		AssertUtil.performSoftAssertEquals(expectedLogoutText, actualLogoutText, "Expected string- \"" + expectedLogoutText + "\" failed to match with actual string- \"" + actualLogoutText + "\"",  driver, test, sAssert);
		Thread.sleep(2000);
		sAssert.assertAll();
	}
	
	public static void s_navigate_to_offer_from_activity_mobile(RemoteWebDriver driver, Dictionary<String, String> paramDict ,String testId, ExtentTest test)
			throws Exception {

				// click on Activity
		Util.click(LoadProp.mnbeproxibidprop, "m_menu_activity_xpath",test, driver);
				// click on Offers
		Util.click(LoadProp.mnbeproxibidprop, "m_menu_activity_offers_xpath",test, driver);
		Thread.sleep(5000);

}
	
	public static void s_refine_search_select_item_mobile(RemoteWebDriver driver, String dropdownOption, String itemToBeSearch ,ExtentTest test)
			throws Exception {

			// click on Refine
		Util.click(LoadProp.mnbeproxibidprop, "m_refine_menu_xpath",test, driver);
			// enter item to search
		Util.sendKeys(LoadProp.mnbeproxibidprop, "m_refine_search_txt_xpath", itemToBeSearch, test, driver);
		Thread.sleep(5000);
		// select option from drop down
		// TODO
		Util.selectElementByVisibleText(driver, test, LoadProp.mnbeproxibidprop, "m_refine_status_dropdown_xpath", dropdownOption);
		Thread.sleep(5000);
		
		// click on first search item
		Util.click(LoadProp.mnbeproxibidprop, "m_search_item_xpath",test, driver);
		Thread.sleep(2000);
		Util.reportPass(driver,test,"==> Item searched and selected successfully!");

}
	
	public static void s_refine_search_item_mobile(RemoteWebDriver driver, String dropdownOption, String itemToBeSearch ,ExtentTest test)
			throws Exception {

			// click on Refine
		Util.click(LoadProp.mnbeproxibidprop, "m_refine_menu_xpath",test, driver);
			// enter item to search
		Util.sendKeys(LoadProp.mnbeproxibidprop, "m_refine_search_txt_xpath", itemToBeSearch, test, driver);
		Thread.sleep(5000);
		// select option from drop down
		// TODO
		Util.selectElementByVisibleText(driver, test, LoadProp.mnbeproxibidprop, "m_refine_status_dropdown_xpath", dropdownOption);
		Thread.sleep(5000);
		Util.reportPass(driver,test,"==> Item searched successfully!");

}
	
		
		
}
