package com.action.pages;

import java.io.IOException;
import java.util.Dictionary;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;

import com.properties.mapping.LoadProp;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.utility.library.Util;

public class CommitPage {
	
	public static void commit_policies(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		// TODO Auto-generated method stub
		try{
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.commitprop.get("commit_policies_required_xpath")));
			String total = driver.findElement(By.xpath(LoadProp.commitprop.get("commit_policies_required_xpath"))).getText();
			test.log(LogStatus.INFO, "Commit Policies Count:" + total);
			Util.click(LoadProp.commitprop, "btn_commit_xpath", test, driver);
			WebElement wrap_msg = driver.findElement(By.xpath(LoadProp.commitprop.get("wrap_msg_xpath")));
			Assert.assertEquals(wrap_msg.getText(), "Commit successful");
			Util.reportPass(driver, test, "Policies committed successfully!!");
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
		
		   
	}
	
}