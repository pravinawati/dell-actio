package com.action.pages;

import java.io.IOException;
import java.util.Dictionary;
import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;

import com.properties.mapping.LoadProp;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.utility.library.Util;

public class ComplianceReporterPage {
		
		public static void verify_cr_about_version(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
			try{
				Util.reportPass(driver,test, "About Box getting loaded");
				Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.compliancereporterprop.get("AboutBoxLink_ComplianceReporter_xpath")));
				Util.click(LoadProp.compliancereporterprop, "AboutBoxLink_ComplianceReporter_xpath", test, driver);
				
				String winHandleBefore = driver.getWindowHandle();
				for(String Winhandle : driver.getWindowHandles()){
					driver.switchTo().window(Winhandle);
				}
				
				WebElement element = driver.findElement(By.xpath(LoadProp.compliancereporterprop.get("VersionLink_ComplianceReporter_xpath")));
				String AboutVersion = element.getText();
				System.out.println("Compliance Reporter Version = "+AboutVersion);
				Assert.assertEquals("Release 9.11.0", AboutVersion);
						
				driver.close();
				driver.switchTo().window(winHandleBefore);
				test.log(LogStatus.INFO, "About Version verified successfully");
			} catch(Exception|AssertionError e){
				Assert.fail(e.getMessage());
			}
		}

		public static void verify_cr_about_copyright(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
			try{
				Util.reportPass(driver,test, "About Box getting loaded");
				Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.compliancereporterprop.get("AboutBoxLink_ComplianceReporter_xpath")));
				Util.click(LoadProp.compliancereporterprop, "AboutBoxLink_ComplianceReporter_xpath", test, driver);
				
				String winHandleBefore = driver.getWindowHandle();
				for(String Winhandle : driver.getWindowHandles()){
					driver.switchTo().window(Winhandle);
				}
				
				WebElement element = driver.findElement(By.xpath(LoadProp.compliancereporterprop.get("Trademark_xpath_ComplianceReporter_xpath")));
				String Trademark = element.getText();
				System.out.println(Trademark);
						Assert.assertEquals("� 2018 Dell Inc. All rights reserved."
								+ "\n" + "Dell Security Management Server is a trademark of Dell Inc. All other trademarks used herein are the property of their respective owners and are used for identification purposes only.", Trademark);

				driver.close();
				driver.switchTo().window(winHandleBefore);
				test.log(LogStatus.INFO, "About Copyright verified successfully");
			} catch(Exception|AssertionError e){
				Assert.fail(e.getMessage());
			}
		}
		
		public static void access_cr_help(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
			try{
				Util.reportPass(driver,test, "Compliance Reporter Help Version");
				Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.compliancereporterprop.get("CR_HelpFile_Link_xpath")));
				Util.click(LoadProp.compliancereporterprop, "CR_HelpFile_Link_xpath", test, driver);

				String subWindowHandler = null;
				Set<String> handles = driver.getWindowHandles();
				Iterator<String> iterator = handles.iterator();
				while (iterator.hasNext()){
					subWindowHandler = iterator.next();
					driver.switchTo().window(subWindowHandler);

					System.out.println(subWindowHandler);
					Util.reportPass(driver,test, "Entered into subwindow Help file");
				}
				Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.compliancereporterprop.get("CR_HelpHome_Link_xpath")));
			} catch(Exception|AssertionError e){
				Assert.fail(e.getMessage());
			}
		}
			
		public static void verify_cr_about_online_help_version(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
			try{
				Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.compliancereporterprop.get("CR_HelpHome_Link_xpath")));
				Util.click(LoadProp.compliancereporterprop, "CR_HelpHome_Link_xpath", test, driver);
				Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.compliancereporterprop.get("CR_Welcome_Link_xpath")));
				Util.click(LoadProp.compliancereporterprop, "CR_Welcome_Link_xpath", test, driver);
				Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.compliancereporterprop.get("Cr_About_OnlineHelpSystem_Link_xpath")));
				Util.click(LoadProp.compliancereporterprop, "Cr_About_OnlineHelpSystem_Link_xpath", test, driver);
				
				WebElement element = driver.findElement(By.xpath(LoadProp.compliancereporterprop.get("CR_About_OnlineHelpystem_Version_xpath")));
				String crhelpversion = element.getText();
				System.out.println(crhelpversion);
				Assert.assertEquals("9.11", crhelpversion);
				test.log(LogStatus.INFO, "Help About version verified successfully");
				//driver.close();
			} catch(Exception|AssertionError e){
				Assert.fail(e.getMessage());
			}
		}
		
		public static void open_and_run_report_complete(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
			try{
				int i = 1;
				int j = 1;
				while(i>=1){
					Thread.sleep(1000);
					while(j>0){
						Util.access_link(LoadProp.getProperty("crurl")+"executeReportDisplay.action", test, driver);
						if(driver.findElements(By.xpath(LoadProp.compliancereporterprop.get("Parent_Reports_Folder_xpath"))).size()!=0){
							break;
						}
						j++;
					}
					//Util.access_link(LoadProp.getProperty("crurl")+"executeReportDisplay.action", test, driver);
					//Util.access_link(LoadProp.getProperty("crurl")+"executeReportDisplay.action", test, driver);
					Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.compliancereporterprop.get("Parent_Reports_Folder_xpath")));
					Util.click(LoadProp.compliancereporterprop, "Parent_Reports_Folder_xpath", test, driver);
					//Thread.sleep(2000);
					Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.compliancereporterprop.get("Report_List_xpath")));
					if(paramDict.get(testId+".name"+i)!=null){
						//serve.click(driver, By.xpath("//div[@id='ext-gen87']/div/div[1]/div[2]/div/div/table/tbody/tr/td/div[contains(text(), 'Log Detail')]"));
						Util.waitUntilElementVisibility(driver, By.xpath("//table/tbody/tr/td/div[contains(text(), '"+paramDict.get(testId+".name"+i)+"')]"));
						driver.findElement(By.xpath("//table/tbody/tr/td/div[contains(text(), '"+paramDict.get(testId+".name"+i)+"')]")).click();
						Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.compliancereporterprop.get("open_report_xpath")));
						Util.click(LoadProp.compliancereporterprop, "open_report_xpath", test, driver);
						String subWindowHandler = null;
						Set<String> handles = driver.getWindowHandles();
						Iterator<String> iterator = handles.iterator();
						while (iterator.hasNext()){
							subWindowHandler = iterator.next();
							driver.switchTo().window(subWindowHandler);							
							//System.out.println(subWindowHandler);
							//Util.reportPass(driver,test, "Entered into subwindow Help file");
						}
						Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.compliancereporterprop.get("RunReport_xpath")));
						Util.reportPass(driver,test, "Report "+paramDict.get(testId+".name"+i)+" opened successfully");
				
						Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.compliancereporterprop.get("RunReport_xpath")));
						Util.click(LoadProp.compliancereporterprop, "RunReport_xpath", test, driver);
						Thread.sleep(2000);
						Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.compliancereporterprop.get("ReportResultHeader_xpath")));
				
						WebElement element = driver.findElement(By.xpath(LoadProp.compliancereporterprop.get("ReportResultHeader_xpath")));
						String ReportRunMsg = element.getText();
						Assert.assertEquals("Report Result: "+paramDict.get(testId+".name"+i), ReportRunMsg);
						test.log(LogStatus.INFO, "Verified report header is correct");
						Util.reportPass(driver,test, "Verified Report "+paramDict.get(testId+".name"+i)+" ran successfully");
						
					}else{
						break;
					}
					i++;
				}
			} catch(Exception|AssertionError e){
				Assert.fail(e.getMessage());
			}
		}
}
		      

