package com.action.pages;

import java.util.Dictionary;

import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;

import com.properties.mapping.LoadProp;
import com.relevantcodes.extentreports.ExtentTest;
import com.utility.library.Util;

public class LoginToNBE {
	
	/**
	 * Method to login into NBE
	 * @param driver RemoteWebDriver instance
	 * @param paramDict Parameter Dictionary 
	 * @param testId Current test case id
	 * @param test ExtentTest instance
	 */
	public static void a_login_to_nbe(RemoteWebDriver driver, Dictionary<String, String> paramDict , String testId, ExtentTest test) {
		try{
			String weburl = LoadProp.getProperty("web_weburl").toString();
			driver.get(weburl);
				// get user name and password
			String username = paramDict.get(testId+".username");
			String password=paramDict.get(testId+".password");
				// login to NBE
			Util.click(LoadProp.nbeproxibidprop, "nbe_login_xpath", test, driver);
			Util.sendKeys(LoadProp.nbeproxibidprop, "username_field_xpath", username, test, driver);
			Util.sendKeys(LoadProp.nbeproxibidprop, "password_field_xpath", password, test, driver );
			Util.click(LoadProp.nbeproxibidprop, "login_btn_xpath", test, driver);
				// report pass if login is success
			Util.reportPass(driver,test,"==> Login is successfully");
			Thread.sleep(3000);
		} catch(Exception|AssertionError e){
			s_logout_incase_of_failure_nbe(driver, test);
			Assert.fail(e.getMessage());
		}
	}
	

	/**
	 * Method to logout of NBE
	 * @param driver RemoteWebDriver instance
	 * @param paramDict Parameter Dictionary 
	 * @param testId Current test case id
	 * @param test ExtentTest instance
	 */
	public static void a_logout_of_nbe(RemoteWebDriver driver, Dictionary<String, String> paramDict ,  String testId, ExtentTest test) {
		try{
			
			Thread.sleep(10000);
			// click on the user name at top and logout of NBE
			Util.click(LoadProp.nbeproxibidprop,"user_xpath", test, driver);
			Util.click(LoadProp.nbeproxibidprop, "logout_btn_xpath", test, driver);
			Util.reportPass(driver,test,"==> Logout successfully");
			Thread.sleep(3000);
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
	/**
	 * Method to logout of NBE application in case any error occurs. So that next test starts from the login screen again.
	 * @param driver RemoteWebDriver instance 
	 * @param test ExtentTest instance
	 */
	public static void s_logout_incase_of_failure_nbe(RemoteWebDriver driver, ExtentTest test){
		try{
			Util.click(LoadProp.nbeproxibidprop, "user_xpath", test, driver);
			Util.click(LoadProp.nbeproxibidprop, "logout_btn_xpath", test, driver);
			Thread.sleep(3000);
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
}
