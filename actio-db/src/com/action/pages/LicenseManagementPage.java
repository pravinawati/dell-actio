package com.action.pages;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.Dictionary;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;

import com.properties.mapping.LoadProp;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.utility.library.Global;
import com.utility.library.Util;

public class LicenseManagementPage {
	
	//Login to Web Application
	public static void upload_license(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			Thread.sleep(1000);
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.licensemngmtprop.get("choose_file_button_xpath")));
			Util.click(LoadProp.licensemngmtprop, "choose_file_button_xpath", test, driver);
			Thread.sleep(2000);
			StringSelection ss = new StringSelection(Global.PROJECT_LOCATION+"cals\\" + paramDict.get(testId+".licensetype") + ".xml");
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			Thread.sleep(500);
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			Thread.sleep(1000);
			robot.keyPress(KeyEvent.VK_TAB);
			robot.keyRelease(KeyEvent.VK_TAB);
			Thread.sleep(1000);
			robot.keyPress(KeyEvent.VK_TAB);
			robot.keyRelease(KeyEvent.VK_TAB);
			Thread.sleep(500);
			robot.keyPress(KeyEvent.VK_O);
			robot.keyRelease(KeyEvent.VK_O);
			Thread.sleep(1000);
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
	public static void upload_invalid_license(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			Thread.sleep(1000);
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.licensemngmtprop.get("choose_file_button_xpath")));
			Util.click(LoadProp.licensemngmtprop, "choose_file_button_xpath", test, driver);
			Thread.sleep(2000);
			StringSelection ss = new StringSelection(Global.PROJECT_LOCATION+"cals\\invalid\\" + paramDict.get(testId+".licensetype") + ".xml");
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			Thread.sleep(500);
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			Thread.sleep(1000);
			robot.keyPress(KeyEvent.VK_TAB);
			robot.keyRelease(KeyEvent.VK_TAB);
			Thread.sleep(1000);
			robot.keyPress(KeyEvent.VK_TAB);
			robot.keyRelease(KeyEvent.VK_TAB);
			Thread.sleep(500);
			robot.keyPress(KeyEvent.VK_O);
			robot.keyRelease(KeyEvent.VK_O);
			Thread.sleep(1000);
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
	public static void upload_edited_license(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			Thread.sleep(1000);
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.licensemngmtprop.get("choose_file_button_xpath")));
			Util.click(LoadProp.licensemngmtprop, "choose_file_button_xpath", test, driver);
			Thread.sleep(2000);
			StringSelection ss = new StringSelection(Global.PROJECT_LOCATION+"cals\\edited\\" + paramDict.get(testId+".licensetype") + ".xml");
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			Thread.sleep(500);
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			Thread.sleep(1000);
			robot.keyPress(KeyEvent.VK_TAB);
			robot.keyRelease(KeyEvent.VK_TAB);
			Thread.sleep(1000);
			robot.keyPress(KeyEvent.VK_TAB);
			robot.keyRelease(KeyEvent.VK_TAB);
			Thread.sleep(500);
			robot.keyPress(KeyEvent.VK_O);
			robot.keyRelease(KeyEvent.VK_O);
			Thread.sleep(1000);
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
	//Default License Check
	
	public static void check_default_license(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			String count;
			String caltype = null;

			/*int i=1;
			while(i>=1){
				
				for (int j=1; j<=6; j++){
					count = driver.findElement(By.xpath("//tbody[@role='rowgroup']//tr[" + i + "]//td[" + j + "]")).getText();
					//System.out.print(count + "\t");
					if(j==1){
						caltype = driver.findElement(By.xpath("//tbody[@role='rowgroup']//tr[" + i + "]//td[" + j + "]")).getText();
					}
					if(j==5){
						Assert.assertEquals(count, "10");
						Util.reportPass(driver,test, "Default Licence count of " + caltype + " is correct, 10!");
					}
				}	
				i++;
			}*/
			for(int i=1;i<=6;i++){
				for (int j=1; j<=6; j++){
					count = driver.findElement(By.xpath("//tbody[@role='rowgroup']//tr[" + i + "]//td[" + j + "]")).getText();
					//System.out.print(count + "\t");
					if(j==2){
						caltype = driver.findElement(By.xpath("//tbody[@role='rowgroup']//tr[" + i + "]//td[" + j + "]")).getText();
					}
					if(j==5){
						Assert.assertEquals(count, "10");
						Util.reportPass(driver,test, "Default Licence count of " + caltype + " is correct, 10!");
					}
				}
			}
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
	public static void delete_license(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			String licenseTypeCode = paramDict.get(testId+".licensetype");
			System.out.println(licenseTypeCode);
			String licenseType = null;
			
			if(licenseTypeCode.equals("tp")){
				licenseType = "Threat Protection";
			}
			if(licenseTypeCode.equals("atp")){
				licenseType = "Advanced Threat Prevention";
			}
			if(licenseTypeCode.equals("ee")){
				licenseType = "Enterprise Edition";
			}
			if(licenseTypeCode.equals("dg")){
				licenseType = "Data Guardian";
			}
			if(licenseTypeCode.equals("me")){
				licenseType = "Mobile Edition";
			}
			if(licenseTypeCode.equals("blm")){
				licenseType = "BitLocker Manager";
			}
			
			Util.waitUntilElementVisibility(driver, By.xpath("//td/span[contains(text(), '" + licenseType + "')]/../../td[7]/button"));
			driver.findElement(By.xpath("//td/span[contains(text(), '" + licenseType + "')]/../../td[7]/button")).click();
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.licensemngmtprop.get("Confirm_Delete_License_xpath")));
			Util.click(LoadProp.licensemngmtprop, "Confirm_Delete_License_xpath", test, driver);
			if(driver.findElements(By.xpath("//td/span[contains(text(), '" + licenseType + "')]/../../td[7]/button")).size()==0){
				Util.reportPass(driver,test, "Licence " + licenseType + " deleted successfully");
			}
			
			/*switch (licenseTypeCode) {
				case "tp":	
					licenseType = "Threat Protection";
					Util.waitUntilElementVisibility(driver, By.xpath("//td/span[contains(text(), '" + licenseType + "')]/../../td[7]/button"));
					driver.findElement(By.xpath("//td/span[contains(text(), '" + licenseType + "')]/../../td[7]/button")).click();
					Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.licensemngmtprop.get("Confirm_Delete_License_xpath")));
					Util.click(LoadProp.licensemngmtprop, "Confirm_Delete_License_xpath", test, driver);
					if(driver.findElements(By.xpath("//td/span[contains(text(), '" + licenseType + "')]/../../td[7]/button")).size()==0){
						Util.reportPass(driver,test, "Licence " + licenseType + " deleted successfully");
					}
					break;
				case "atp":	
					licenseType = "Advanced Threat Prevention";
					Util.waitUntilElementVisibility(driver, By.xpath("//td/span[contains(text(), '" + licenseType + "')]/../../td[7]/button"));
					driver.findElement(By.xpath("//td/span[contains(text(), '" + licenseType + "')]/../../td[7]/button")).click();
					Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.licensemngmtprop.get("Confirm_Delete_License_xpath")));
					Util.click(LoadProp.licensemngmtprop, "Confirm_Delete_License_xpath", test, driver);
					if(driver.findElements(By.xpath("//td/span[contains(text(), '" + licenseType + "')]/../../td[7]/button")).size()==0){
						Util.reportPass(driver,test, "Licence " + licenseType + " deleted successfully");
					}
					break;
				case "ee":	
					licenseType = "Enterprise Edition";
					Util.waitUntilElementVisibility(driver, By.xpath("//td/span[contains(text(), '" + licenseType + "')]/../../td[7]/button"));
					driver.findElement(By.xpath("//td/span[contains(text(), '" + licenseType + "')]/../../td[7]/button")).click();
					Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.licensemngmtprop.get("Confirm_Delete_License_xpath")));
					Util.click(LoadProp.licensemngmtprop, "Confirm_Delete_License_xpath", test, driver);
					if(driver.findElements(By.xpath("//td/span[contains(text(), '" + licenseType + "')]/../../td[7]/button")).size()==0){
						Util.reportPass(driver,test, "Licence " + licenseType + " deleted successfully");
					}
					break;
				case "dg":	
					licenseType = "Data Guardian";
					Util.waitUntilElementVisibility(driver, By.xpath("//td/span[contains(text(), '" + licenseType + "')]/../../td[7]/button"));
					driver.findElement(By.xpath("//td/span[contains(text(), '" + licenseType + "')]/../../td[7]/button")).click();
					Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.licensemngmtprop.get("Confirm_Delete_License_xpath")));
					Util.click(LoadProp.licensemngmtprop, "Confirm_Delete_License_xpath", test, driver);
					if(driver.findElements(By.xpath("//td/span[contains(text(), '" + licenseType + "')]/../../td[7]/button")).size()==0){
						Util.reportPass(driver,test, "Licence " + licenseType + " deleted successfully");
					}
					break;
				case "me":	
					licenseType = "Mobile Edition";
					Util.waitUntilElementVisibility(driver, By.xpath("//td/span[contains(text(), '" + licenseType + "')]/../../td[7]/button"));
					driver.findElement(By.xpath("//td/span[contains(text(), '" + licenseType + "')]/../../td[7]/button")).click();
					Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.licensemngmtprop.get("Confirm_Delete_License_xpath")));
					Util.click(LoadProp.licensemngmtprop, "Confirm_Delete_License_xpath", test, driver);
					if(driver.findElements(By.xpath("//td/span[contains(text(), '" + licenseType + "')]/../../td[7]/button")).size()==0){
						Util.reportPass(driver,test, "Licence " + licenseType + " deleted successfully");
					}
					break;
				case "blm":	
					licenseType = "BitLocker Manager";
					Util.waitUntilElementVisibility(driver, By.xpath("//td/span[contains(text(), '" + licenseType + "')]/../../td[7]/button"));
					driver.findElement(By.xpath("//td/span[contains(text(), '" + licenseType + "')]/../../td[7]/button")).click();
					Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.licensemngmtprop.get("Confirm_Delete_License_xpath")));
					Util.click(LoadProp.licensemngmtprop, "Confirm_Delete_License_xpath", test, driver);
					if(driver.findElements(By.xpath("//td/span[contains(text(), '" + licenseType + "')]/../../td[7]/button")).size()==0){
						Util.reportPass(driver,test, "Licence " + licenseType + " deleted successfully");
					}
					break;
				case "":	
					licenseType = "";
					Util.waitUntilElementVisibility(driver, By.xpath("//td/span[contains(text(), '" + licenseType + "')]/../../td[7]/button"));
					driver.findElement(By.xpath("//td/span[contains(text(), '" + licenseType + "')]/../../td[7]/button")).click();
					Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.licensemngmtprop.get("Confirm_Delete_License_xpath")));
					Util.click(LoadProp.licensemngmtprop, "Confirm_Delete_License_xpath", test, driver);
					if(driver.findElements(By.xpath("//td/span[contains(text(), '" + licenseType + "')]/../../td[7]/button")).size()==0){
						Util.reportPass(driver,test, "Licence " + licenseType + " deleted successfully");
					}
					break;
			}*/
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
	public static void upload_duplicate_license(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			Thread.sleep(1000);
			
			String licenseType;
			String license = null;
			licenseType = paramDict.get(testId+".licensetype");
			if(licenseType.equals("tp")){
				license = "Threat Protection";
			}
			if(licenseType.equals("atp")){
				license = "Advanced Threat Prevention";
			}
			if(licenseType.equals("ee")){
				license = "Enterprise Edition";
			}
			if(licenseType.equals("dg")){
				license = "Data Guardian";
			}
			if(licenseType.equals("me")){
				license = "Mobile Edition";
			}
			if(licenseType.equals("blm")){
				license = "BitLocker Manager";
			}
			if(driver.findElements(By.xpath("//td/span[contains(text(), '" + license + "')]/../../td[7]/button")).size()==0){
				upload_license(driver, paramDict, testId, test);
				verify_valid_license_upload(driver, paramDict, testId, test);
			}
			
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.licensemngmtprop.get("choose_file_button_xpath")));
			Util.click(LoadProp.licensemngmtprop, "choose_file_button_xpath", test, driver);
			Thread.sleep(2000);
			StringSelection ss = new StringSelection(Global.PROJECT_LOCATION+"cals\\" + paramDict.get(testId+".licensetype") + ".xml");
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
			Robot robot = new Robot();
			
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			Thread.sleep(500);
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			Thread.sleep(1000);
			robot.keyPress(KeyEvent.VK_TAB);
			robot.keyRelease(KeyEvent.VK_TAB);
			Thread.sleep(1000);
			robot.keyPress(KeyEvent.VK_TAB);
			robot.keyRelease(KeyEvent.VK_TAB);
			Thread.sleep(500);
			robot.keyPress(KeyEvent.VK_O);
			robot.keyRelease(KeyEvent.VK_O);
			Thread.sleep(1000);
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
	public static void verify_valid_license_upload(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.licensemngmtprop.get("success_license_upload_msg_xpath")));
			Util.click(LoadProp.licensemngmtprop, "confirm_file_upload_button_xpath", test, driver);
			Util.reportPass(driver,test,"License uploaded successfully.");
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
	public static void verify_duplicate_license_upload(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.licensemngmtprop.get("duplicate_license_upload_msg_xpath")));
			Util.click(LoadProp.licensemngmtprop, "confirm_file_upload_button_xpath", test, driver);
			Util.reportPass(driver,test,"The license has already been uploaded.");
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
	public static void verify_invalid_license_upload(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.licensemngmtprop.get("invalid_license_upload_msg_xpath")));
			Util.click(LoadProp.licensemngmtprop, "confirm_file_upload_button_xpath", test, driver);
			Util.reportPass(driver,test,"Invalid License File.");
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
	public static void verify_edited_license_upload(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.licensemngmtprop.get("edited_license_upload_msg_xpath")));
			Util.click(LoadProp.licensemngmtprop, "confirm_file_upload_button_xpath", test, driver);
			Util.reportPass(driver,test,"The license has an invalid signature.");
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
}
