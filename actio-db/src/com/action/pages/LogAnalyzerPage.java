package com.action.pages;

import java.util.Dictionary;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;

import com.properties.mapping.LoadProp;
import com.relevantcodes.extentreports.ExtentTest;
import com.utility.library.Util;

public class LogAnalyzerPage {
		
		public static void search_admin_action_logs(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
			try{
				String category;
				Util.waitUntilElementVisibility(driver,By.xpath(LoadProp.loganalyzerprop.get("LogAnalyser_Category_Expand_xpath")));
				Util.click(LoadProp.loganalyzerprop, "LogAnalyser_Category_Expand_xpath", test, driver);
				Util.reportPass(driver,test, "Log Analyzer Category Expanded");
				Util.waitUntilElementVisibility(driver,By.xpath(LoadProp.loganalyzerprop.get("LogCategory_AdminActions_xpath")));
				Util.click(LoadProp.loganalyzerprop, "LogCategory_AdminActions_xpath", test, driver);
				Util.reportPass(driver,test, "Clicked on Log Analyzer Category Admin Actions");
				Util.waitUntilElementVisibility(driver,By.xpath(LoadProp.loganalyzerprop.get("Click_Search_xpath")));
				Util.click(LoadProp.loganalyzerprop, "Click_Search_xpath", test, driver);
				Util.reportPass(driver,test, "Clicked on Log Analyzer Search Button");
				
				int i=1;
				while(i>=1){
					int getElement = driver.findElements(By.xpath("//tbody[@role='rowgroup']//tr[" + i + "]//td[5]")).size();
					if(getElement!=0){
						category = driver.findElement(By.xpath("//tbody[@role='rowgroup']//tr[" + i + "]//td[5]")).getText();
						Assert.assertEquals(category, "Admin Actions");
						i++;
					}else{
						break;
					}
				}
				Util.reportPass(driver,test, "Verified all searched logs are Admin Actions categorized");
			} catch(Exception|AssertionError e){
				Assert.fail(e.getMessage());
			}
		}
		      
		public static void search_system_logs(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
			try{
				String category;
				Util.waitUntilElementVisibility(driver,By.xpath(LoadProp.loganalyzerprop.get("LogAnalyser_Category_Expand_xpath")));
				Util.click(LoadProp.loganalyzerprop, "LogAnalyser_Category_Expand_xpath", test, driver);
				Util.reportPass(driver,test, "Log Analyzer Category Expanded");
				Util.waitUntilElementVisibility(driver,By.xpath(LoadProp.loganalyzerprop.get("LogCategory_SystemLogs_xpath")));
				Util.click(LoadProp.loganalyzerprop, "LogCategory_SystemLogs_xpath", test, driver);
				Util.reportPass(driver,test, "Clicked on Log Analyzer Category Admin Actions");
				Util.waitUntilElementVisibility(driver,By.xpath(LoadProp.loganalyzerprop.get("Click_Search_xpath")));
				Util.click(LoadProp.loganalyzerprop, "Click_Search_xpath", test, driver);
				Util.reportPass(driver,test, "Clicked on Log Analyzer Search Button");
				
				int i=1;
				while(i>=1){
					int getElement = driver.findElements(By.xpath("//tbody[@role='rowgroup']//tr[" + i + "]//td[5]")).size();
					if(getElement!=0){
						category = driver.findElement(By.xpath("//tbody[@role='rowgroup']//tr[" + i + "]//td[5]")).getText();
						Assert.assertEquals(category, "System Logs");
						i++;
					}else{
						break;
					}
				}
				Util.reportPass(driver,test, "Verified all searched logs are System logs categorized");
			} catch(Exception|AssertionError e){
				Assert.fail(e.getMessage());
			}
		}
		         
}