package com.action.pages;

import java.util.Dictionary;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;

import com.properties.mapping.LoadProp;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.utility.library.Util;

public class EndpointGroupsPage {

	//Add Rule Defined Group
	/* prop xls file
	 * 
	 * addendpointgroup | group_name | Krishna
	 * addendpointgroup | group_spec | CATEGORY = "WINDOWS"
	 * 
	 */
	public static void add_rule_defined_group(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		
		try{
			//Add Endpoint Group
			test.log(LogStatus.INFO, "Add Rule Defined Group");
			Util.click(LoadProp.endpointgroupsprop, "endpointAddBtn_xpath", test, driver);
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.endpointgroupsprop.get("addEndpointGroupPage_xpath")));
			
			String group_name = paramDict.get(testId+".group_name");
			String group_spec = paramDict.get(testId+".group_spec");
	        String group_des = "You only automate a test not testing...";
			
			//Rule Define
	        Util.click(LoadProp.endpointgroupsprop, "selectGroupType_xpath", test, driver);
			driver.findElement(By.xpath("//ul[@id='endpoint-class_listbox']/*[starts-with(text(), 'RULE-DEFINED Group')]")).click();
			Util.sendKeys(LoadProp.endpointgroupsprop, "endpointGroupName_xpath", group_name, test, driver);
			Util.sendKeys(LoadProp.endpointgroupsprop, "endpointDes_xpath", group_des, test, driver);
			Util.sendKeys(LoadProp.endpointgroupsprop, "endpointSpec_xpath", group_spec, test, driver);
			
			//Preview
			Util.click(LoadProp.endpointgroupsprop, "endpointPreview_xpath", test, driver);
			
			//Endpoint Group Count
			List<WebElement> page = driver.findElements(By.xpath("//tbody[@role='rowgroup']/tr"));
			String endpoint_group_page_size = String.valueOf(page.size());
			test.log(LogStatus.INFO, "Preview Endpoint: "+ endpoint_group_page_size);
			
			//Add Endpoint Group
			Util.click(LoadProp.endpointgroupsprop, "endpointAddGroupBtn_xpath", test, driver);
			//role="dialog"
			Util.waitUntilElementVisibility(driver, By.xpath("//div[@role='dialog']"));
			//ddpDialogTitle
			String addEndpointGroupDilog = driver.findElement(By.xpath(LoadProp.endpointgroupsprop.get("ddpDialogTitle_xpath"))).getText();
			test.log(LogStatus.INFO, addEndpointGroupDilog);
			driver.findElement(By.xpath("//button[starts-with(@id,'resolveButton')]")).click();
			
			//Alert Message
			String add_usergroup_alert = driver.findElement(By.xpath("//div[@id='notificationPanel']")).getText();
			test.log(LogStatus.INFO, add_usergroup_alert);
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.endpointgroupsprop.get("addEndpointGroupPage_xpath")));

			//cancel
			Util.click(LoadProp.endpointgroupsprop, "endpointcancelBtn_xpath", test, driver);
			
			//Check Endpoint members  == 
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.endpointgroupsprop.get("endpointGroupPage_xpath")));
			
			//role="rowgroup"
			//Assert User Group Name
			List<WebElement> endpoint_group_page = driver.findElements(By.xpath("//div[@data-role='pager']/ul/li"));
		    String page_size = String.valueOf(endpoint_group_page.size());
			int ac_page_size = Integer.parseInt(page_size) - 1;
			
			for(int i=1; i<=ac_page_size; i++){
				int optGroup = 1;
				String find_group = "false";
				test.log(LogStatus.INFO, "Find Group: " + find_group);
				
				for(int j=1; j<=25; j++){
					String row = driver.findElement(By.xpath("//tbody[@role='rowgroup']/tr["+ optGroup +"]/td[1]")).getText();
					test.log(LogStatus.INFO, "Row: " + row);
					//Find User
					if(row.contentEquals(group_name)){
						find_group = "true";
						String endpoint_row = driver.findElement(By.xpath("//tbody[@role='rowgroup']/tr["+ optGroup +"]/td[2]")).getText();
						test.log(LogStatus.INFO, "Preview Endpoint: "+ endpoint_group_page_size);
						test.log(LogStatus.INFO, "Members Endpoint: "+ endpoint_row);
						
						if(endpoint_row.contentEquals(endpoint_group_page_size)){
							test.log(LogStatus.INFO, "Successfully match endpoint group members count...");
						}else{
							Util.reportPass(driver,test, "Failed to match endpoint group members count...");
							Assert.assertEquals("", "none");
						}
						
						test.log(LogStatus.INFO, "Find User: " + find_group);
						break;
					}
					optGroup++;
				}
				
				//Find User
				if(find_group == "true"){
					Util.reportPass(driver,test, "Find User: " + find_group);
					break;
				}else{
					Util.reportPass(driver,test, "Find User: " + find_group);
					test.log(LogStatus.INFO, "Pagination Next Page Users"+ i);
					Util.click(LoadProp.endpointgroupsprop, "paginationNextPageUsers_xpath", test, driver);
				}
			}
			
			//check Commit Count == 1
			Util.access_link(LoadProp.getProperty("web_weburl")+"manageCommit", test, driver);
			test.log(LogStatus.INFO, "Commit link clicked!!");
					
			//Get Count
			String c_count = driver.findElement(By.xpath("//p[@id='policyCommitRequired']")).getText();
			String acc_c_count = "Pending Policy Changes: 1";
					
			if(c_count.contentEquals(acc_c_count)){
				test.log(LogStatus.INFO, "Pending Policy Changes: 1");
				driver.findElement(By.xpath("//button[@id='commitPolicies']")).click();
				test.log(LogStatus.INFO, "Successfully Commit Pending Policy");
			}else{
				test.log(LogStatus.INFO, c_count);
				test.log(LogStatus.INFO, "Failed to match Commit count..");
				Assert.assertEquals("", "none");
			}
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
	//Add Admin Defined Group
	public void add_admin_defined_group(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		
		try{
			//Add Endpoint Group
			test.log(LogStatus.INFO, "Add Rule Defined Group");
			Util.click(LoadProp.endpointgroupsprop, "endpointAddBtn_xpath", test, driver);
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.endpointgroupsprop.get("addEndpointGroupPage_xpath")));
					
			String group_name = paramDict.get(testId+".group_name");
			String group_des = "You only automate a test not testing...";
					
			//Rule Define
			Util.click(LoadProp.endpointgroupsprop, "selectGroupType_xpath", test, driver);
			driver.findElement(By.xpath("//ul[@id='endpoint-class_listbox']/*[starts-with(text(), 'ADMIN-DEFINED Group')]")).click();
			Util.sendKeys(LoadProp.endpointgroupsprop, "adminDefineGroupName_xpath", group_name, test, driver);
			Util.sendKeys(LoadProp.endpointgroupsprop, "adminDefineDes_xpath", group_des, test, driver);
			
			//ADD Endpoint Group
			Util.click(LoadProp.endpointgroupsprop, "adminDEfineAddButton_xpath", test, driver);
			
			//role="dialog"
			Util.waitUntilElementVisibility(driver, By.xpath("//div[@role='dialog']"));

			//ddpDialogTitle
			String addEndpointGroupDilog = driver.findElement(By.xpath(LoadProp.endpointgroupsprop.get("ddpDialogTitle_xpath"))).getText();
			test.log(LogStatus.INFO, addEndpointGroupDilog);
			driver.findElement(By.xpath("//button[starts-with(@id,'resolveButton')]")).click();
					
			//Alert Message
			String add_usergroup_alert = driver.findElement(By.xpath("//div[@id='notificationPanel']")).getText();
			test.log(LogStatus.INFO, add_usergroup_alert);
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.endpointgroupsprop.get("addEndpointGroupPage_xpath")));

			//cancel
			Util.click(LoadProp.endpointgroupsprop, "endpointcancelBtn_xpath", test, driver);
			
			//Assert Endpoint Group Name
			List<WebElement> page = driver.findElements(By.xpath("//div[@data-role='pager']/ul/li"));
			String page_size = String.valueOf(page.size());
			int ac_page_size = Integer.parseInt(page_size) - 1;
					
			for(int i=1; i<=ac_page_size; i++){
					int optGroup = 1;
					String find_group = "false";
					test.log(LogStatus.INFO, "Find Group: " + find_group);
						
				for(int j=1; j<=25; j++){
					String row = driver.findElement(By.xpath("//tbody[@role='rowgroup']/tr["+ optGroup +"]/td[1]")).getText();
					test.log(LogStatus.INFO, "Row: " + row);
					//Find User
					if(row.contentEquals(group_name)){
						find_group = "true";
						test.log(LogStatus.INFO, "Find User: " + find_group);
						break;
						}
							optGroup++;
						}
						
			//Find User
			if(find_group == "true"){
				Util.reportPass(driver,test, "Find User: " + find_group);
				break;
			}else{
				Util.reportPass(driver,test, "Find User: " + find_group);
				test.log(LogStatus.INFO, "Pagination Next Page Users"+ i);
				Util.click(LoadProp.endpointgroupsprop, "paginationNextPageUsers_xpath", test, driver);
				}
			}
			
			//check Commit Count == 1
			Util.access_link(LoadProp.getProperty("web_weburl")+"manageCommit", test, driver);
			test.log(LogStatus.INFO, "Commit link clicked!!");
							
			//Get Count
			String c_count = driver.findElement(By.xpath("//p[@id='policyCommitRequired']")).getText();
			String acc_c_count = "Pending Policy Changes: 1";
							
			if(c_count.contentEquals(acc_c_count)){
				test.log(LogStatus.INFO, "Pending Policy Changes: 1");
				driver.findElement(By.xpath("//button[@id='commitPolicies']")).click();
				test.log(LogStatus.INFO, "Successfully Commit Pending Policy");
			}else{
				test.log(LogStatus.INFO, c_count);
				test.log(LogStatus.INFO, "Failed to match Commit count..");
				Assert.assertEquals("", "none");
			}
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
}
