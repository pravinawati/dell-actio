package com.action.pages;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.Dictionary;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;

import com.properties.mapping.LoadProp;
import com.relevantcodes.extentreports.ExtentTest;
import com.utility.library.Util;

public class PolicyPage {
	
	//Login to Web Application
	public static void setup_policy(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			Util.reportPass(driver,test,"Set Policy beings");
			Util.waitUntilElementVisibility(driver,By.xpath(LoadProp.policyprop.get("searched_endpoint_link_xpath")));
			Util.click(LoadProp.policyprop, "searched_endpoint_link_xpath", test, driver);
			Thread.sleep(2000);
			Util.reportPass(driver,test, "Click on Windows Authentication link");
			Util.waitUntilElementVisibility(driver,By.xpath(LoadProp.policyprop.get("windows_authentication_link_xpath")));
			Util.click(LoadProp.policyprop, "windows_authentication_link_xpath", test, driver);
			Util.waitUntilElementVisibility(driver,By.xpath(LoadProp.policyprop.get("add_rule_button_xpath")));
			Util.click(LoadProp.policyprop, "add_rule_button_xpath", test, driver);
			Thread.sleep(500);
			Util.click(LoadProp.policyprop, "click_second_dropdown_xpath", test, driver);
			Robot robot = new Robot();
			
			robot.keyPress(KeyEvent.VK_DOWN);
			robot.keyRelease(KeyEvent.VK_DOWN);
			Thread.sleep(500);
			robot.keyPress(KeyEvent.VK_DOWN);
			robot.keyRelease(KeyEvent.VK_DOWN);
			Thread.sleep(500);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyPress(KeyEvent.VK_ENTER);
			Util.waitUntilElementVisibility(driver,By.xpath(LoadProp.policyprop.get("add_rule_button_xpath")));
			Util.click(LoadProp.policyprop, "add_rule_button_xpath", test, driver);
			Thread.sleep(500);
			Util.click(LoadProp.policyprop, "click_third_dropdown_xpath", test, driver);
			robot.keyPress(KeyEvent.VK_DOWN);
			robot.keyRelease(KeyEvent.VK_DOWN);
			Thread.sleep(500);
			robot.keyPress(KeyEvent.VK_DOWN);
			robot.keyRelease(KeyEvent.VK_DOWN);
			Thread.sleep(500);
			robot.keyPress(KeyEvent.VK_DOWN);
			robot.keyRelease(KeyEvent.VK_DOWN);
			Thread.sleep(500);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyPress(KeyEvent.VK_ENTER);
			Thread.sleep(500);
			Util.waitUntilElementVisibility(driver,By.xpath(LoadProp.policyprop.get("save_policy_xpath")));
			Util.click(LoadProp.policyprop, "save_policy_xpath", test, driver);
			Util.reportPass(driver,test, "after save policy");
			//serve.waitUntil(ExpectedConditions.presenceOfElementLocated(By.xpath(PolicyPageProp.get("policy_confirmation_message"))), driver, 100);
			//Util.reportPass(driver,test, "after save policy");
			Util.click(LoadProp.policyprop, "return_link_xpath", test, driver);
			Util.reportPass(driver,test, "Return link clicked");
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
	public static void commit_policy(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			Util.reportPass(driver,test,"Commit Policy begins");
			Util.waitUntilElementVisibility(driver,By.xpath(LoadProp.policyprop.get("commit_comments_xpath")));
			Util.sendKeys(LoadProp.policyprop, "commit_comments_xpath", "test", test, driver);
			Util.click(LoadProp.policyprop, "commit_policies_button_xpath", test, driver);
			Thread.sleep(2000);
			//serve.waitUntil(ExpectedConditions.presenceOfElementLocated(By.xpath(PolicyPageProp.get("commit_successful_message"))), driver, 100);
			//serve.click(driver, By.xpath(PolicyPageProp.get("windows_authentication_link")));
			Util.reportPass(driver,test,"Policy committed successfully");
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
}
