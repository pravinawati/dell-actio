package com.action.pages;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Dictionary;

import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import com.properties.mapping.LoadProp;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.utility.library.AssertUtil;
import com.utility.library.Util;

public class NBEActionMethods {
	
	static DecimalFormat decimalFormat = new DecimalFormat("#,###.##");

	/**
	 * Method to accept the offer by buyer
	 * @param driver Driver instance
	 * @param paramDict parameter dictionary
	 * @param testId Current test case id
	 * @param test Extent report instance
	 * @throws IOException exception
	 * @throws InterruptedException exception
	 */
	public static void a_accept_offer_buyer_nbe(RemoteWebDriver driver,	Dictionary<String, String> paramDict, String testId, ExtentTest test)
			throws IOException, InterruptedException 
	{			
		try {
			// fetch the company item id
			String itemIdToBeSearch = paramDict.get(testId+".companyitemid");
			
			// search and select the item
		NBESearchItem.s_search_select_item_nbe(driver, paramDict, test, "Active Offers", itemIdToBeSearch);
			// Click on the action button
		Util.click(LoadProp.nbeproxibidprop ,"accept_offer_btn_xpath", test, driver);
			// Confirm the action
		Util.click(LoadProp.nbeproxibidprop, "confirm_action_xpath", test, driver);
		Thread.sleep(10000);

			// Soft assert the Status, Status message, Original Asking Price, Current Offer Price, Transaction History Message and Amount
		SoftAssert sAssert= new SoftAssert();
		s_validate_status(LoadProp.nbeproxibidprop.get("accept_status"), test, driver, sAssert);
		s_validate_status_message(LoadProp.nbeproxibidprop.get("accept_message"), test, driver, sAssert);
		s_validate_original_asking_price("$" + decimalFormat.format(Double.parseDouble(paramDict.get(testId+".listprice"))) + ".00", test, driver, sAssert);
		s_validate_current_offer_price("$" + decimalFormat.format(Double.parseDouble(paramDict.get(testId+".sellercounteramount"))) + ".00", test, driver, sAssert);
		s_validate_transaction_history_message(LoadProp.nbeproxibidprop.get("accept_history_message") + " " + LoadProp.getProperty("company_name") + "." , test, driver, sAssert);
		s_validate_transaction_history_offer_price("$" + decimalFormat.format(Double.parseDouble(paramDict.get(testId+".sellercounteramount"))) + ".00", test, driver, sAssert);
		s_validate_item_in_filter_option(driver, paramDict, test, "Active Offers", itemIdToBeSearch, sAssert);
		
		try {
			sAssert.assertAll();
		}catch( Exception e) {
			test.log(LogStatus.FAIL, e.getMessage());
		}
	} catch(Exception|AssertionError e){
				// logout of NBE in case of exception for next test to continue.
			LoginToNBE.s_logout_incase_of_failure_nbe(driver, test);
			Assert.fail("Failed in 'a_accept_offer_buyer_nbe' action. " + e.getMessage());
	}	
}
	
	/**
	 * Method to counter the offer by buyer
	 * @param driver RemoteWebDriver instance
	 * @param paramDict parameter dictionary
	 * @param testId  Current test case id
	 * @param test ExtentTest report instance
	 * @throws IOException exception
	 * @throws InterruptedException exception
	 */
	public static void a_counter_offer_buyer_nbe(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test)
			throws IOException, InterruptedException 
	{	
		try {
			// fetch company item id
			String itemIdToBeSearch = paramDict.get(testId+".companyitemid");
			String counterOfferAmount = paramDict.get(testId+".buyercounteramount");
			
			// search and select the item
			NBESearchItem.s_search_select_item_nbe(driver, paramDict, test, "Active Offers", itemIdToBeSearch);
			// click on counter offer button
			Util.click(LoadProp.nbeproxibidprop, "counter_offer_btn_xpath",test, driver);
			// enter counter offer amount
			Util.sendKeys(LoadProp.nbeproxibidprop,"enter_amount_field_xpath", counterOfferAmount, test, driver);
			// review counter offer
			Util.click(LoadProp.nbeproxibidprop, "review_offer_btn_xpath",test, driver);
			// confirm counter offer
			Util.click(LoadProp.nbeproxibidprop, "confirm_counter_xpath",test, driver);
			Thread.sleep(10000);

			// Soft assert the Status, Status message, Original Asking Price, Current Offer Price, Transaction History Message and Amount
			SoftAssert sAssert= new SoftAssert();
			s_validate_status(LoadProp.nbeproxibidprop.get("counter_status"), test, driver, sAssert);
			s_validate_status_message(LoadProp.nbeproxibidprop.get("counter_message"), test, driver, sAssert);
			s_validate_original_asking_price("$" + decimalFormat.format(Double.parseDouble(paramDict.get(testId+".listprice"))) + ".00", test, driver, sAssert);
			s_validate_current_offer_price("$" + decimalFormat.format(Double.parseDouble(paramDict.get(testId+".buyercounteramount"))) + ".00", test, driver, sAssert);
			s_validate_transaction_history_message(LoadProp.nbeproxibidprop.get("counter_history_message") + " " + LoadProp.getProperty("company_name") + "." , test, driver, sAssert);
			s_validate_transaction_history_offer_price("$" + decimalFormat.format(Double.parseDouble(paramDict.get(testId+".buyercounteramount"))) + ".00", test, driver, sAssert);
			s_validate_item_in_filter_option(driver, paramDict, test, "Active Offers", itemIdToBeSearch, sAssert);

			try {
				sAssert.assertAll();
			}catch( Exception e) {
				test.log(LogStatus.FAIL, e.getMessage());
			}
		} catch(Exception|AssertionError e){
					// logout of NBE in case of exception for next test to continue.
				LoginToNBE.s_logout_incase_of_failure_nbe(driver, test);
				Assert.fail("Failed in 'a_counter_offer_buyer_nbe' action. " + e.getMessage());
		}	
	}
	
	/**
	 * Method to withdraw the offer.
	 * @param driver Driver instance
	 * @param paramDict parameter dictionary
	 * @param test Extent report instance
	 * @throws IOException exception
	 * @throws InterruptedException exception
	 */
	public static void a_decline_offer_buyer_nbe (RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test)
	throws IOException, InterruptedException {
		try{
			// get item id to search
			String itemIdToBeSearch = paramDict.get(testId+".companyitemid");
			String declineComment = paramDict.get(testId+".declinemessage");
			// search and select the item
			NBESearchItem.s_search_select_item_nbe(driver, paramDict, test, "Active Offers", itemIdToBeSearch);
			// click on decline offer button
			Util.click(LoadProp.nbeproxibidprop, "decline_offer_btn_xpath",test, driver);
			// enter decline comment
			Util.sendKeys(LoadProp.nbeproxibidprop,"decline_offer_msg_xpath", declineComment, test, driver);
			// confirm decline offer
			Util.click(LoadProp.nbeproxibidprop, "decline_offer_confirm_btn_xpath",test, driver);
			Thread.sleep(10000);

			// Soft assert the Status, Status message, Original Asking Price, Current Offer Price, Transaction History Message and Amount
			SoftAssert sAssert= new SoftAssert();
			s_validate_status(LoadProp.nbeproxibidprop.get("decline_status"), test, driver, sAssert);
			s_validate_status_message(LoadProp.nbeproxibidprop.get("decline_message"), test, driver, sAssert);
			s_validate_original_asking_price("$" + decimalFormat.format(Double.parseDouble(paramDict.get(testId+".listprice"))) + ".00", test, driver, sAssert);
			s_validate_current_offer_price("$" + decimalFormat.format(Double.parseDouble(paramDict.get(testId+".sellercounteramount"))) + ".00", test, driver, sAssert);
			s_validate_transaction_history_message(LoadProp.nbeproxibidprop.get("decline_history_message") + " " + LoadProp.getProperty("company_name") + "." , test, driver, sAssert);
			s_validate_transaction_history_offer_price("$" + decimalFormat.format(Double.parseDouble(paramDict.get(testId+".sellercounteramount"))) + ".00", test, driver, sAssert);
			s_validate_options_message(LoadProp.nbeproxibidprop.get("decline_options_message"), test, driver, sAssert);
			s_validate_item_in_filter_option(driver, paramDict, test, "Declined Offers", itemIdToBeSearch, sAssert);

			try {
				sAssert.assertAll();
			}catch( Exception e) {
				test.log(LogStatus.FAIL, e.getMessage());
			}
		} catch(Exception|AssertionError e){
			// logout of NBE in case of exception for next test to continue.
			LoginToNBE.s_logout_incase_of_failure_nbe(driver, test);
			Assert.fail("Failed in 'a_decline_offer_buyer_nbe' action. " + e.getMessage());
		}	
	}
	
	/**
	 * Method to withdraw the offer by buyer.
	 * @param driver Driver instance
	 * @param paramDict parameter dictionary
	 * @param test Extent report instance
	 * @throws IOException exception
	 * @throws InterruptedException exception
	 */
	public static void a_withdraw_offer_buyer_nbe (RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test)
	throws IOException, InterruptedException {
		try{
			// get item id to search
			String itemIdToBeSearch = paramDict.get(testId+".companyitemid");
			String transactionTypeCode = paramDict.get(testId+".transactiontype");
			// search and select the item
			NBESearchItem.s_search_select_item_nbe(driver, paramDict, test, "Active Offers", itemIdToBeSearch);
			// click on withdraw offer button
			Util.click(LoadProp.nbeproxibidprop, "withdraw_offer_btn_xpath",test, driver);
			// confirm withdraw offer
			Util.click(LoadProp.nbeproxibidprop, "withdraw_offer_confirm_btn_xpath",test, driver);
			Thread.sleep(10000);

			// Soft assert the Status, Status message, Original Asking Price, Current Offer Price, Transaction History Message and Amount
			SoftAssert sAssert= new SoftAssert();
			
			if(transactionTypeCode.equalsIgnoreCase("BN")){
				s_validate_status(LoadProp.nbeproxibidprop.get("bn_withdraw_offer_status"), test, driver, sAssert);
				s_validate_status_message(LoadProp.nbeproxibidprop.get("bn_withdraw_offer_message"), test, driver, sAssert);
				s_validate_buy_now_offer_price("$" + decimalFormat.format(Double.parseDouble(paramDict.get(testId+".listprice"))) + ".00", test, driver, sAssert);
				s_validate_transaction_history_message(LoadProp.nbeproxibidprop.get("withdraw_offer_history_message") , test, driver, sAssert);
				s_validate_transaction_history_offer_price("$" + decimalFormat.format(Double.parseDouble(paramDict.get(testId+".listprice"))) + ".00", test, driver, sAssert);
				s_validate_options_message(LoadProp.nbeproxibidprop.get("bn_withdraw_offer_options_message"), test, driver, sAssert);
				s_validate_item_in_filter_option(driver, paramDict, test, "Canceled Offers", itemIdToBeSearch, sAssert);
				
			} else if(transactionTypeCode.equalsIgnoreCase("MO")) {
				s_validate_status(LoadProp.nbeproxibidprop.get("withdraw_offer_status"), test, driver, sAssert);
				s_validate_status_message(LoadProp.nbeproxibidprop.get("withdraw_offer_message"), test, driver, sAssert);
				s_validate_original_asking_price("$" + decimalFormat.format(Double.parseDouble(paramDict.get(testId+".listprice"))) + ".00", test, driver, sAssert);
				s_validate_current_offer_price("$" + decimalFormat.format(Double.parseDouble(paramDict.get(testId+".buyerofferamount"))) + ".00", test, driver, sAssert);
				s_validate_transaction_history_message(LoadProp.nbeproxibidprop.get("withdraw_offer_history_message") + " " + LoadProp.getProperty("company_name") + "." , test, driver, sAssert);
				s_validate_transaction_history_offer_price("$" + decimalFormat.format(Double.parseDouble(paramDict.get(testId+".buyerofferamount"))) + ".00", test, driver, sAssert);
				s_validate_options_message(LoadProp.nbeproxibidprop.get("withdraw_offer_options_message"), test, driver, sAssert);
				s_validate_item_in_filter_option(driver, paramDict, test, "Canceled Offers", itemIdToBeSearch, sAssert);
			} 			
			try {
				sAssert.assertAll();
			}catch( Exception|AssertionError e) {
				test.log(LogStatus.FAIL, e.getMessage());
			}
		} catch(Exception|AssertionError e){
			// logout of NBE in case of exception for next test to continue.
			LoginToNBE.s_logout_incase_of_failure_nbe(driver, test);
			Assert.fail("Failed in 'a_withdraw_offer_buyer_nbe' action. : " + e.getMessage());
		}	
	}

	
	/**
	 * Method to approve item by buyer.
	 * @param driver Driver instance
	 * @param paramDict parameter dictionary
	 * @param test Extent report instance
	 * @throws IOException exception
	 * @throws InterruptedException exception
	 */
	public static void a_approve_item_buyer_nbe (RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test)
	throws IOException, InterruptedException {
		try{
			// get item id to search
			String itemIdToBeSearch = paramDict.get(testId+".companyitemid");
			String transactionTypeCode = paramDict.get(testId+".transactiontype");
			// search and select the item			
			NBESearchItem.s_search_select_item_nbe(driver, paramDict, test, "Active Offers", itemIdToBeSearch);
			// click on withdraw offer button
			Util.click(LoadProp.nbeproxibidprop, "approve_item_btn_xpath",test, driver);
			// confirm withdraw offer
			Util.click(LoadProp.nbeproxibidprop, "approve_item_confirm_btn_xpath",test, driver);
			Thread.sleep(10000);

			// Soft assert the Status, Status message, Original Asking Price, Current Offer Price, Transaction History Message and Amount
			SoftAssert sAssert= new SoftAssert();
			
			if(transactionTypeCode.equalsIgnoreCase("BN")){
				s_validate_status(LoadProp.nbeproxibidprop.get("approve_item_status"), test, driver, sAssert);
				s_validate_status_message(LoadProp.nbeproxibidprop.get("approve_item_message"), test, driver, sAssert);
				s_validate_buy_now_offer_price("$" + decimalFormat.format(Double.parseDouble(paramDict.get(testId+".listprice"))) + ".00", test, driver, sAssert);
				s_validate_transaction_history_message(LoadProp.nbeproxibidprop.get("approve_item_history_message") , test, driver, sAssert);
				s_validate_transaction_history_offer_price("$" + decimalFormat.format(Double.parseDouble(paramDict.get(testId+".listprice"))) + ".00", test, driver, sAssert);
				s_validate_item_in_filter_option(driver, paramDict, test, "Active Offers", itemIdToBeSearch, sAssert);
				
			} else if(transactionTypeCode.equalsIgnoreCase("MO")) {
				s_validate_status(LoadProp.nbeproxibidprop.get("approve_item_status"), test, driver, sAssert);
				s_validate_status_message(LoadProp.nbeproxibidprop.get("approve_item_message"), test, driver, sAssert);
				s_validate_current_offer_price("$" + decimalFormat.format(Double.parseDouble(paramDict.get(testId+".buyerofferamount"))) + ".00", test, driver, sAssert);
				s_validate_transaction_history_message(LoadProp.nbeproxibidprop.get("approve_item_history_message") , test, driver, sAssert);
				s_validate_transaction_history_offer_price("$" + decimalFormat.format(Double.parseDouble(paramDict.get(testId+".buyerofferamount"))) + ".00", test, driver, sAssert);
				s_validate_item_in_filter_option(driver, paramDict, test, "Active Offers", itemIdToBeSearch, sAssert);
			} 			
			try {
				sAssert.assertAll();
			}catch( Exception|AssertionError e) {
				test.log(LogStatus.FAIL, e.getMessage());
			}
		} catch(Exception|AssertionError e){
			// logout of NBE in case of exception for next test to continue.
			LoginToNBE.s_logout_incase_of_failure_nbe(driver, test);
			Assert.fail("Failed in 'a_approve_item_buyer_nbe' action. : " + e.getMessage());
		}	
	}

	/**
	 * Method to approve item by buyer.
	 * @param driver Driver instance
	 * @param paramDict parameter dictionary
	 * @param test Extent report instance
	 * @throws IOException exception
	 * @throws InterruptedException exception
	 */
	public static void a_file_dispute_buyer_nbe (RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test)
	throws IOException, InterruptedException {
		try{
			// get item id to search
			String itemIdToBeSearch = paramDict.get(testId+".companyitemid");
			String transactionTypeCode = paramDict.get(testId+".transactiontype");
			String disputeComment = paramDict.get(testId+".disputecomment");
			
			// search and select the item			
			NBESearchItem.s_search_select_item_nbe(driver, paramDict, test, "All Offers", itemIdToBeSearch);
			// click on withdraw offer button
    		Util.click(LoadProp.nbeproxibidprop, "file_dispute_item_btn_xpath",test, driver);
			// enter decline comment
			Util.sendKeys(LoadProp.nbeproxibidprop,"file_dispute_item_message_xpath", disputeComment, test, driver);
			// confirm withdraw offer
			Util.click(LoadProp.nbeproxibidprop, "file_dispute_confirm_btn_xpath",test, driver);
			Thread.sleep(10000);

			// Soft assert the Status, Status message, Original Asking Price, Current Offer Price, Transaction History Message and Amount
			SoftAssert sAssert= new SoftAssert();
			
			if(transactionTypeCode.equalsIgnoreCase("BN")){
				s_validate_status(LoadProp.nbeproxibidprop.get("file_dispute_item_status"), test, driver, sAssert);
				s_validate_status_message(LoadProp.nbeproxibidprop.get("file_dispute_item_message"), test, driver, sAssert);
				s_validate_decline_offer_price("$" + decimalFormat.format(Double.parseDouble(paramDict.get(testId+".listprice"))) + ".00", test, driver, sAssert);
				s_validate_transaction_history_message(LoadProp.nbeproxibidprop.get("file_dispute_item_history_message")  + " " + LoadProp.getProperty("company_name") + "." , test, driver, sAssert);
				s_validate_transaction_history_offer_price("$" + decimalFormat.format(Double.parseDouble(paramDict.get(testId+".listprice"))) + ".00", test, driver, sAssert);
				s_validate_options_message(LoadProp.nbeproxibidprop.get("bn_file_dispute_item_options_message"), test, driver, sAssert);
				s_validate_item_in_filter_option(driver, paramDict, test, "All Offers", itemIdToBeSearch, sAssert);
				
			} else if(transactionTypeCode.equalsIgnoreCase("MO")) {
				s_validate_status(LoadProp.nbeproxibidprop.get("file_dispute_item_status"), test, driver, sAssert);
				s_validate_status_message(LoadProp.nbeproxibidprop.get("file_dispute_item_message"), test, driver, sAssert);
		//		s_validate_original_asking_price("$" + decimalFormat.format(Double.parseDouble(paramDict.get(testId+".listprice"))) + ".00", test, driver, sAssert);
		//		s_validate_current_offer_price("$" + decimalFormat.format(Double.parseDouble(paramDict.get(testId+".buyerofferamount"))) + ".00", test, driver, sAssert);
				s_validate_decline_offer_price("$" + decimalFormat.format(Double.parseDouble(paramDict.get(testId+".buyerofferamount"))) + ".00", test, driver, sAssert);
				s_validate_transaction_history_message(LoadProp.nbeproxibidprop.get("file_dispute_item_history_message") , test, driver, sAssert);
				s_validate_transaction_history_offer_price("$" + decimalFormat.format(Double.parseDouble(paramDict.get(testId+".buyerofferamount"))) + ".00", test, driver, sAssert);
				s_validate_options_message(LoadProp.nbeproxibidprop.get("file_dispute_item_options_message"), test, driver, sAssert);
				s_validate_item_in_filter_option(driver, paramDict, test, "All Offers", itemIdToBeSearch, sAssert);
			} 			
			try {
				sAssert.assertAll();
			}catch( Exception|AssertionError e) {
				test.log(LogStatus.FAIL, e.getMessage());
			}
		} catch(Exception|AssertionError e){
			// logout of NBE in case of exception for next test to continue.
			LoginToNBE.s_logout_incase_of_failure_nbe(driver, test);
			Assert.fail("Failed in 'a_file_dispute_buyer_nbe' action. : " + e.getMessage());
		}	
	}
	



	/**
	 * Method to validate the action performed on item from filter drop-down.
	 * @param driver Driver instance
	 * @param paramDict parameter dictionary
	 * @param test Extent report instance
	 * @param filter filter to pass xpath of element
	 * @param itemToBeSearch item name
	 * @throws IOException exception
	 * @throws InterruptedException exception
	 */
	public static void s_validate_item_in_filter_option (RemoteWebDriver driver, Dictionary<String, String> paramDict, ExtentTest test, String filter, String itemToBeSearch, SoftAssert  sAssert )
			throws IOException, InterruptedException {

			test.log(LogStatus.INFO, "Asserting if the item is displayed under currect filter");
				Util.click(LoadProp.nbeproxibidprop, "back_to_activity_xpath", test, driver);
			Thread.sleep(1000);
			NBESearchItem.s_search_item_nbe(driver, paramDict, test, filter, itemToBeSearch);
			String actualItemId = Util.getText(LoadProp.nbeproxibidprop , "item_id_on_search_xpath", test, driver);
			AssertUtil.performSoftAssertEquals("Item ID: "+ itemToBeSearch, actualItemId, "Item Id : " + itemToBeSearch + " is not found under filter : " + filter, driver, test, sAssert);
	}
	
	/**
	 * Method to soft assert the Offer price in Transaction history
	 * @param expectedTransactionHistoryOfferPrice transaction history price
	 * @param test ExtentTest instance
	 * @param driver RemoteWebDriver instance
	 * @param sAssert SoftAssert instance
	 * @throws IOException exception
	 */
	private static void s_validate_transaction_history_offer_price( String expectedTransactionHistoryOfferPrice, ExtentTest test, RemoteWebDriver driver, SoftAssert sAssert)  throws IOException{
		test.log(LogStatus.INFO, "Asserting Transaction history offer price");
		String actualTransactionHistoryOfferPrice =  Util.getText(LoadProp.nbeproxibidprop , "history_offer_price_xpath", test, driver);
		AssertUtil.performSoftAssertEquals(expectedTransactionHistoryOfferPrice, actualTransactionHistoryOfferPrice, "Offer Price in Transaction History is incorrect.", driver, test, sAssert);
	}
	
	/**
	 * Method to soft assert the Transaction history message
	 * @param expectedTransactionHistoryMessage transaction history message
	 * @param test ExtentTest instance
	 * @param driver RemoteWebDriver instance
	 * @param sAssert SoftAssert instance
	 * @throws IOException exception
	 */
	private static void s_validate_transaction_history_message(String expectedTransactionHistoryMessage, ExtentTest test, RemoteWebDriver driver, SoftAssert sAssert)  throws IOException{
		test.log(LogStatus.INFO, "Asserting Transaction history message");
		String actualTransactionHistoryMessage =  Util.getText(LoadProp.nbeproxibidprop , "history_message_xpath", test, driver);
		AssertUtil.performSoftAssertEquals(expectedTransactionHistoryMessage, actualTransactionHistoryMessage, "Transaction History Message is incorrect.", driver, test, sAssert);
	}

	/**
	 * Method to soft assert the current offer price
	 * @param expectedCurrentOfferPrice current offer price
	 * @param test ExtentTest instance
	 * @param driver RemoteWebDriver instance
	 * @param sAssert SoftAssert instance
	 * @throws IOException exception
	 */
	private static void s_validate_current_offer_price(String expectedCurrentOfferPrice, ExtentTest test, RemoteWebDriver driver, SoftAssert sAssert)  throws IOException{
		test.log(LogStatus.INFO, "Asserting Current Offer price");
		String actualCurrentOfferPrice = Util.getText(LoadProp.nbeproxibidprop , "current_offer_price_xpath", test, driver);
		AssertUtil.performSoftAssertEquals(expectedCurrentOfferPrice, actualCurrentOfferPrice, "Currnet Offer Price is incorrect.", driver, test, sAssert);
	}

	/**
	 * Method to soft assert the original asking price
	 * @param expectedOriginalAskingPrice original asking price
	 * @param test ExtentTest instance
	 * @param driver RemoteWebDriver instance
	 * @param sAssert SoftAssert instance
	 * @throws IOException exception
	 */
	private static void s_validate_original_asking_price(String expectedOriginalAskingPrice, ExtentTest test, RemoteWebDriver driver, SoftAssert sAssert)  throws IOException{
		test.log(LogStatus.INFO, "Asserting Original Asking price");
		String actualOriginalAskingPrice = Util.getText(LoadProp.nbeproxibidprop , "original_asking_price_xpath", test, driver);
		AssertUtil.performSoftAssertEquals(expectedOriginalAskingPrice, actualOriginalAskingPrice, "Original Asking Price is incorrect.", driver, test, sAssert);
	}

	/**
	 * Method to soft assert the buy now offer price
	 * @param expectedOriginalAskingPrice original asking price
	 * @param test ExtentTest instance
	 * @param driver RemoteWebDriver instance
	 * @param sAssert SoftAssert instance
	 * @throws IOException exception
	 */
	private static void s_validate_buy_now_offer_price(String expectedBuyNowOfferPrice, ExtentTest test, RemoteWebDriver driver, SoftAssert sAssert) throws IOException {
		test.log(LogStatus.INFO, "Asserting Offer Price");
		String actualBuyNowOfferPrice = Util.getText(LoadProp.nbeproxibidprop , "buynow_offer_price_xpath", test, driver);
		AssertUtil.performSoftAssertEquals(expectedBuyNowOfferPrice, actualBuyNowOfferPrice, "Buy Now Offer Price is incorrect.", driver, test, sAssert);
	}
	
	/**
	 * Method to soft assert the buy now offer price
	 * @param expectedOriginalAskingPrice original asking price
	 * @param test ExtentTest instance
	 * @param driver RemoteWebDriver instance
	 * @param sAssert SoftAssert instance
	 * @throws IOException exception
	 */
	private static void s_validate_decline_offer_price(String expectedBuyNowOfferPrice, ExtentTest test, RemoteWebDriver driver, SoftAssert sAssert) throws IOException {
		test.log(LogStatus.INFO, "Asserting Offer Price");
		String actualBuyNowOfferPrice = Util.getText(LoadProp.nbeproxibidprop , "file_dispute_price_xpath", test, driver);
		AssertUtil.performSoftAssertEquals(expectedBuyNowOfferPrice, actualBuyNowOfferPrice, "Offer Price is incorrect.", driver, test, sAssert);
	}


	/**
	 * Method to soft assert the Action status message
	 * @param expectedStatusMessage status message
	 * @param test ExtentTest instance
	 * @param driver RemoteWebDriver instance
	 * @param sAssert SoftAssert instance
	 * @throws IOException exception
	 */
	private static void s_validate_status_message(String expectedStatusMessage, ExtentTest test, RemoteWebDriver driver, SoftAssert sAssert)  throws IOException{
		test.log(LogStatus.INFO, "Asserting Status Message");
		String actualStatusMessage = Util.getText(LoadProp.nbeproxibidprop, "action_status_message_xpath", test, driver);
		AssertUtil.performSoftAssertEquals(expectedStatusMessage, actualStatusMessage, "Status message is incorrect.", driver, test, sAssert);
	}

	/**
	 * Method to soft assert the Action status
	 * @param expectedStatus status 
	 * @param test ExtentTest instance
	 * @param driver RemoteWebDriver instance
	 * @param sAssert SoftAssert instance
	 * @throws IOException exception
	 */
	public static void s_validate_status(String expectedStatus, ExtentTest test, RemoteWebDriver driver, SoftAssert sAssert ) throws IOException{
		test.log(LogStatus.INFO, "Asserting Status");
		String actualStatus = Util.getText(LoadProp.nbeproxibidprop , "action_status_xpath", test, driver);
		AssertUtil.performSoftAssertEquals(expectedStatus, actualStatus, "Status is incorrect.", driver, test, sAssert);
	}

	/**
	 * Method to soft assert the message displayed in the options section when no action buttons are available
	 * @param expectedStatus status 
	 * @param test ExtentTest instance
	 * @param driver RemoteWebDriver instance
	 * @param sAssert SoftAssert instance
	 * @throws IOException exception
	 */
	private static void s_validate_options_message(String expectedOptionsMessage, ExtentTest test, RemoteWebDriver driver, SoftAssert sAssert) throws IOException {
		test.log(LogStatus.INFO, "Asserting text in the Options section");
		String actualOptionsMessage = Util.getText(LoadProp.nbeproxibidprop , "transaction_options_message_xpath", test, driver);
		AssertUtil.performSoftAssertEquals(expectedOptionsMessage, actualOptionsMessage, "Message displayed in Options section is incorrect.", driver, test, sAssert);
	}
	
	
}
