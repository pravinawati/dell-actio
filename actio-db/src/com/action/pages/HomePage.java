package com.action.pages;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Dictionary;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;

import com.properties.mapping.LoadProp;
import com.relevantcodes.extentreports.ExtentTest;
import com.utility.library.Util;

public class HomePage {

	public static void create_test_suite(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		// TODO Auto-generated method stub page
		Util.reportPassWithoutScreenshot(test,"Started Create Test Suite In QM .......");
		Runtime runtime = Runtime.getRuntime();
		try {
		    Process p1 = runtime.exec("cmd /c start createqmsuite.bat");
		} catch(IOException ioException) {
			Util.reportPassWithoutScreenshot(test,ioException.getMessage() );
		}
		Util.reportPassWithoutScreenshot(test,"End Test Suite Creation .......");
		   
	}
	
	public static void update_test_suite(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		// TODO Auto-generated method stub
		
		Util.reportPassWithoutScreenshot(test,"Started Test Suite Update .......");
		
		Runtime runtime = Runtime.getRuntime();
		try {
		    Process p1 = runtime.exec("cmd /c start bulkupdate.bat");
		    
		} catch(IOException ioException) {
			Util.reportPassWithoutScreenshot(test,ioException.getMessage() );
		}
		Util.reportPassWithoutScreenshot(test,"End of Test Suite Updation.......");
		   
	}


	
	public static void access_ent_page(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			Util.access_link(LoadProp.getProperty("web_weburl")+"getEnterprise", test, driver);
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.homepageprop.get("enterprise_page_title_xpath")));
			Util.reportPass(driver,test,"==> Navigated to Enterprise Page successfully");
			Thread.sleep(3000);
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
	public static void access_com_page(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			Util.access_link(LoadProp.getProperty("web_weburl")+"manageCommit", test, driver);
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.homepageprop.get("commit_page_title_xpath")));
			Util.reportPass(driver,test,"==> Navigated to Commit Page successfully");
			Thread.sleep(3000);
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
	public static void access_dash_page(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			Util.access_link(LoadProp.getProperty("web_weburl")+"getDashboard", test, driver);
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.homepageprop.get("dashboard_page_title_xpath")));
			Util.reportPass(driver,test,"==> Navigated to Dashboard Page successfully");
			Thread.sleep(3000);
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
	public static void access_domain_page(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			Util.access_link(LoadProp.getProperty("web_weburl")+"manageDomain", test, driver);
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.homepageprop.get("domains_page_title_xpath")));
			Util.reportPass(driver,test,"==> Navigated to Domains Page successfully");
			Thread.sleep(3000);
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
	public static void access_usergroups_page(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			Util.access_link(LoadProp.getProperty("web_weburl")+"manageUserGroup", test, driver);
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.homepageprop.get("usergroups_page_title_xpath")));
			Util.reportPass(driver,test,"==> Navigated to User Groups Page successfully");
			Thread.sleep(3000);
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
	public static void access_users_page(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			Util.access_link(LoadProp.getProperty("web_weburl")+"manageUsers", test, driver);
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.homepageprop.get("users_page_title_xpath")));
			Util.reportPass(driver,test,"==> Navigated to Users Page successfully");
			Thread.sleep(3000);
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
	public static void access_endpointgroups_page(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			Util.access_link(LoadProp.getProperty("web_weburl")+"manageEndpointGroup", test, driver);
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.homepageprop.get("endpointgroups_page_title_xpath")));
			Util.reportPass(driver,test,"==> Navigated to Endpoint Groups Page successfully");
			Thread.sleep(3000);
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
	public static void access_endpoints_page(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			Util.access_link(LoadProp.getProperty("web_weburl")+"manageEndpoints", test, driver);
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.homepageprop.get("endpoints_page_title_xpath")));
			Util.reportPass(driver,test,"==> Navigated to Endpoints Page successfully");
			Thread.sleep(3000);
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
	public static void access_administrators_page(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			Util.access_link(LoadProp.getProperty("web_weburl")+"manageAdmins", test, driver);
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.homepageprop.get("administrators_page_title_xpath")));
			Util.reportPass(driver,test,"==> Navigated to Administrators Page successfully");
			Thread.sleep(3000);
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
	public static void access_managereports_page(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			Util.access_link(LoadProp.getProperty("web_weburl")+"manageReports", test, driver);
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.homepageprop.get("managereports_page_title_xpath")));
			Util.reportPass(driver,test,"==> Navigated to Manage Reports Page successfully");
			Thread.sleep(3000);
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
	public static void access_auditevents_page(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			Util.access_link(LoadProp.getProperty("web_weburl")+"report?name=AUDIT_EVENT&page=auditEvent/auditEvent", test, driver);
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.homepageprop.get("auditevents_page_title_xpath")));
			Util.reportPass(driver,test,"==> Navigated to Audit Events Page successfully");
			Thread.sleep(3000);
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
	public static void access_loganalyzer_page(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			Util.access_link(LoadProp.getProperty("web_weburl")+"manageLogs", test, driver);
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.homepageprop.get("loganalyzer_page_title_xpath")));
			Util.reportPass(driver,test,"==> Navigated to Log Analyzer Page successfully");
			Thread.sleep(3000);
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
	public static void access_recoverdata_page(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			Util.access_link(LoadProp.getProperty("web_weburl")+"recoverData", test, driver);
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.homepageprop.get("recoverdata_page_title_xpath")));
			Util.reportPass(driver,test,"==> Navigated to Recover Data Page successfully");
			Thread.sleep(3000);
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
	public static void access_recoverendpoint_page(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			Util.access_link(LoadProp.getProperty("web_weburl")+"recoverEndpoint", test, driver);
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.homepageprop.get("recoverendpoint_page_title_xpath")));
			Util.reportPass(driver,test,"==> Navigated to Recover Endpoint Page successfully");
			Thread.sleep(3000);
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
	public static void access_licensemanagement_page(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			Util.access_link(LoadProp.getProperty("web_weburl")+"manageLicenses", test, driver);
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.homepageprop.get("licensemanagement_page_title_xpath")));
			Util.reportPass(driver,test,"==> Navigated to License Management Page successfully");
			Thread.sleep(3000);
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
	public static void access_servicesmanagement_page(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			Util.access_link(LoadProp.getProperty("web_weburl")+"servicesManagement", test, driver);
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.homepageprop.get("servicesmanagement_page_title_xpath")));
			Util.reportPass(driver,test,"==> Navigated to Services Management Page successfully");
			Thread.sleep(3000);
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
	public static void access_notificationsmanagement_page(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			Util.access_link(LoadProp.getProperty("web_weburl")+"manageAlerts", test, driver);
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.homepageprop.get("notificationsmanagement_page_title_xpath")));
			Util.reportPass(driver,test,"==> Navigated to Notifications Management Page successfully");
			Thread.sleep(3000);
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
	public static void access_externalusermanagement_page(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			Util.access_link(LoadProp.getProperty("web_weburl")+"manageExternalUsers", test, driver);
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.homepageprop.get("externalusermanagement_page_title_xpath")));
			Util.reportPass(driver,test,"==> Navigated to External User Management Page successfully");
			Thread.sleep(3000);
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
	public void access_compliancereporter(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			Util.access_link(LoadProp.getProperty("crurl"), test, driver);
			Util.reportPass(driver,test,"==> Compliance Reporter url accessed.");
			Thread.sleep(3000);
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}		
	}
	
	public static void access_crlink(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			Util.clickReporting(driver, paramDict, test);
			Util.click(LoadProp.homepageprop, "Compliance_Reporter_Link_xpath", test, driver);
			Thread.sleep(2000);
			ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
			Thread.sleep(2000);
			//System.out.println(tabs);
		    driver.switchTo().window(tabs.get(1));
		    Util.reportPass(driver,test, "Compliance Reporter link clicked!!");
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
	public static void search_and_click_entity(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			Util.clear(LoadProp.homepageprop, "searchbar_xpath", test, driver);
			//serve.clear(driver, By.xpath(homePageProp.get("searchbar")), paramDict.get("text.search"));
			Util.sendKeys(LoadProp.homepageprop, "searchbar_xpath", paramDict.get(testId+".search_text"), test, driver);
			WebElement elem = driver.findElementByXPath(LoadProp.homepageprop.get("searchbar_xpath"));
			elem.sendKeys(Keys.ENTER);
			//driver.switchTo().defaultContent();
			Util.waitUntilElementVisibility(driver, By.xpath("//*[@class='grid-link ng-binding' and contains(text(), '" + paramDict.get(testId+".search_text") + "')]"));
			Util.reportPass(driver,test, "Entity Searched!!");
		  
			Thread.sleep(10000);
			

			driver.findElement(By.xpath("//a[@class='grid-link ng-binding' and contains(text(), '" + paramDict.get(testId+".search_text") + "')]")).click();
			Util.reportPass(driver,test, "Entity Clicked!!");
		  
			Util.waitUntilElementVisibility(driver, By.xpath("//div[@class='page-title' and contains(text(), '" + paramDict.get(testId+".search_text") + "')]"));
			Util.reportPass(driver,test, "Page landed to Endpoint Details for: " +paramDict.get(testId+".search_text"));
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}

}
