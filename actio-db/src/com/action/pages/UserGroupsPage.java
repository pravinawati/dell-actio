package com.action.pages;

import java.util.Dictionary;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;

import com.properties.mapping.LoadProp;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.utility.library.Util;

public class UserGroupsPage {
	
	//Add Active Directory User Group
	/* prop xls file
	 * 
	 * addusergroup | domain | DDPS.LOCAL
	 * addusergroup | group | aa
	 * 
	 */
	public static void add_active_directory_usergroup(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			test.log(LogStatus.INFO, "Add Active Directory User Group");
			Util.click(LoadProp.usergroupsprop, "initiate_add_user_group_button_xpath", test, driver);
			test.log(LogStatus.INFO, "Add User Group button clicked!!");
			
			Util.waitUntilElementVisibility(driver,By.xpath(LoadProp.usergroupsprop.get("addUserGroupPage_xpath")));
			test.log(LogStatus.INFO, "Search and Select a user group");
			String domain_name = paramDict.get(testId+".domain");
			String usergroup_name = paramDict.get(testId+".group");
			System.out.println(domain_name);
			System.out.println(usergroup_name);
			
			//AD
			test.log(LogStatus.INFO, "SET :: Active Directory User Group");
			Util.click(LoadProp.usergroupsprop, "selectGroupType_xpath", test, driver);
			driver.findElement(By.xpath("//ul[@id='endpoint-class_listbox']/*[starts-with(text(), 'Active Directory User Group')]")).click();
			test.log(LogStatus.INFO, "Selected Active Directory User Group from dropdown");
			
			//select domain from drop down
			Util.waitUntilElementVisibility(driver,By.xpath(LoadProp.usergroupsprop.get("select_domain_drop_down_xpath")));
			Util.click(LoadProp.usergroupsprop, "select_domain_drop_down_xpath", test, driver);
			driver.findElement(By.xpath("//ul[@id='domain_listbox']/*[starts-with(text(), '" + domain_name + "')]")).click();
			
			//search user group name
			Util.clear(LoadProp.usergroupsprop, "search_user_groupname_xpath", test, driver);
			Util.sendKeys(LoadProp.usergroupsprop, "search_user_groupname_xpath", usergroup_name, test, driver);
			Util.click(LoadProp.usergroupsprop, "groupSearchBtn_xpath", test, driver);
			Util.waitUntilElementVisibility(driver,By.xpath("//select[@id='groupList']/option[1]"));
			Util.click(LoadProp.usergroupsprop, "search_user_group_xpath", test, driver);
			test.log(LogStatus.INFO, LoadProp.usergroupsprop.get("search_completion_message_xpath"));
			
			//Select Group Name
			String search_group_name = driver.findElement(By.xpath("//select[@id='groupList']/option[1]")).getText();
			String replace_domain_name = domain_name.replace(".LOCAL", "");
			String acc_usergroup_name = usergroup_name +" (" + replace_domain_name +"\\"+ usergroup_name +")";
			
			if(search_group_name.contentEquals(acc_usergroup_name)){
				//Assert Group name : Click
				test.log(LogStatus.INFO, "Successfully find "+ acc_usergroup_name +" from list..");
				driver.findElement(By.xpath("//select[@id='groupList']/option[1]")).click();
				Util.click(LoadProp.usergroupsprop, "addGroupBtn_xpath", test, driver);
				test.log(LogStatus.INFO, "User Group Added Successfully..");
				
				//Check User Group Page
				String add_usergroup_alert = driver.findElement(By.xpath("//div[@id='notificationPanel']")).getText();
				test.log(LogStatus.INFO, add_usergroup_alert);
				
				//Back To User Group page
				Util.click(LoadProp.usergroupsprop, "cancelGroupBtn_xpath", test, driver);
				Util.waitUntilElementVisibility(driver,By.xpath(LoadProp.usergroupsprop.get("initiate_add_user_group_button_xpath")));
				
				//Assert User Group Name
				List<WebElement> page = driver.findElements(By.xpath("//div[@data-role='pager']/ul/li"));
				String page_size = String.valueOf(page.size());
				int ac_page_size = Integer.parseInt(page_size) - 1;
				
				for(int i=1; i<=ac_page_size; i++){
					int optGroup = 1;
					String find_group = "false";
					test.log(LogStatus.INFO, "Find Group: " + find_group);
					
					for(int j=1; j<=25; j++){
						String row = driver.findElement(By.xpath("//tbody[@role='rowgroup']/tr["+ optGroup +"]/td[1]")).getText();
						test.log(LogStatus.INFO, "Row: " + row);
						//Find User
						if(row.contentEquals(acc_usergroup_name)){
							find_group = "true";
							test.log(LogStatus.INFO, "Find User: " + find_group);
							break;
						}
						optGroup++;
					}
					
					//Find User
					if(find_group == "true"){
						Util.reportPass(driver,test, "Find User: " + find_group);
						break;
					}else{
						Util.reportPass(driver,test, "Find User: " + find_group);
						test.log(LogStatus.INFO, "Pagination Next Page Users"+ i);
						Util.click(LoadProp.usergroupsprop, "paginationNextPageUsers_xpath", test, driver);
					}
				}
			}else{
				Util.reportPass(driver,test, "Failed to find "+ search_group_name +" from list..");
				Assert.assertEquals("", "none");
			}
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
	//Add Admin Defined User Group
	/* prop xls file
	 * 
	 * addusergroup | group | krishna
	 * 
	 * 
	 */
	public static void add_admin_defined_usergroup(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			test.log(LogStatus.INFO, "Add Active Directory User Group");
			Util.click(LoadProp.usergroupsprop, "initiate_add_user_group_button_xpath", test, driver);
			test.log(LogStatus.INFO, "Add User Group button clicked!!");

			Util.waitUntilElementVisibility(driver,By.xpath(LoadProp.usergroupsprop.get("addUserGroupPage_xpath")));
			test.log(LogStatus.INFO, "Search and Select a user group");
			String group_name = paramDict.get(testId+".group");
			String usergroup_des = "You only automate a test not testing...";
			
			//Admin Define Group :: ADMIN-DEFINED User Group
			test.log(LogStatus.INFO, "SET :: ADMIN-DEFINED User Group");
			Util.click(LoadProp.usergroupsprop, "selectGroupType_xpath", test, driver);
			driver.findElement(By.xpath("//ul[@id='endpoint-class_listbox']/*[starts-with(text(), 'ADMIN-DEFINED User Group')]")).click();
			test.log(LogStatus.INFO, "Selected ADMIN-DEFINED User Group from dropdown");
			
			//Group Name
			Util.sendKeys(LoadProp.usergroupsprop, "adminDefineGroupName_xpath", group_name, test, driver);
			Util.sendKeys(LoadProp.usergroupsprop, "adminDefineDes_xpath", usergroup_des, test, driver);
			
			//ADD
			Util.click(LoadProp.usergroupsprop, "adminDefineAddGroupBtn_xpath", test, driver);
			test.log(LogStatus.INFO, "User Group Added Button..");
			Util.waitUntilElementVisibility(driver,By.xpath("//div[@role='dialog']"));
			test.log(LogStatus.INFO, "User Group Add Popup Window is opened..");
			driver.findElement(By.xpath("//button[starts-with(@id,'resolveButton')]")).click();
			
			//Check User Group Page
			String add_usergroup_alert = driver.findElement(By.xpath("//div[@id='notificationPanel']")).getText();
			test.log(LogStatus.INFO, add_usergroup_alert);
			
			//Back to User Group Page
			Util.waitUntilElementVisibility(driver,By.xpath(LoadProp.usergroupsprop.get("cancelAdminDefine_xpath")));
			Util.click(LoadProp.usergroupsprop, "cancelAdminDefine_xpath", test, driver);
			Util.waitUntilElementVisibility(driver,By.xpath(LoadProp.usergroupsprop.get("initiate_add_user_group_button_xpath")));
			
			//Assert User Group Name
			List<WebElement> page = driver.findElements(By.xpath("//div[@data-role='pager']/ul/li"));
			String page_size = String.valueOf(page.size());
			int ac_page_size = Integer.parseInt(page_size) - 1;
			System.out.println(ac_page_size);
			
			for(int i=1; i<=ac_page_size; i++){
				int optGroup = 1;
				String find_group = "false";
				test.log(LogStatus.INFO, "Find Group: " + find_group);
				
				for(int j=1; j<=25; j++){
					String row = driver.findElement(By.xpath("//tbody[@role='rowgroup']/tr["+ optGroup +"]/td[1]")).getText();
					test.log(LogStatus.INFO, "Row: " + row);
					//Find User
					if(row.contentEquals(group_name)){
						find_group = "true";
						test.log(LogStatus.INFO, "Find User: " + find_group);
						break;
					}
					optGroup++;
				}
				
				//Find User
				if(find_group == "true"){
					test.log(LogStatus.INFO, "Find User: " + find_group);
					break;
				}else{
					test.log(LogStatus.INFO, "Find User: " + find_group);
					test.log(LogStatus.INFO, "Pagination Next Page Users"+ i);
					Util.click(LoadProp.usergroupsprop, "paginationNextPageUsers_xpath", test, driver);
				}
			}
			
			//check Commit Count == 1
			Thread.sleep(1000);
			System.out.println("navigating to commit");
			Util.access_link(LoadProp.getProperty("web_weburl")+"manageCommit", test, driver);
			test.log(LogStatus.INFO, "Commit link clicked!!");
			
			//Get Count
			String c_count = driver.findElement(By.xpath("//p[@id='policyCommitRequired']")).getText();
			String acc_c_count = "Pending Policy Changes: 1";
			
			if(c_count.contentEquals(acc_c_count)){
				test.log(LogStatus.INFO, "Pending Policy Changes: 1");
				driver.findElement(By.xpath("//button[@id='commitPolicies']")).click();
				Util.reportPass(driver,test, "Successfully Commit Pending Policy");
			}else{
				test.log(LogStatus.INFO, c_count);
				Util.reportPass(driver,test, "Failed to match Commit count..");
				Assert.assertEquals("", "none");
			}
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}

	public static void delete_usergroup(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			test.log(LogStatus.INFO, "Verify User Group deletion");
			String xpath = "//div[@class='k-grid-content k-auto-scrollable']/table/tbody/tr/td[1]/a[contains(text(), '"+paramDict.get(testId+".usergroupname")+"')]/../../td[4]";
			driver.findElement(By.xpath(xpath)).click();
			Util.click(LoadProp.usergroupsprop, "delete_user_group_xpath", test, driver);
			Util.click(LoadProp.usergroupsprop, "confirm_delete_user_group_xpath", test, driver);
			//Util.waitUntilElementVisibility(driver,By.xpath(LoadProp.usergroupsprop.get("successful_user_group_deletion_message_xpath")));
			Util.waitUntilElementVisibility(driver,By.xpath(LoadProp.usergroupsprop.get("wrap_msg_xpath")));
			WebElement wrap_msg = driver.findElement(By.xpath(LoadProp.usergroupsprop.get("wrap_msg_xpath")));
			String expected = wrap_msg.getText();
			Assert.assertEquals("Successfully removed the User Group.", expected);
			Util.reportPass(driver,test, "Delete User Group successfully!!");
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
}
