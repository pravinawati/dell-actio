package com.action.pages;

import java.io.IOException;
import java.util.Dictionary;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;

import com.properties.mapping.LoadProp;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.utility.library.Util;

public class AndroidTest {
	
	public static void hrms_app_login_invalid(RemoteWebDriver driver,	Dictionary<String, String> paramDict, String testId, ExtentTest test) throws IOException {
		try {
			System.out.println("App Launched");
			test.log(LogStatus.INFO, "App launched");
			
			//Enter Username
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.androidprop.get("hrms_username_xpath")));
			driver.findElement(By.xpath(LoadProp.androidprop.get("hrms_username_xpath"))).sendKeys("xyz");
			System.out.println("Username entered.");
			driver.navigate().back();
			
			//Enter Password
			//Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.androidprop.get("hrms_password_xpath")));
			driver.findElement(By.xpath(LoadProp.androidprop.get("hrms_password_xpath"))).sendKeys("xyz");
			System.out.println("Password entered.");
			driver.navigate().back();
			
			//click login button
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.androidprop.get("hrms_login_button_xpath")));
			Util.click(LoadProp.androidprop, "hrms_login_button_xpath", test, driver);
			System.out.println("Click login button.");
			
			//Verify welcome screen
			//Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.androidprop.get("invalid__cred_login_validation_xpath")));
			//WebElement element = driver.findElement(By.xpath(LoadProp.androidprop.get("hrms_welcome_screen_xpath")));
			//System.out.println(element.getText());
			//Assert.assertTrue(Util.getText(LoadProp.androidprop, "invalid__cred_login_validation_xpath", test, driver).contains("Invalid credentials"));
			//Assert.assertTrue(Util.getText(LoadProp.androidprop, "toast_xpath", test, driver).contains("Invalid credentials"));
			
			test.log(LogStatus.INFO, "Log in validated successfully.");
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void hrms_app_login(RemoteWebDriver driver,	Dictionary<String, String> paramDict, String testId, ExtentTest test) throws IOException {
		try {
			System.out.println("App Launched");
			test.log(LogStatus.INFO, "App launched");
			
			//Enter Username
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.androidprop.get("hrms_username_xpath")));
			driver.findElement(By.xpath(LoadProp.androidprop.get("hrms_username_xpath"))).sendKeys("pranay.khapekar");
			System.out.println("Username entered.");
			driver.navigate().back();
			
			//Enter Password
			//Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.androidprop.get("hrms_password_xpath")));
			driver.findElement(By.xpath(LoadProp.androidprop.get("hrms_password_xpath"))).sendKeys("password");
			System.out.println("Password entered.");
			driver.navigate().back();
			
			//click login button
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.androidprop.get("hrms_login_button_xpath")));
			Util.click(LoadProp.androidprop, "hrms_login_button_xpath", test, driver);
			System.out.println("Click login button.");
			
			//Verify welcome screen
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.androidprop.get("hrms_welcome_screen_xpath")));
			//WebElement element = driver.findElement(By.xpath(LoadProp.androidprop.get("hrms_welcome_screen_xpath")));
			//System.out.println(element.getText());
			Assert.assertTrue(Util.getText(LoadProp.androidprop, "hrms_welcome_screen_xpath", test, driver).contains("Welcome,"));
			
			test.log(LogStatus.INFO, "Logged in successfully.");
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void validate_blank_credentials(RemoteWebDriver driver,	Dictionary<String, String> paramDict, String testId, ExtentTest test) throws IOException {
		try {
			System.out.println("App Launched");
			test.log(LogStatus.INFO, "App launched");
			
			//click login button
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.androidprop.get("hrms_login_button_xpath")));
			Util.click(LoadProp.androidprop, "hrms_login_button_xpath", test, driver);
			System.out.println("Click login button.");
			
			//Verify welcome screen
			//Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.androidprop.get("blank_cred_login_validation_xpath")));
			//WebElement element = driver.findElement(By.xpath(LoadProp.androidprop.get("hrms_welcome_screen_xpath")));
			//System.out.println(element.getText());
			//Assert.assertTrue(Util.getText(LoadProp.androidprop, "toast_xpath", test, driver).contains("Please Fill All The Field"));
			
			test.log(LogStatus.INFO, "Logged in successfully.");
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
