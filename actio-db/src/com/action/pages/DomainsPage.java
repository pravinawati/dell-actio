package com.action.pages;

import java.io.IOException;
import java.util.Dictionary;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;

import com.properties.mapping.LoadProp;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.utility.library.Util;

public class DomainsPage {
	
	//Login to Web Application
	public static void add_domain(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			test.log(LogStatus.INFO, "Inside add domain. Click on Add Domain button");
			//serve.captureImage(driver,imgAppProp.get("superadminHome"));
			Thread.sleep(2000);
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.domainsprop.get("popup_add_domain_xpath")));
			Util.click(LoadProp.domainsprop, "popup_add_domain_xpath", test, driver);
			Util.clear(LoadProp.domainsprop, "domain_hostname_xpath", test, driver);
			Util.sendKeys(LoadProp.domainsprop, "domain_hostname_xpath", paramDict.get(testId+".domain_hostname"), test, driver);
			Util.clear(LoadProp.domainsprop, "domain_port_xpath", test, driver);
			Util.sendKeys(LoadProp.domainsprop, "domain_port_xpath", paramDict.get(testId+".domain_port"), test, driver);
			Util.clear(LoadProp.domainsprop, "domain_username_xpath", test, driver);
			Util.sendKeys(LoadProp.domainsprop, "domain_username_xpath", paramDict.get(testId+".domain_username"), test, driver);
			Util.clear(LoadProp.domainsprop, "domain_password_xpath", test, driver);
			Util.sendKeys(LoadProp.domainsprop, "domain_password_xpath", paramDict.get(testId+".domain_password"), test, driver);
			Util.click(LoadProp.domainsprop, "add_btn_domain_xpath", test, driver);
		    Thread.sleep(1000);
		    Util.click(LoadProp.domainsprop, "click_ok_button_xpath", test, driver);
		    Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.domainsprop.get("ddps_local_link_xpath")));
		    Thread.sleep(1000);
			Util.reportPass(driver,test,"Added Domain Successfully!");
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
	public static void verify_add_domain(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			test.log(LogStatus.INFO, "Verifying newly added Domain");
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.domainsprop.get("ddps_local_link_xpath")));
			Util.click(LoadProp.domainsprop, "ddps_local_link_xpath", test, driver);
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.domainsprop.get("details_tab_xpath")));
			Util.click(LoadProp.domainsprop, "details_tab_xpath", test, driver);
			WebElement element = driver.findElement(By.xpath("//div[@class='col-sm-5']/p"));
			Assert.assertEquals(element.getText(), "DDPS.LOCAL");
			/*Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.domainsprop.get("members_tab_xpath")));
			Util.click(LoadProp.domainsprop, "members_tab_xpath", test, driver);
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.domainsprop.get("settings_tab_xpath")));
			Util.click(LoadProp.domainsprop, "settings_tab_xpath", test, driver);*/
			Util.reportPass(driver,test, "Add Domain verification completed");
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
	public static void update_domain(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			test.log(LogStatus.INFO, "Click On added or listed Domain on the domain page");
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.domainsprop.get("ddps_local_link_xpath")));
			Util.click(LoadProp.domainsprop, "ddps_local_link_xpath", test, driver);
			test.log(LogStatus.INFO, "Domain ddps.local clicked!!");
		    Util.click(LoadProp.domainsprop, "settings_tab_xpath", test, driver);
		    test.log(LogStatus.INFO, "Domain Settings tab clicked!!");
		    Thread.sleep(2000);
		    Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.domainsprop.get("Domain_User_Input_Box_xpath")));
		    //Util.click(LoadProp.domainsprop, "domain_username_xpath", test, driver);
		    Util.clear(LoadProp.domainsprop, "Domain_User_Input_Box_xpath", test, driver);
			test.log(LogStatus.INFO, "Domain update user input box clicked!!");
		    Util.sendKeys(LoadProp.domainsprop, "Domain_User_Input_Box_xpath", paramDict.get(testId+".domain_username"), test, driver);
		    test.log(LogStatus.INFO, "input provided in Domain update user input box !!");
		    Util.clear(LoadProp.domainsprop, "domain_password_xpath", test, driver);
		    Util.sendKeys(LoadProp.domainsprop, "domain_password_xpath", paramDict.get(testId+".domain_password"), test, driver);
		    test.log(LogStatus.INFO, "input provided in Domain update user input box !!");
		    Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.domainsprop.get("Update_Domain_Link_xpath")));
		    Util.click(LoadProp.domainsprop, "Update_Domain_Link_xpath", test, driver);
		    test.log(LogStatus.INFO, "Clicked on Update Domain Button !!");
		    //Thread.sleep(500);
		    //WebElement element = driver.findElement(By.xpath(LoadProp.domainsprop.get("wrap_msg_xpath")));
		    test.log(LogStatus.INFO, "Notification generated !!");
		    //String SuccessfulNotification = driver.findElement(By.xpath(LoadProp.domainsprop.get("wrap_msg_xpath"))).getText();
		    //String SuccessfulNotification = element.getText();
		    //System.out.println(SuccessfulNotification);
		    //Assert.assertEquals(SuccessfulNotification, "Successfully updated domain settings.");
		    Util.reportPass(driver,test, "Domain settings updated successfully");
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
		
	}

}
