package com.action.pages;

import java.util.Dictionary;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;

import com.properties.mapping.LoadProp;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.utility.library.Util;

public class UsersPage {

	//Add User By Domain
	/* prop xls file
	 * 
	 * adduser | domain | DDPS.LOCAL
	 * adduser | user | kp1
	 * 
	 */
	public static void add_users_by_domain(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			Util.click(LoadProp.usersprop, "addUserByDomainBtn_xpath", test, driver);
			Util.waitUntilElementVisibility(driver,By.xpath(LoadProp.usersprop.get("addUserDialog_xpath")));
			
			//Select dropdown domain 
			Util.click(LoadProp.usersprop, "domainDropdown_xpath", test, driver);
			String domain_name = paramDict.get(testId+".domain");
			driver.findElement(By.xpath("//ul[@id='domain_listbox']/*[starts-with(text(), '" + domain_name + "')]")).click();
			test.log(LogStatus.INFO, "Select Domain: "+ domain_name);
			
			//Insert User
			String user_name = paramDict.get(testId+".user");
			Util.sendKeys(LoadProp.usersprop, "searchName_xpath", user_name, test, driver);
			test.log(LogStatus.INFO, "Insert User Name: "+ user_name);
			
			//Search Btn
			Util.click(LoadProp.usersprop, "searchUserBtn_xpath", test, driver);
			test.log(LogStatus.INFO, "Searching for user..");
			
			//Check Search Text
			Util.waitUntilElementVisibility(driver,By.xpath("//select[@id='userList']/option[1]"));
			String search_user_name = driver.findElement(By.xpath("//select[@id='userList']/option[1]")).getText();
			
			//Check User Name matched or not
			String acc_user = user_name;
			//String acc_user = user_name + " (" + user_name + "@" + domain_name +")";
			test.log(LogStatus.INFO, "Search User Name: "+ search_user_name);
			test.log(LogStatus.INFO, "Actual User Name: "+ acc_user);
			
			if(search_user_name.contains(acc_user)){
				test.log(LogStatus.INFO, "Search username and Actual username matched...");
				//Add User from list
				driver.findElement(By.xpath("//select[@id='userList']/option[1]")).click();
				Util.click(LoadProp.usersprop, "addUserBtn_xpath", test, driver);
				test.log(LogStatus.INFO, "New User added: " + acc_user);
				
				//User Page 
				Util.click(LoadProp.usersprop, "userPageRefreshBtn_xpath", test, driver);
				Util.waitUntilElementVisibility(driver,By.xpath("//tbody[@role='rowgroup']"));
				
				//int pagination = 1;
				//int item_count = 25;
				
				//String page = driver.findElement(By.xpath("//div[@data-role='pager']/ul/li")).getText();
				//Util.reportPass(driver,test, "Page: " + page);
				
				List<WebElement> page = driver.findElements(By.xpath("//div[@data-role='pager']/ul/li"));
				String page_size = String.valueOf(page.size());
				int ac_page_size = Integer.parseInt(page_size) - 1;
				
				for(int i=1; i<=ac_page_size; i++){
					int optUser = 1;
					String find_user = "false";
					//Util.reportPass(driver,test, "Find User: " + find_user);
					
					for(int j=1; j<=25; j++){
						String row = driver.findElement(By.xpath("//tbody[@role='rowgroup']/tr["+ optUser +"]/td[3]")).getText();
						//Util.reportPass(driver,test, "Row: " + row);
						//Find User
						if(row.contains(acc_user)){
							find_user = "true";
							//Util.reportPass(driver,test, "Find User: " + find_user);
							break;
						}
						optUser++;
						}
					
					//Find User
					if(find_user == "true"){
						Util.reportPass(driver,test, "User found successfully: " + find_user);
						break;
					}else{
						Util.reportPass(driver,test, "Find User: " + find_user);
						test.log(LogStatus.INFO, "Pagination Next Page Users"+ i);
						Util.click(LoadProp.usersprop, "paginationNextPageUsers_xpath", test, driver);
					}
				}
			}else{
				Util.reportPass(driver,test, "Failed to find "+ acc_user +" from list..");
				Assert.assertEquals("", "none");
			}
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}

	public static void perform_arbitration(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			int i = 1;
			test.log(LogStatus.INFO, "Inside function"+paramDict.get(testId+".level1"));
			test.log(LogStatus.INFO, LoadProp.getProperty("web_weburl")+"manage"+paramDict.get(testId+".level"+i));
				
			while(i>=1){
				test.log(LogStatus.INFO, LoadProp.getProperty("web_weburl")+"manage"+paramDict.get(testId+".level"+i));
				Util.access_link(LoadProp.getProperty("weburl"+"manage"+paramDict.get(testId+".level"+i)), test, driver);
				//serve.clear(driver, By.xpath(homePageProp.get("searchbar")), paramDict.get("select.item"+i));
				Util.sendKeys(LoadProp.usersprop, "searchbar_xpath", paramDict.get(testId+".item"+i), test, driver);
				WebElement elem = driver.findElementByXPath(LoadProp.usersprop.get("searchbar_xpath"));
				elem.sendKeys(Keys.ENTER);
				//driver.switchTo().defaultContent();
				Util.waitUntilElementVisibility(driver,By.xpath("//a[@class='grid-link ng-binding' and contains(text(), '" + paramDict.get(testId+".item"+i) + "')]"));
				test.log(LogStatus.INFO, "Entity Searched!!");
				  
				Thread.sleep(1000);
				driver.findElement(By.xpath("//a[@class='grid-link ng-binding' and contains(text(), '" + paramDict.get(testId+".item"+i) + "')]")).click();
				Util.reportPass(driver,test, "Entity Clicked!!");
				Util.waitUntilElementVisibility(driver,By.xpath("//div[@class='page-title' and contains(text(), '" + paramDict.get(testId+".item"+i) + "')]"));
				
				Util.click(LoadProp.usersprop, "securitypoliciestab_xpath", test, driver);

				i++;
			}
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
}