package com.action.pages;

import java.util.Dictionary;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;

import com.properties.mapping.LoadProp;
import com.relevantcodes.extentreports.ExtentTest;
import com.utility.library.Util;

public class ServicesManagementPage {
	
	// ProductNotification On / off.
	public static void product_notification_off(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {	
		try{
			//Click On Product Notification Tab
			Util.click(LoadProp.servicesmngmtprop, "Product_Notification_Tab_xpath", test, driver);
			Util.reportPass(driver,test,"Click on Product Notification Tab");
			Thread.sleep(500);
					
			//Click On Product Notification ON
			Util.click(LoadProp.servicesmngmtprop, "product_Notification_On_button_xpath", test, driver);
			Util.click(LoadProp.servicesmngmtprop, "save_preference_on_xpath", test, driver);
			Util.reportPass(driver,test, "Click ON Product Notification button");
			Thread.sleep(800);
				
			// Refresh the Browser
			//serve.refreshbrowser(driver);
			//Thread.sleep(1500);
			
			//Click On Product Notification Tab
			Util.click(LoadProp.servicesmngmtprop, "Product_Notification_Tab_xpath", test, driver);
			Util.reportPass(driver,test,"Click on Product Notification Tab");
			Thread.sleep(500);
						
			WebElement productnotificationon = driver.findElement(By.xpath(LoadProp.servicesmngmtprop.get("product_notification_on_text_xpath")));
			String notf_msg = productnotificationon.getText();
			Assert.assertEquals(notf_msg, "You have enrolled to receive Product Notifications.");
					
			//Click On Product Notification OFF
			Util.click(LoadProp.servicesmngmtprop, "product_Notification_Off_button_xpath", test, driver);
			Util.click(LoadProp.servicesmngmtprop, "save_preference_off_xpath", test, driver);
			Util.reportPass(driver,test, "Click OFF Product Notification button");
				
			WebElement productnotificationoff = driver.findElement(By.xpath(LoadProp.servicesmngmtprop.get("product_notification_off_text_xpath")));
			String notf_msg_1 = productnotificationoff.getText();
			Assert.assertEquals(notf_msg_1, "You have not enrolled to receive Product Notifications.");
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
	public static void enable_product_notification(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {	
		try{
			Util.access_link(LoadProp.getProperty("web_weburl"+"servicesManagement"), test, driver);
			Thread.sleep(500);
			
			//Click On Product Notification Tab
			Util.click(LoadProp.servicesmngmtprop, "Product_Notification_Tab_xpath", test, driver);
			Util.reportPass(driver,test,"Clicked on Product Notification Tab");
			Thread.sleep(500);
			Util.click(LoadProp.servicesmngmtprop, "product_Notification_On_button_xpath", test, driver);
			Util.click(LoadProp.servicesmngmtprop, "save_preference_on_xpath", test, driver);
			Util.reportPass(driver,test, "Product Notification enabled");
			
			Util.access_link(LoadProp.getProperty("web_weburl"+"manageLogs"), test, driver);
			Thread.sleep(500);
			System.out.println(driver.findElement(By.xpath("//tbody[@role='rowgroup']/tr[1]/td[6]")).getText());
			Assert.assertEquals("Enrolled to receive product notifications.", driver.findElement(By.xpath("//tbody[@role='rowgroup']/tr[1]/td[6]")).getText());
			Util.reportPass(driver,test, "Successfully verified in logs");
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
	public static void disable_product_notification(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {	
		try{
			Util.access_link(LoadProp.getProperty("web_weburl"+"servicesManagement"), test, driver);
			Thread.sleep(500);
			
			//Click On Product Notification Tab
			Util.click(LoadProp.servicesmngmtprop, "Product_Notification_Tab_xpath", test, driver);
			Util.reportPass(driver,test,"Clicked on Product Notification Tab");
			Thread.sleep(500);
			
			Util.click(LoadProp.servicesmngmtprop, "product_Notification_Off_button_xpath", test, driver);
			Util.click(LoadProp.servicesmngmtprop, "save_preference_off_xpath", test, driver);
			Util.reportPass(driver,test, "Product Notification disabled");
			
			Util.access_link(LoadProp.getProperty("web_weburl"+"manageLogs"), test, driver);
			Thread.sleep(500);
			System.out.println(driver.findElement(By.xpath("//tbody[@role='rowgroup']/tr[1]/td[6]")).getText());
			Assert.assertEquals("Unenrolled for product notifications.", driver.findElement(By.xpath("//tbody[@role='rowgroup']/tr[1]/td[6]")).getText());
			Util.reportPass(driver,test, "Successfully verified in logs");
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
	//Advance Threat Protection Provisioning
	public static void atp_provision(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) { 
		try{
			//Click On Advanced Threat Protection Service Tab
			Util.click(LoadProp.servicesmngmtprop, "setup_atp_xpath", test, driver);
			Util.reportPass(driver,test,"Clicked on setup ATP link");
			Util.waitUntilElementVisibility(driver,By.xpath(LoadProp.recoverdataprop.get("service_setup_next_button")));
			
			//Click On Next button of Service Setup Pop page
			Util.click(LoadProp.servicesmngmtprop, "service_setup_next_button_xpath", test, driver);
			Util.reportPass(driver,test,"Clicked Next to continue service setup");
			Util.waitUntilElementVisibility(driver,By.xpath(LoadProp.recoverdataprop.get("Select_your_region")));

			//Click On Next button of Select your region pop page
			Util.click(LoadProp.servicesmngmtprop, "region_dropdown_xpath", test, driver);
			Util.click(LoadProp.servicesmngmtprop, "select_EURO_xpath", test, driver);
			Util.click(LoadProp.servicesmngmtprop, "Select_your_region_xpath", test, driver);
			Util.reportPass(driver,test,"Region selected and clicked next to continue");
			Util.waitUntilElementVisibility(driver,By.xpath(LoadProp.recoverdataprop.get("ATP_EULA_Accept")));
			
			//Accept ATP EULA
			Util.click(LoadProp.servicesmngmtprop, "ATP_EULA_Accept_xpath", test, driver);
			Util.click(LoadProp.servicesmngmtprop, "ATP_EULA_Next_step_xpath", test, driver);
			Util.reportPass(driver,test,"ATP EULA accepted and clicked next to continue");

			//Enter provisioning form details
		    /*serve.sendKeys(driver, By.xpath(servicemanagementProp.get("company_name")), paramDict.get(testId+".company_name"));
			serve.sendKeys(driver, By.xpath(servicemanagementProp.get("contact_name")), paramDict.get(testId+".contact_name"));
			serve.sendKeys(driver, By.xpath(servicemanagementProp.get("contact_email_address")), paramDict.get(testId+".contact_email_address"));
			serve.sendKeys(driver, By.xpath(servicemanagementProp.get("country")), paramDict.get(testId+".country"));*/
			
			Util.sendKeys(LoadProp.servicesmngmtprop, "company_name_xpath", "Company Name", test, driver);
			Util.sendKeys(LoadProp.servicesmngmtprop, "contact_name_xpath", "Contact Name", test, driver);
			Util.sendKeys(LoadProp.servicesmngmtprop, "contact_email_address_xpath", "ContactID@address", test, driver);
			Util.sendKeys(LoadProp.servicesmngmtprop, "country_xpath", "Country Name", test, driver);
			Util.click(LoadProp.servicesmngmtprop, "ATP_Form_Next_button_xpath", test, driver);
			Util.reportPass(driver,test,"Details entered and clicked On Next button");
			Util.waitUntilElementVisibility(driver,By.xpath(LoadProp.recoverdataprop.get("certificate_backup")));
			
			//Click, I backed up certificate checkbox
			Util.click(LoadProp.servicesmngmtprop, "certificate_backup_xpath", test, driver);
			Util.reportPass(driver,test,"Checked, I have downloaded the certificate");
			Util.click(LoadProp.servicesmngmtprop, "completeprovision_next_button_xpath", test, driver);
			Util.reportPass(driver,test,"Clicked next to complete ATP Provisioning");
			Thread.sleep(1000);
		    
			Util.click(LoadProp.servicesmngmtprop, "completeprovision_Ok_button_xpath", test, driver);
			Util.reportPass(driver,test,"Clicked on Ok to Exit ATP Provision");
			Util.waitUntilElementVisibility(driver,By.xpath(LoadProp.servicesmngmtprop.get("ATPProvisiongreencheck_xpath")));
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	 

		
}