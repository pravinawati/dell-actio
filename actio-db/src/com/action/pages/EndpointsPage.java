package com.action.pages;

import java.io.IOException;
import java.util.Dictionary;

import javax.print.DocFlavor.STRING;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;

import com.properties.mapping.LoadProp;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.utility.library.Global;
import com.utility.library.Util;

import freemarker.cache.SoftCacheStorage;

public class EndpointsPage {

	/*//Login to Web Application
	public static void search_and_click_endpoint(RemoteWebDriver driver, BufferedWriter logger, Dictionary<String, String> paramDict) throws IOException, InterruptedException  {
		log.logInfo(logger,"Search and click endpoint()");
		serve.waitUntil(ExpectedConditions.visibilityOfElementLocated(By.xpath(EndpointPageProp.get("search_endpoint_text"))), driver, 100);
		serve.click(driver, By.xpath(EndpointPageProp.get("search_endpoint_text")));
		serve.sendKeys(driver, By.xpath(EndpointPageProp.get("search_endpoint_text")), paramDict.get("searchandclickendpoint.search_endpoint_text"));
		Thread.sleep(500);
		WebElement ele= driver.findElementByXPath(EndpointPageProp.get("search_endpoint_text"));
		ele.sendKeys(Keys.ENTER);
		Thread.sleep(2000);
		serve.waitUntil(ExpectedConditions.visibilityOfElementLocated(By.xpath(EndpointPageProp.get("searched_endpoint_link"))), driver, 100);
		serve.click(driver, By.xpath(EndpointPageProp.get("searched_endpoint_link")));
		Thread.sleep(2000);
		log.logInfo(logger, "End Searched and clicked to view details ");
	}
	
	public static void view_endpoint(RemoteWebDriver driver, BufferedWriter logger, Dictionary<String, String> paramDict) throws IOException, InterruptedException  {
		log.logInfo(logger,"view endpoint details");
		serve.waitUntil(ExpectedConditions.visibilityOfElementLocated(By.xpath(EndpointPageProp.get("endpoint_details_details_text"))), driver, 100);
		serve.click(driver, By.xpath(EndpointPageProp.get("detailsandactions_tab")));
		serve.click(driver, By.xpath(EndpointPageProp.get("users_tab")));
		serve.click(driver, By.xpath(EndpointPageProp.get("endpointgroups_tab")));
		serve.click(driver, By.xpath(EndpointPageProp.get("threat_tab")));
		log.logInfo(logger, "Endpoint details viewed");
	}
	
	public static void endpoint_protection(RemoteWebDriver driver, BufferedWriter logger, Dictionary<String, String> paramDict) throws IOException, InterruptedException  {
		log.logInfo(logger,"Endpoint Protection verification");
		serve.click(driver, By.xpath(EndpointPageProp.get("policy_tab")));
		serve.waitUntil(ExpectedConditions.visibilityOfElementLocated(By.xpath(EndpointPageProp.get("threat_protection_link"))), driver, 100);
		serve.click(driver, By.xpath(EndpointPageProp.get("threat_protection_link")));
		if(driver.findElementByXPath(EndpointPageProp.get("endpoint_protection_on")).isDisplayed())
		{
			log.logInfo(logger, "Endpoint is protected");
		}
		else
		{
			log.logInfo(logger, "Endpoint is NOT PROTECTED");
		}
		serve.click(driver, By.xpath(EndpointPageProp.get("return_link")));
		serve.waitUntil(ExpectedConditions.visibilityOfElementLocated(By.xpath(EndpointPageProp.get("threat_protection_link"))), driver, 100);
		log.logInfo(logger, "Endpoint Protection verified");
 
	}*/
	
	public static void add_endpoints(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		// TODO Auto-generated method stub
		
		test.log(LogStatus.INFO, "Started Adding Endpoint .......");
		
		Runtime runtime = Runtime.getRuntime();
		try {
		    Process p1 = runtime.exec("cmd /c start activateAgent.bat");
		    //InputStream is = p1.getInputStream();
		    //int i = 0;
		    //while( (i = is.read() ) != -1) {
		      //  System.out.print((char)i);
		  //  }
		} catch(IOException ioException) {
		    System.out.println(ioException.getMessage() );
		}
		Util.reportPass(driver,test, "End Adding Endpoint .......");   
	}
	
	public static void add_regularshield(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		// TODO Auto-generated method stub
		
		test.log(LogStatus.INFO, "Started Adding Shield Endpoint .......");
		
		Runtime runtime = Runtime.getRuntime();
		try {
		    Process p2 = runtime.exec("cmd /c start activateRegularShield.bat");
		    //InputStream is = p1.getInputStream();
		    //int i = 0;
		    //while( (i = is.read() ) != -1) {
		      //  System.out.print((char)i);
		  //  }
		} catch(IOException ioException) {
		    System.out.println(ioException.getMessage() );
		}
		Util.reportPass(driver,test, "End Adding Shield Endpoint ......."); 
	}
	
	public static void add_agent(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		  // TODO Auto-generated method stub
		  
		test.log(LogStatus.INFO, "Started Adding Agent Endpoint .......");
		  
		  Runtime runtime = Runtime.getRuntime();
		  try {
		      Process p2 = runtime.exec("cmd /c start activateAgent.bat");
		      //InputStream is = p1.getInputStream();
		      //int i = 0;
		      //while( (i = is.read() ) != -1) {
		        //  System.out.print((char)i);
		    //  }
		  } catch(IOException ioException) {
		      System.out.println(ioException.getMessage() );
		  }
		  Util.reportPass(driver,test, "End Adding Agent Endpoint .......");   
	}

	public static void add_cloud(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		  // TODO Auto-generated method stub
		  
		test.log(LogStatus.INFO, "Started Adding Cloud Endpoint .......");
		  
		  Runtime runtime = Runtime.getRuntime();
		  try {
		      Process p2 = runtime.exec("cmd /c start activateCloud.bat");
		      //InputStream is = p1.getInputStream();
		      //int i = 0;
		      //while( (i = is.read() ) != -1) {
		        //  System.out.print((char)i);
		    //  }
		  } catch(IOException ioException) {
		      System.out.println(ioException.getMessage() );
		  }
		  Util.reportPass(driver,test, "End Adding Cloud Endpoint .......");   
	}
	
	public static void add_bitlocker(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		  // TODO Auto-generated method stub
		  
		test.log(LogStatus.INFO, "Started Adding bitlocker Endpoint .......");
		  
		  Runtime runtime = Runtime.getRuntime();
		  try {
		      Process p2 = runtime.exec("cmd /c start activateBitlocker.bat");
		      //InputStream is = p1.getInputStream();
		      //int i = 0;
		      //while( (i = is.read() ) != -1) {
		        //  System.out.print((char)i);
		    //  }
		  } catch(IOException ioException) {
		      System.out.println(ioException.getMessage() );
		  }
		  Util.reportPass(driver,test, "End Adding Bitlocker Endpoint .......");   
	}
	
	public void get_device_recovery_keys(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			String fileName = paramDict.get(testId+".endpoint_name")+".exe";
			Util.click(LoadProp.endpointsprop, "detailsandactions_tab_xpath", test, driver);
			Util.click(LoadProp.endpointsprop, "click_device_RecoveryKeys_xpath", test, driver);
			Util.reportPass(driver,test, "Clicked on Device Recovery Keys button");
		
			//Enter Device Recovery Keys password
			Util.sendKeys(LoadProp.endpointsprop, "password_box_xpath", "1", test, driver);
			Util.reportPass(driver,test, "Entered Device Recovery Keys password");
		
			//Click on Download button
			Util.click(LoadProp.endpointsprop, "click_download_xpath", test, driver);
			Util.reportPass(driver,test, "Clicked on Download button");
			Thread.sleep(3000);
							
		
			Util.verifyFileDownloaded(driver, LoadProp.getProperty("key_downloadPath"), fileName, test);
		
			Util.deleteFileDownloaded(driver, LoadProp.getProperty("key_downloadPath"), fileName, test);
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}	
	}
	
	public void get_recovery_id(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{

		Global.endpoint_recovery_id = Util.getText(LoadProp.endpointsprop, "recoveryid_xpath", test, driver);
		System.out.println(Global.endpoint_recovery_id);
		Thread.sleep(5000);

		Util.reportPass(driver,test, "Capture Recovery ID");
		Thread.sleep(5000);
		} catch(Exception|AssertionError e){
		Assert.fail(e.getMessage());
		}	
}


	public void shield_details(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			
			for(int i = 1; i < 6; i++) {
				WebElement x=driver.findElement(By.xpath("//div[@id='myContent']/div["+ i +"]/div/p"));
				if (x.getText().isEmpty()){
					Assert.assertEquals("Failed", "Data is Not Available");

				}else {
					System.out.println("["+ i +"]String = "+ x.getText());
					Assert.assertNotNull("Pass", "Data is Available");
					Util.reportPass(driver,test, "Shield Details");	
				}
				}
			} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
			}
		}
	
	
	/*public static void delete_endpoint(RemoteWebDriver driver, BufferedWriter logger, Dictionary<String, String> paramDict) throws IOException, InterruptedException  {
		log.logInfo(logger,"Deleting endpoint " + paramDict.get("deleteendpoint.endpoint"));
		Thread.sleep(2000);
		serve.click(driver, By.xpath("//*[contains(text(), '" + paramDict.get("deleteendpoint.endpoint") + "')]"));
		Thread.sleep(2000);
		serve.click(driver, By.xpath("//ul[@class='k-tabstrip-items k-reset']/li[2]"));
		serve.click(driver, By.xpath("//button[@class='toolbar-button ng-scope ng-isolate-scope' and @label='Remove']"));
		serve.click(driver, By.xpath("//button[contains(text(), 'Ok')]"));
		Thread.sleep(5000);
		if(driver.findElements(By.xpath("//*[contains(text(), '" + paramDict.get("deleteendpoint.endpoint") + "')]")).size()==0){
			log.logInfo(logger,"Endpoint deleted successfully");
		}
		
		serve.click(driver, By.xpath(EndpointPageProp.get("policy_tab")));
		serve.waitUntil(ExpectedConditions.visibilityOfElementLocated(By.xpath(EndpointPageProp.get("threat_protection_link"))), driver, 100);
		serve.click(driver, By.xpath(EndpointPageProp.get("threat_protection_link")));
		if(driver.findElementByXPath(EndpointPageProp.get("endpoint_protection_on")).isDisplayed())
		{
			log.logInfo(logger, "Endpoint is protected");
		}
		else
		{
			log.logInfo(logger, "Endpoint is NOT PROTECTED");
		}
		serve.click(driver, By.xpath(EndpointPageProp.get("return_link")));
		serve.waitUntil(ExpectedConditions.visibilityOfElementLocated(By.xpath(EndpointPageProp.get("threat_protection_link"))), driver, 100);
		log.logInfo(logger, "Endpoint Protection verified");
 
	}
	
	public static void agent_endpoint_sync(RemoteWebDriver driver, BufferedWriter logger, Dictionary<String, String> paramDict) throws IOException, InterruptedException  {
		
		log.logInfo(logger,"Verify SED Commands ..");
		
		serve.click(driver, By.xpath(EndpointPageProp.get("populationlnk")));
		serve.click(driver, By.xpath(EndpointPageProp.get("endptlnk")));
		
		serve.click(driver, By.linkText(EndpointPageProp.get("endpointname")));
		serve.click(driver,By.xpath("//li[@id='detail']/span[2]"));
		waitForPageLoad(driver);
		driver.manage().window().maximize();
		
		int lock_size=driver.findElements(By.xpath(EndpointPageProp.get("lock"))).size();
		if(lock_size==0){
			log.logInfo(logger,"Clicked on Lock Command" );
			Thread.sleep(3000);
			}
		
		Thread.sleep(3000);
		serve.waitUntil(ExpectedConditions.visibilityOfElementLocated(By.xpath(EndpointPageProp.get("lock"))), driver, 100);
		
		
		serve.click(driver, By.xpath(EndpointPageProp.get("lock")));
		Thread.sleep(3000);
		
	
		for(int i=0;i<5;i++){
		
			int ok_size=driver.findElements(By.xpath("//button[@id='resolveButton" + i +"']")).size();
			if(ok_size==1){
				
				serve.waitUntil(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@id='resolveButton" + i +"']")), driver, 100);
				serve.click(driver, By.xpath("//button[@id='resolveButton" + i +"']"));
				waitForPageLoad(driver);
				break;
				
			}
			
		}
				
		Thread.sleep(1000);
		
		serve.click(driver, By.xpath(EndpointPageProp.get("populationlnk")));
		serve.click(driver, By.xpath(EndpointPageProp.get("endptlnk")));
		
		serve.click(driver, By.linkText(EndpointPageProp.get("endpointname")));
		serve.click(driver,By.xpath("//li[@id='detail']/span[2]"));
		waitForPageLoad(driver);
		
		String lockedCmdStatus = "";
		lockedCmdStatus= driver.findElement(By.xpath("//div[@id='commandGrid']/div[3]/table/tbody/tr/td[5]")).getText();
		
		String sender="";
		String cmdReceviedDate="";
		String cmdSentDateTime="";
		
		if(lockedCmdStatus.trim().length() == 0)
		{
		
			Thread.sleep(60000);
			serve.click(driver, By.xpath(EndpointPageProp.get("populationlnk")));
			serve.click(driver, By.xpath(EndpointPageProp.get("endptlnk")));
			
			serve.click(driver, By.linkText(EndpointPageProp.get("endpointname")));
			serve.click(driver,By.xpath("//li[@id='detail']/span[2]"));
			
			log.logInfo(logger, "Command|Sent|Sender|Received|State");
			
			lockedCmdStatus = driver.findElement(By.xpath("//div[@id='commandGrid']/div[3]/table/tbody/tr/td[5]")).getText();
			String Sender = driver.findElement(By.xpath("//div[@id='commandGrid']/div[3]/table/tbody/tr/td[3]")).getText();
			String CMDReceviedDate = driver.findElement(By.xpath("//div[@id='commandGrid']/div[3]/table/tbody/tr/td[4]")).getText();
			String CMDSentDateTime = driver.findElement(By.xpath("//div[@id='commandGrid']/div[3]/table/tbody/tr/td[2]")).getText();
			log.logInfo(logger, "Lock|" + CMDSentDateTime +"|"+ Sender + "|"+CMDReceviedDate  +"|"+lockedCmdStatus);
			
		}
		
		//UN-LOCK
		
		log.logInfo(logger, " For UNLOCK ");
		log.logInfo(logger,"Clicked on Un-Lock Command");
		log.logInfo(logger, " Command|Sent|Sender|Received|State");
		serve.click(driver, By.xpath(EndpointPageProp.get("unlock")));
		Thread.sleep(3000);
		
		for(int i=0;i<10;i++){
			
			int ok_size=driver.findElements(By.xpath("//button[@id='resolveButton" + i +"']")).size();
			
			if(ok_size==1){
				serve.waitUntil(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@id='resolveButton" + i +"']")), driver, 100);
				serve.click(driver, By.xpath("//button[@id='resolveButton" + i +"']"));
				waitForPageLoad(driver);
				break;
			}
			
		}
		
		

		String unLockedCmdStatus = "";
		
		
			Thread.sleep(60000);
			serve.click(driver, By.xpath(EndpointPageProp.get("populationlnk")));
			serve.click(driver, By.xpath(EndpointPageProp.get("endptlnk")));
			
			serve.click(driver, By.linkText(EndpointPageProp.get("endpointname")));
			serve.click(driver,By.xpath("//li[@id='detail']/span[2]"));
							
			unLockedCmdStatus = driver.findElement(By.xpath("//div[@id='commandGrid']/div[3]/table/tbody/tr[2]/td[5]")).getText();
			 sender = driver.findElement(By.xpath("//div[@id='commandGrid']/div[3]/table/tbody/tr[2]/td[3]")).getText();
			 cmdReceviedDate = driver.findElement(By.xpath("//div[@id='commandGrid']/div[3]/table/tbody/tr[2]/td[4]")).getText();
			 cmdSentDateTime = driver.findElement(By.xpath("//div[@id='commandGrid']/div[3]/table/tbody/tr[2]/td[2]")).getText();
			
			log.logInfo(logger, "Un-Lock|" + cmdSentDateTime +"|"+ sender + "|"+cmdReceviedDate  +"|"+unLockedCmdStatus);
	
	    
	}
	
	
	public static void send_commands(RemoteWebDriver driver, BufferedWriter logger, Dictionary<String, String> paramDict) throws IOException, InterruptedException  {
		
		log.logInfo(logger,"Sending commands");
		
		int i=1;
		while(i>=1){
			String commandInput = paramDict.get("send.command"+i);
			if(commandInput!=null){
				serve.click(driver, By.xpath(EndpointPageProp.get("clickDetailsTab")));
				
				serve.waitUntil(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='commandGrid']/div/button/span[contains(text(), '"+paramDict.get("send.command"+i)+"')]")), driver, 100);
				serve.click(driver, By.xpath("//div[@id='commandGrid']/div/button/span[contains(text(), '"+paramDict.get("send.command"+i)+"')]"));
				log.logInfo(logger,"Clicked on "+paramDict.get("send.command"+i)+" command.");
				
				serve.waitUntil(ExpectedConditions.visibilityOfElementLocated(By.xpath(EndpointPageProp.get("clickYesToCommand"))), driver, 100);
				serve.click(driver, By.xpath(EndpointPageProp.get("clickYesToCommand")));
				log.logInfo(logger,"Clicked Yes for "+paramDict.get("send.command"+i)+" command.");
						
				WebElement notitextlock = driver.findElement(By.xpath(EndpointPageProp.get("sedcommandsentnotification")));
				System.out.println(notitextlock.getText());
				String notificationlock = notitextlock.getText();
				
				Assert.assertEquals("Command sent to the device", notificationlock);
				
				i++;
			}else{
				break;
			}
			
		}
		
		
		
	}
	
	public void agentinventory(RemoteWebDriver driver, BufferedWriter logger, Dictionary<String, String> paramDict) throws IOException, InterruptedException  {
		log.logInfo(logger,"Getting inventory received status");
		serve.waitUntil(ExpectedConditions.visibilityOfElementLocated(By.xpath(EndpointPageProp.get("agent_endpoint"))), driver, 100);
		serve.click(driver, By.xpath(EndpointPageProp.get("agent_endpoint")));
		Thread.sleep(5000);
		WebElement invreceived = driver.findElement(By.xpath(EndpointPageProp.get("inventoryreceived")));
		log.logInfo(logger,"Inventory value stored in element");
		String received = "Inventory Received:"+invreceived.getText();
		System.out.println(received);
		
		WebElement invprocessed = driver.findElement(By.xpath(EndpointPageProp.get("iventoryprocessed")));
		log.logInfo(logger,"Inventory value stored in element");
		String processed = "Inventory Received:"+invprocessed.getText();
		System.out.println(processed);
		
		Assert.assertEquals(received, processed);
		
	}
	
	
	public void waitForPageLoad(RemoteWebDriver d){
        String s="";
        
        while(!s.equals("complete")){
        s=(String) d.executeScript("return document.readyState");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        }

    }*/
	
}
