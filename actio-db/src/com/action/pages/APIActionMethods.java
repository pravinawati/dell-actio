package com.action.pages;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Dictionary;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.properties.mapping.LoadProp;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.utility.library.AssertUtil;
import com.utility.library.ConnectDB;
import com.utility.library.Util;

public class APIActionMethods {
	
	RequestSpecification httpsRequest;
	Response response;
	int statusCode;
	/**
	 * Test to create new item in Core using API end point.
	 * @param paramDict
	 * @param test
	 * @throws ParseException 
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void a_create_item_seller_api (Dictionary<String, String> paramDict , String testId, ExtentTest test) throws FileNotFoundException, IOException, ParseException{
		
			//get company item id 
    	String companyItemId = paramDict.get(testId+".companyitemid");
    	String listPrice = paramDict.get(testId+".listprice");
    	String minimumPrice = paramDict.get(testId+".minimumprice");
    		//set base url
		RestAssured.baseURI = LoadProp.getProperty("base_uri_core");
			//set required headers
		httpsRequest = RestAssured.given();
		httpsRequest.header("Content-Type", LoadProp.getProperty("content_type"));
		httpsRequest.header("PartnerToken", LoadProp.getProperty("partner_token"));
			//create json parser instance
		JSONParser parser = new JSONParser();
			//parse the json file and convert to JSONArray object
		JSONArray jsonArray = (JSONArray) parser.parse(new FileReader(System.getProperty("user.dir")+"//resources//jsonFiles//CreateItem.json"));
			// extract JSONObject from the array
		JSONObject object = (JSONObject)jsonArray.get(0);
			//replace the item id and item title in the object
		object.put("title", companyItemId+ " Title");
		object.put("itemId", companyItemId);
		object.put("listPrice", listPrice);
		object.put("minimumPrice", minimumPrice);
		
			//attache the body to the request and POST the request
		response = httpsRequest.body("[" + object + "]").post("items?partnerId=" + LoadProp.getProperty("partner_id") + "&acl=true");
			//get status code
		int statusCode = response.getStatusCode();
			//get JsonPath from the response
		JsonPath path = response.jsonPath();
		if(statusCode == 200){
			String responseItemId = path.get("entities[0].itemId");
			String responseTitle = path.get("entities[0].title");
				// Assert the response	
			SoftAssert sAssert= new SoftAssert();
			AssertUtil.performSoftAssertEqualsWithoutScreenshot(companyItemId, responseItemId, "Response Item Id do not match with the provided Company Item ID", test, sAssert);
			AssertUtil.performSoftAssertEqualsWithoutScreenshot(companyItemId+ " Title", responseTitle, "Response Item Title do not match with the provided Company Item Title", test, sAssert);
			try {
				sAssert.assertAll();
				Util.reportPassWithoutScreenshot(test, "Item is created successfully.  --- ItemId : " + responseItemId + " --- Item Title : " + responseTitle);
				System.out.println("Item Id : " + responseItemId + " -----  Item Title " + responseTitle);
			}catch( Exception e) {
				test.log(LogStatus.INFO, e.getMessage());
			}
		} else {
			String responseCode = path.get("messages[0].code");
			String responseDescription = path.get("messages[0].description");
			String responseSeverity = path.get("messages[0].severity");
				// report failure	
			Util.reportCriticalFailureWithoutScreenshot("Item not created. --- Status Code : " + statusCode + " --- Code : " + responseCode + " --- Description : " + responseDescription + " --- Severity : " + responseSeverity); 
		} 
	}
	
	
	

	
	/**
	 * Method to make offer an item
	 * @param paramDict parameter dictionary
	 * @param test ExtentTest instance
	 * @throws SQLException sql exception
	 * @throws FileNotFoundException exception
	 * @throws IOException exception
	 * @throws ParseException exception
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void a_make_offer_buyer_api(Dictionary<String, String> paramDict , String testId, ExtentTest test) throws SQLException, FileNotFoundException, IOException, ParseException{
		
		try{
		int transactionId = 0;
			// fetch the item id from database
		String companyItemId = paramDict.get(testId+".companyitemid");
		String buyerOfferAmount =  paramDict.get(testId+".buyerofferamount");
		int itemId = ConnectDB.getItemIdfromDB(companyItemId);
			// get transaction id for the supplied item id
		transactionId = s_get_transaction_id_api(itemId, "MO", test);
			// get request header
		s_get_request_header_core("base_uri_test1", "buyer_user_name");
			// parse json file and get json object
		JSONObject object = s_get_json_object("MakeOfferBuyer");
			//get entity object and replace id with Transaction Id
		JSONObject entityObject = (JSONObject) object.get("entity");
		entityObject.put("id",transactionId);
		entityObject.put("amount",buyerOfferAmount);

			// get item object and replace id, company item id and company item title
		JSONObject itemObject = (JSONObject) entityObject.get("item");
		itemObject.put("id",transactionId);
		itemObject.put("companyItemId",companyItemId);
		itemObject.put("title",companyItemId+" Title");
			// execute the counter offer request by seller
		s_execute_put_transaction_request(object, transactionId, companyItemId, "OFFER_MADE", "MO", "OFFER_MADE", "Make Offer by Buyer", test);
		
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
	/**
	 * Method to Buy Now by Buyer from CORE
	 * @param paramDict
	 * @param test
	 * @throws SQLException
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws ParseException
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void a_buy_now_buyer_api(Dictionary<String, String> paramDict , String testId, ExtentTest test) throws SQLException, FileNotFoundException, IOException, ParseException{
		
		int transactionId = 0;
			// fetch the item id from database
		String companyItemId = paramDict.get(testId+".companyitemid");
		String offerAmount =  paramDict.get(testId+".listprice");
		
		int itemId = ConnectDB.getItemIdfromDB(companyItemId);
			// get transaction id for the supplied item id
		transactionId = s_get_transaction_id_api(itemId, "IP", test);
			// get request header
		httpsRequest = s_get_request_header_core("base_uri_test1", "buyer_user_name");
			// parse json file to extract json object
		JSONObject object = s_get_json_object("BuyNowBuyer");
			//get entity object and replace id with Transaction Id
		JSONObject entityObject = (JSONObject) object.get("entity");
		entityObject.put("id",transactionId);
		entityObject.put("amount",offerAmount);

			// get item object and replace id, company item id and company item title
		JSONObject itemObject = (JSONObject) entityObject.get("item");
		itemObject.put("id",transactionId);
		itemObject.put("companyItemId",companyItemId);
		itemObject.put("title",companyItemId+" Title");
		// execute the counter offer request by seller
		s_execute_put_transaction_request(object, transactionId, companyItemId, "SALE_PENDING", "IP", "SALE_PENDING", "Buy Now by Buyer", test);
	}
	
	
	/**
	 * Method to accept offer by seller
	 * @param paramDict parameter dictionary
	 * @param test ExtentTest instance
	 * @throws SQLException exception
	 * @throws FileNotFoundException exception
	 * @throws IOException exception
	 * @throws ParseException exception
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void a_accept_offer_seller_api(Dictionary<String, String> paramDict , String testId, ExtentTest test) throws SQLException, FileNotFoundException, IOException, ParseException{
		
			// get user specified company item id from parameter dictionary
		String companyItemId = paramDict.get(testId+".companyitemid");
		String transactionType = paramDict.get(testId+".transactiontype");
		
			// fetch item id from DB using the companyItemId
		int itemId = ConnectDB.getItemIdfromDB(companyItemId);
			// fetch transaction id from DB using item id
		int transactionId  = ConnectDB.getTransactionIdFromDB(itemId);
			// get request header
		s_get_request_header_core("base_uri_test1", "seller_user_name");
			// parse json file and extract json object
		JSONObject object = s_get_json_object("AcceptOfferSeller");
			//get entity object and replace id with Transaction Id
		JSONObject entityObject = (JSONObject) object.get("entity");
		entityObject.put("id",transactionId);
			// get item object and replace id, company item id and company item title
		JSONObject itemObject = (JSONObject) entityObject.get("item");
		itemObject.put("id",transactionId);
		itemObject.put("companyItemId",companyItemId);
		itemObject.put("title",companyItemId+" Title");
		// execute the counter offer request by seller		
		if(transactionType.equalsIgnoreCase("BN")){

			entityObject.put("transactionType","IP");
			entityObject.put("transactionStatus","PURCHASE_SELLER_CONFIRMED");
			entityObject.put("transactionReportTransactionStatus","SALE_PENDING");
			s_execute_put_transaction_request(object, transactionId, companyItemId, "PURCHASE_SELLER_CONFIRMED", "IP", "PURCHASE_APPROVED", "Accept Offer by Seller", test);
		
		} else if (transactionType.equalsIgnoreCase("MO")){
			entityObject.put("transactionType","MO");
			entityObject.put("transactionStatus","OFFER_SELLER_ACCEPTED");
			entityObject.put("transactionReportTransactionStatus","OFFER_ACCEPTED");
			s_execute_put_transaction_request(object, transactionId, companyItemId, "OFFER_SELLER_ACCEPTED", "MO", "OFFER_ACCEPTED", "Accept Offer by Seller", test);
		}
	}

	
	/**
	 * Method to counter offer by Seller using API
	 * @param paramDict parameter dictionary
	 * @param test ExtentTest instance
	 * @throws SQLException exception
	 * @throws FileNotFoundException exception
	 * @throws IOException exception
	 * @throws ParseException exception
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void a_counter_offer_seller_api(Dictionary<String, String> paramDict ,  String testId, ExtentTest test) throws SQLException, FileNotFoundException, IOException, ParseException{
		
			// get user specified company item id from parameter dictionary
		String companyItemId = paramDict.get(testId+".companyitemid");
		String sellerCounterAmount= paramDict.get(testId+".sellercounteramount");
			// fetch item id from DB using the companyItemId
		int itemId = ConnectDB.getItemIdfromDB(companyItemId);
			// fetch transaction id from DB using item id
		int transactionId  = ConnectDB.getTransactionIdFromDB(itemId);
			// get request header
		httpsRequest = s_get_request_header_core("base_uri_test1", "seller_user_name");
			// parse json file to extract json object
		JSONObject object = s_get_json_object("CounterOfferSeller");
		JSONObject entityObject = (JSONObject) object.get("entity");
		entityObject.put("id",transactionId);
		entityObject.put("amount",sellerCounterAmount);
		
			// get item object and replace id, company item id and company item title
		JSONObject itemObject = (JSONObject) entityObject.get("item");
		itemObject.put("id",transactionId);
		itemObject.put("companyItemId",companyItemId);
		itemObject.put("title",companyItemId+" Title");
			// execute the counter offer request by seller 
		s_execute_put_transaction_request(object, transactionId, companyItemId, "OFFER_SELLER_COUNTERED", "MO", "OFFER_COUNTERED", "Counter Offer by Seller", test);
	}
		
	/**
	 * Method to Create Payment Summary by Seller
	 * @param paramDict parameter dictionary
	 * @param test ExtentTest instance
	 * @throws SQLException exception
	 * @throws FileNotFoundException exception
	 * @throws IOException exception
	 * @throws ParseException exception
	 */
	@SuppressWarnings("unchecked")
	public void a_create_payment_summary_seller_api(Dictionary<String, String> paramDict , String testId, ExtentTest test) throws SQLException, FileNotFoundException, IOException, ParseException{
		
			// get user specified company item id from parameter dictionary
		String companyItemId = paramDict.get(testId+".companyitemid");
			// fetch item id from DB using the companyItemId
		int itemId = ConnectDB.getItemIdfromDB(companyItemId);
			// fetch transaction id from DB using item id
		int transactionId  = ConnectDB.getTransactionIdFromDB(itemId);
			// get request header
		s_get_request_header_core("base_uri_test1", "seller_user_name");
			// parse json file and extract json object
		JSONObject object = s_get_json_object("CreatePaymentSummarySeller");
		//replace Transaction Id
		object.put("transactionId",transactionId);
		long price = (long) object.get("price");
		long total = (long) object.get("total");
			//attached updated json object to request and execute the request
		response = httpsRequest.body(object).post("paymentSummary/");
			// get status code
		int statusCode = response.getStatusCode();
		JsonPath path = response.jsonPath();
		
		if(statusCode == 201){
			int responseTransactionId = path.getInt("transactionId");
			long responsePrice = (long) object.get("price");
			long responseTotal = (long) object.get("total");

			SoftAssert sAssert= new SoftAssert();
			AssertUtil.performSoftAssertEqualsWithoutScreenshot(String.valueOf(price), String.valueOf(responsePrice), "Price is incorrect : " + responsePrice, test, sAssert);
			AssertUtil.performSoftAssertEqualsWithoutScreenshot(String.valueOf(total), String.valueOf(responseTotal), "Total is incorrect : " + responseTotal, test, sAssert);

			try {
				sAssert.assertAll();
				Util.reportPassWithoutScreenshot(test, "Create Payment Summary by Seller is successful."
						+ " --- Transaction Id : " + responseTransactionId + " --- Transaction Price : " + responsePrice +
						" --- Transaction Total Price : " + responseTotal);
				System.out.println("Create Payment Summary by Seller is successful. : " + responseTransactionId);
			}catch( Exception e) {
				test.log(LogStatus.INFO, e.getMessage());
			}
		} else {
			String responseMessage = path.getString("message");
			Util.reportCriticalFailureWithoutScreenshot("Create Payment Summary by Seller is NOT successfull.  --- Message : " + responseMessage);

		}
	}
	
	
	/**
	 * Method to request funds via proxy pay by seller
	 * @param paramDict parameter dictionary
	 * @param test ExtentTest instance
	 * @throws SQLException exception
	 * @throws FileNotFoundException exception
	 * @throws IOException exception
	 * @throws ParseException exception
	 */
	@SuppressWarnings("unchecked")
	public void a_request_fund_via_proxypay_seller_api(Dictionary<String, String> paramDict , String testId, ExtentTest test) throws SQLException, FileNotFoundException, IOException, ParseException{
		
			// get user specified company item id from parameter dictionary
		String companyItemId = paramDict.get(testId+".companyitemid");
			// fetch item id from DB using the companyItemId
		int itemId = ConnectDB.getItemIdfromDB(companyItemId);
			// fetch transaction id from DB using item id
		int transactionId  = ConnectDB.getTransactionIdFromDB(itemId);
			// get request header
		httpsRequest = s_get_request_header_core("base_uri_paysafe", "seller_user_name");
			// parse json file to extract json object
		JSONObject object = s_get_json_object("RequestFundsViaProxyPaySeller");
			//replace Transaction Id
		object.put("transactionId",transactionId);
			//attached updated json object to request and execute the request
		response = httpsRequest.body(object).post("/requestFunds");
		// get status code
		int statusCode = response.getStatusCode();
		JsonPath path = response.jsonPath();
	
		if(statusCode == 200){
			String responseData = path.getString("data");
			String responseStatus = path.getString("status");

			SoftAssert sAssert= new SoftAssert();
			AssertUtil.performSoftAssertEqualsWithoutScreenshot("Request Sent", responseData, "Response data is incorrect : " + responseData, test, sAssert);
			AssertUtil.performSoftAssertEqualsWithoutScreenshot("SUCCESS", responseStatus, "Response status is incorrect : " + responseStatus, test, sAssert);

		try {
			sAssert.assertAll();
			Util.reportPassWithoutScreenshot(test, "Request Funds vis ProxyPay by Seller is successful. --- Transaction data : " + responseData + " --- Transaction status : " + responseStatus);
			System.out.println("Request Funds vis ProxyPay by Seller is successful. : " + responseStatus);
		}catch( Exception e) {
			test.log(LogStatus.INFO, e.getMessage());
		}
		}
	}
	
	
	/**
	 *  Method for Payment Instruction Sent by Seller using api
	 * @param paramDict  parameter dictionary
	 * @param testId test case id
	 * @param test ExtentTest instance
	 * @throws SQLException exception
	 * @throws FileNotFoundException exception
	 * @throws IOException exception
	 * @throws ParseException exception
	 */
	@Test
	public void a_payment_instruction_sent_seller_api(Dictionary<String, String> paramDict, String testId, ExtentTest test) throws SQLException, FileNotFoundException, IOException, ParseException{
		
			// get user specified company item id from parameter dictionary
		String companyItemId = paramDict.get(testId+".companyitemid");
			// fetch item id from DB using the companyitemId
		int itemId = ConnectDB.getItemIdfromDB(companyItemId);
			// fetch transaction id from DB using item id
		int transactionId  = ConnectDB.getTransactionIdFromDB(itemId);
			// get request header
		httpsRequest = s_get_request_header_core("base_uri_test1", "seller_user_name");
			// parse json file to extract json object
		JSONObject object = s_get_json_object("PaymentInstructionSentSeller");
			// execute the counter offer request by seller 
		s_execute_put_transaction_request(object, transactionId, companyItemId, "PAYMENT_INSTRUCTIONS_SENT", "MO", "PAYMENT_INSTRUCTIONS_SENT", "Payment Instruction Sent by Seller", test);
	}
	
	
	/**
	 *  Method for Decline request by Seller using api - only for Buy Now
	 * @param paramDict  parameter dictionary
	 * @param testId test case id
	 * @param test ExtentTest instance
	 * @throws SQLException exception
	 * @throws FileNotFoundException exception
	 * @throws IOException exception
	 * @throws ParseException exception
	 */
	@Test
	public void a_purchase_rejected_seller_api(Dictionary<String, String> paramDict, String testId, ExtentTest test) throws SQLException, FileNotFoundException, IOException, ParseException{
		
			// get user specified company item id from parameter dictionary
		String companyItemId = paramDict.get(testId+".companyitemid");
			// fetch item id from DB using the companyitemId
		int itemId = ConnectDB.getItemIdfromDB(companyItemId);
			// fetch transaction id from DB using item id
		int transactionId  = ConnectDB.getTransactionIdFromDB(itemId);
			// get request header
		httpsRequest = s_get_request_header_core("base_uri_test1", "seller_user_name");
			// parse json file to extract json object
		JSONObject object = s_get_json_object("DeclineRequestSeller");
			// execute the counter offer request by seller 
		s_execute_put_transaction_request(object, transactionId, companyItemId, "PURCHASE_SELLER_REJECTED", "IP", "PURCHASE_DECLINED", "Purchase Declined by Seller", test);
	}
	
	/**
	 *  Method to Withdraw Offer by Seller using api - only for MO
	 * @param paramDict  parameter dictionary
	 * @param testId test case id
	 * @param test ExtentTest instance
	 * @throws SQLException exception
	 * @throws FileNotFoundException exception
	 * @throws IOException exception
	 * @throws ParseException exception
	 */
	@Test
	public void a_withdraw_offer_seller_api(Dictionary<String, String> paramDict, String testId, ExtentTest test) throws SQLException, FileNotFoundException, IOException, ParseException{
		
			// get user specified company item id from parameter dictionary
		String companyItemId = paramDict.get(testId+".companyitemid");
			// fetch item id from DB using the companyitemId
		int itemId = ConnectDB.getItemIdfromDB(companyItemId);
			// fetch transaction id from DB using item id
		int transactionId  = ConnectDB.getTransactionIdFromDB(itemId);
			// get request header
		httpsRequest = s_get_request_header_core("base_uri_test1", "seller_user_name");
			// parse json file to extract json object
		JSONObject object = s_get_json_object("WithdrawOfferSeller");
			// execute the counter offer request by seller 
		s_execute_put_transaction_request(object, transactionId, companyItemId, "SALE_SELLER_CANCELLED", "MO", "SALE_CANCELLED", "Withdraw Offer by Seller", test);
	}
	
	
	/**
	 * Method to initiate paysafe transaction
	 * @param paramDict Parameter Dictionary
	 * @param testId Current test id 
	 * @param test ExtentTest instance
	 * @throws SQLException exception
	 * @throws FileNotFoundException exception
	 * @throws IOException exception
	 * @throws ParseException exception
	 * @throws InterruptedException 
	 */
	@SuppressWarnings("unchecked")
	public void a_initiate_paysafe_transaction_api (Dictionary<String, String> paramDict, String testId, ExtentTest test) throws SQLException, FileNotFoundException, IOException, ParseException, InterruptedException{
		// get user specified company itme id from the parameter dictionary
		String companyItemId = paramDict.get(testId+".companyitemid");
		// fetch item id from DB using the companyitemId
		int itemId = ConnectDB.getItemIdfromDB(companyItemId);
		// fetch transaction id from DB using item id
		int transactionId  = ConnectDB.getTransactionIdFromDB(itemId);
		// fetch payment summary id 
		int paymentSummaryId = ConnectDB.getPaymentSummaryIdFromDB(transactionId);
		
		// get request header
		httpsRequest = s_get_request_header_paysafe("base_uri_paysafe_transaction");
		// parse json file to extract json object
		JSONObject object = s_get_json_object("PaySafeInitiateTransaction");
		//replace Transaction Id
		object.put("id",transactionId);
		// replace name by companyitem id
		object.put("name",companyItemId);
		// replace payment summary id
		object.put("paymentSummaryId",paymentSummaryId);
		
		//attache the body to the request and POST the request
		response = httpsRequest.body(object).post("paySafe/transaction/");
		//get status code
		int statusCode = response.getStatusCode();
		if(statusCode == 201){
			System.out.println("statusCode : " + statusCode );
			Util.reportPassWithoutScreenshot(test, "PaySafe Transactions initiated successfully.");
			Thread.sleep(10000);
		} else {
			Util.reportCriticalFailureWithoutScreenshot("PaySafe Transactions initiation is NOT successfull.  --- Status Code : " + statusCode );
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public void a_activate_paysafe_transaction_api (Dictionary<String, String> paramDict, String testId, ExtentTest test) throws SQLException, FileNotFoundException, IOException, ParseException, InterruptedException{

		Thread.sleep(10000);
		// get user specified company itme id from the parameter dictionary
		String companyItemId = paramDict.get(testId+".companyitemid");
		// fetch item id from DB using the companyitemId
		int itemId = ConnectDB.getItemIdfromDB(companyItemId);
		// fetch transaction id from DB using item id
		int transactionId  = ConnectDB.getTransactionIdFromDB(itemId);
		// fetch payment summary id 
		int paymentSummaryId = ConnectDB.getPaymentSummaryIdFromDB(transactionId);
		// fetch payment id
		int paymentId = ConnectDB.getPaymentIdFromDB(paymentSummaryId);
		
		// get request header
		httpsRequest = s_get_request_header_paysafe("base_uri_paysafe_transaction");
		// parse json file to extract json object
		JSONObject object = s_get_json_object("ActivatePaySafeTransaction");
		//replace External Id with Payment Id
		object.put("ExternalId",paymentId);
		//attache the body to the request and POST the request
		response = httpsRequest.body(object).post("paySafe/callback/notification");
		//get status code
		int statusCode = response.getStatusCode();
		if(statusCode == 200){
			System.out.println("statusCode : " + statusCode );
			Util.reportPassWithoutScreenshot(test, "PaySafe Transactions Activated successfully.");
			
			Thread.sleep(10000);
		} else {
			Util.reportCriticalFailureWithoutScreenshot("PaySafe Transactions Activation is NOT successfull.  --- Status Code : " + statusCode );
		}

		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/** =========================== SUB ACTIONS =========================== **/
	
	/**
	 * Sub action method to set request header
	 * @param baseUri String base url
	 * @return RequestSpecification instance
	 */
	public RequestSpecification s_get_request_header_core(String baseUri, String userName){

		RestAssured.baseURI = LoadProp.getProperty(baseUri);
			// set required headers
		httpsRequest = RestAssured.given();
		httpsRequest.header("Content-Type", LoadProp.getProperty("content_type"));
		httpsRequest.header("api-key", LoadProp.getProperty("api_key"));
		httpsRequest.header("user-name", LoadProp.getProperty(userName));
		return httpsRequest;
	}
	
	/**
	 * Sub action method to set request header
	 * @param baseUri String base url
	 * @return RequestSpecification instance
	 */
	public RequestSpecification s_get_request_header_paysafe(String baseUri){

		RestAssured.baseURI = LoadProp.getProperty(baseUri);
			// set required headers
		httpsRequest = RestAssured.given();
		httpsRequest.header("Content-Type", LoadProp.getProperty("content_type"));
		httpsRequest.header("Accept", LoadProp.getProperty("content_type"));
		httpsRequest.header("Authorization", LoadProp.getProperty("authorization"));
		return httpsRequest;
	}

	/**
	 * Sub action method to parse json file and extract json object from it
	 * @param filename json file name without extension
	 * @return json object
	 * @throws FileNotFoundException exception
	 * @throws IOException exception
	 * @throws ParseException exception
	 */
	public JSONObject s_get_json_object(String filename) throws FileNotFoundException, IOException, ParseException{
		//create json parser instance
		JSONParser parser = new JSONParser();
			//parse the json file and convert to JSONArray object
		JSONArray jsonArray = (JSONArray) parser.parse(new FileReader(System.getProperty("user.dir")+"//resources//jsonFiles//" + filename + ".json"));
			// extract JSONObject from the array
		JSONObject object = (JSONObject) jsonArray.get(0);
		return object;
	}
	
	/**
	 * Method to return Transaction ID for the selected item
	 * @param itemId User defined company item id
	 * @param test ExtentTest instance
	 * @return transaction id of the item
	 */
	public int s_get_transaction_id_api(int itemId, String transactionType, ExtentTest test){
		
		int responseTransactionId=0;
			// get request header
		s_get_request_header_core("base_uri_test1", "buyer_user_name");
			// collect response for the POST request
		response = httpsRequest.get("transactions?itemId=" + itemId + "&type="+transactionType);
			// get status code
		statusCode = response.getStatusCode();
			// get Json Path of the response
		JsonPath path = response.jsonPath();
				
		if(statusCode == 200){
			responseTransactionId = path.getInt("entities[0].id");
			Util.reportPassWithoutScreenshot(test, "Item is fetched successfully. \n Transaction Id : " + responseTransactionId);
		} else {
				// get response error message
			String responseMessage = path.getString("message");
				// report failure
			Util.reportCriticalFailureWithoutScreenshot("Item is not fetched successfully. \n Message : " + responseMessage);
		}
		return responseTransactionId;
	}
	
	
	/**
	 * Method to execute PUT request for Transaction
	 * @param object JSON object
	 * @param transactionId transaction id
	 * @param companyItemId company item id
	 * @param transactionStatus transaction status
	 * @param transactionType transaction type
	 * @param transactionReportStatus transaction report status
	 * @param message sub message
	 * @param test ExtentTest instance
	 */
	@SuppressWarnings("unchecked")
	public void s_execute_put_transaction_request(JSONObject object, int transactionId, String companyItemId, String transactionStatus, String transactionType, String transactionReportStatus, String message, ExtentTest test ){

		JSONObject entityObject = (JSONObject) object.get("entity");
		entityObject.put("id",transactionId);
			// get item object and replace id, company item id and company item title
		JSONObject itemObject = (JSONObject) entityObject.get("item");
		itemObject.put("id",transactionId);
		itemObject.put("companyItemId",companyItemId);
		itemObject.put("title",companyItemId+" Title");
		
			//attached updated json object to request and execute the request
		response = httpsRequest.body(object).put("transactions/"+transactionId);
			// get status code
		int statusCode = response.getStatusCode();
		JsonPath path = response.jsonPath();
		if(statusCode == 200){

			int responseTransactionId = path.getInt("entities[0].id");
			String responseTransactionType = path.getString("entities[0].transactionType");
			String responseTransactionStatus = path.getString("entities[0].transactionStatus");
			String responseTransactionReportStatus = path.getString("entities[0].transactionReportTransactionStatus");
				
				// Assert the response	
			SoftAssert sAssert= new SoftAssert();
			
			AssertUtil.performSoftAssertEqualsWithoutScreenshot(Integer.toString(transactionId), Integer.toString(responseTransactionId), "Transaction Id is incorrect : --- Expected : " +  transactionId + " : --- Actual : " + responseTransactionId, test, sAssert);
			AssertUtil.performSoftAssertEqualsWithoutScreenshot(transactionStatus, responseTransactionStatus, "Transaction Status is incorrect : " + responseTransactionStatus, test, sAssert);
			AssertUtil.performSoftAssertEqualsWithoutScreenshot(transactionType, responseTransactionType, "Transaction Type is incorrect :  --- Expected : " +  transactionType + " : --- Actual : " + responseTransactionType, test, sAssert);
			AssertUtil.performSoftAssertEqualsWithoutScreenshot(transactionReportStatus, responseTransactionReportStatus, "Transaction Report Status is incorrect : --- Expected : " +  transactionReportStatus + " : --- Actual : " + responseTransactionReportStatus, test, sAssert);
			try {
				sAssert.assertAll();
				Util.reportPassWithoutScreenshot(test, message + " is successful."
						+ " --- Transaction Id : " + responseTransactionId + " --- Transaction Type : " + responseTransactionType +
						" --- Transaction Status : " + responseTransactionStatus + " --- Transaction Report Status : " + responseTransactionReportStatus);
			}catch( Exception e) {
				test.log(LogStatus.INFO, e.getMessage());
			}
		} else {
			String responseMessage = path.getString("message");
			Util.reportCriticalFailureWithoutScreenshot(message + " is NOT successfull.  --- Status Code : " + statusCode + "  --- Message : " + responseMessage);
		}
	}
	
	
}
