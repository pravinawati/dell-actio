package com.action.pages;

import java.util.Dictionary;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.properties.mapping.LoadProp;
import com.relevantcodes.extentreports.ExtentTest;
import com.utility.library.Util;

public class NotificationsManagementPage {
	
	// Add Email ID
	public static void add_email_notification(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {	
		try{
			JavascriptExecutor js =(JavascriptExecutor)driver;
			//Navigate to 'Management >> Notification Management'
			//Util.clickManagement(driver, paramDict, test);
			//Util.click(LoadProp.notificationsmngmtprop, "Notification_Management_xpath", test, driver);
			//Click on Add button
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.notificationsmngmtprop.get("Add_Notification_Management_xpath")));
			Util.click(LoadProp.notificationsmngmtprop, "Add_Notification_Management_xpath", test, driver);
			Util.reportPass(driver,test, "Click Add button on Notification Management Page");
			Thread.sleep(500);
			
			//Enter Email ID and click on Cancel button
			Util.sendKeys(LoadProp.notificationsmngmtprop, "Add_Email_xpath", paramDict.get(testId+".addmailid"), test, driver);
			Util.reportPass(driver,test, "Email ID entered");
			Util.click(LoadProp.notificationsmngmtprop, "Cancel_Button_xpath", test, driver);
			Util.reportPass(driver,test, "Cick on Cancel button");
			
			//Click on Add button
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.notificationsmngmtprop.get("Add_Notification_Management_xpath")));
			Util.click(LoadProp.notificationsmngmtprop, "Add_Notification_Management_xpath", test, driver);
			Util.reportPass(driver,test, "Click Add button on Notification Management Page");
			Thread.sleep(500);
			
			
			//Enter Email ID to add notification
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.notificationsmngmtprop.get("Add_Email_xpath")));
			Util.sendKeys(LoadProp.notificationsmngmtprop, "Add_Email_xpath", paramDict.get(testId+".addmailid"), test, driver);
			//driver.findElement(By.xpath("//*[@id='addEditNotificationWin']/section/span")).click();
			Util.reportPass(driver,test, "Email ID entered");

			/*Thread.sleep(500);
			WebElement type_d = driver.findElement(By.xpath(LoadProp.notificationsmngmtprop.get("Type_Dropdown_xpath")));
	        js.executeScript("window.scrollTo(0,"+type_d.getLocation().x+")");
	        type_d.click();*/
	        
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.notificationsmngmtprop.get("Type_Dropdown_xpath")));
			Util.click(LoadProp.notificationsmngmtprop, "Type_Dropdown_xpath", test, driver);
			
			int type = 1;
			  
			while(type >= 1){
				String gettype = paramDict.get(testId+".type"+type);
				if(gettype!=null){
					Util.waitUntilElementVisibility(driver, By.xpath("//div[@id='context']/div/span[contains(text(), '"+paramDict.get(testId+".type"+type)+"')]"));
					driver.findElement(By.xpath("//div[@id='context']/div/span[contains(text(), '"+paramDict.get(testId+".type"+type)+"')]")).click();
					type++;
				}else{
					break;
				}
			}
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.notificationsmngmtprop.get("Type_Dropdown_xpath")));
			Util.click(LoadProp.notificationsmngmtprop, "Type_Dropdown_xpath", test, driver);
			Util.reportPass(driver,test, "Close type dropdown");
			
			//Thread.sleep(2000);
			//Edit Priority

			/*WebElement priority_d = driver.findElement(By.xpath(LoadProp.notificationsmngmtprop.get("Priority_Dropdown_xpath")));
	        js.executeScript("window.scrollTo(0,"+priority_d.getLocation().x+")");
	        priority_d.click();*/
			
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.notificationsmngmtprop.get("Priority_Dropdown_xpath")));
			Util.click(LoadProp.notificationsmngmtprop, "Priority_Dropdown_xpath", test, driver);
			
			int priority = 1;
			  
			while(priority >= 1){
				String getpriority = paramDict.get(testId+".priority"+priority);
				if(getpriority!=null){
					Util.waitUntilElementVisibility(driver, By.xpath("//div[@id='context']/div/span[contains(text(), '"+paramDict.get(testId+".priority"+priority)+"')]"));
					driver.findElement(By.xpath("//div[@id='context']/div/span[contains(text(), '"+paramDict.get(testId+".priority"+priority)+"')]")).click();
					priority++;
				}else{
					break;
				}
			}
			
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.notificationsmngmtprop.get("Priority_Dropdown_xpath")));
			Util.click(LoadProp.notificationsmngmtprop, "Priority_Dropdown_xpath", test, driver);
			Util.reportPass(driver,test, "Close priority dropdown");
			
			//Click Add button
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.notificationsmngmtprop.get("Add_Button_xpath")));
			Util.click(LoadProp.notificationsmngmtprop, "Add_Button_xpath", test, driver);
			Util.reportPass(driver,test, "Notification added");
		
			//Click on Cancel and Refresh button
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.notificationsmngmtprop.get("Cancel_Button_xpath")));
			Util.click(LoadProp.notificationsmngmtprop, "Cancel_Button_xpath", test, driver);
			Util.reportPass(driver,test, "Dialogue popup closed");

			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.notificationsmngmtprop.get("wrap_msg_xpath")));
			
			WebElement element = driver.findElement(By.xpath(LoadProp.notificationsmngmtprop.get("wrap_msg_xpath")));
			String email_add_msg = element.getText();
			Assert.assertEquals(email_add_msg, "The notification was successfully added.");
			Util.click(LoadProp.notificationsmngmtprop, "wrap_msg_close_xpath", test, driver);

			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.notificationsmngmtprop.get("Click_Refresh_Button_xpath")));
			Util.click(LoadProp.notificationsmngmtprop, "Click_Refresh_Button_xpath", test, driver);
			Util.waitUntilElementVisibility(driver,By.xpath("//div[@class='k-grid-content k-auto-scrollable']/table/tbody/tr/td/span[contains(text(), '" + paramDict.get(testId+".addmailid") + "')]"));
			Util.reportPass(driver,test, "Notification for Email ID: " + paramDict.get(testId+".addmailid") + " has been successfully added");
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}	
		
	//Edit Email Notification
	public static void edit_email_notification(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			//Navigate to 'Management >> Notification Management'
			//Util.clickManagement(driver, paramDict, test);
			//Util.click(LoadProp.notificationsmngmtprop, "Notification_Management_xpath", test, driver);
			
			//Select Email notification to edit
			String emailid = paramDict.get(testId+".editmailid");
			String filtertext1 = driver.findElement(By.xpath("//tbody[@role='rowgroup']/tr/td[1]/span[contains(text(), '"+emailid+"')]/../../td[3]")).getText();
			
			driver.findElement(By.xpath("//div[@class='k-grid-content k-auto-scrollable']/table/tbody/tr/td/span[contains(text(), '" + emailid + "')]")).click();
			Util.reportPass(driver,test, "Email ID: " + emailid + " selected.");
			//Thread.sleep(500);
			
			//Click on Edit button
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.notificationsmngmtprop.get("Click_Edit_Button_xpath")));
			Util.click(LoadProp.notificationsmngmtprop, "Click_Edit_Button_xpath", test, driver);
			Util.reportPass(driver,test, "Clicked on Edit button");
			Thread.sleep(500);
			
			//Enter Email ID
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.notificationsmngmtprop.get("Add_Email_xpath")));
			Util.clear(LoadProp.notificationsmngmtprop, "Add_Email_xpath", test, driver);
			Util.sendKeys(LoadProp.notificationsmngmtprop, "Add_Email_xpath", paramDict.get(testId+".changetomailid"), test, driver);
			Util.reportPass(driver,test, "Email ID modified to " + paramDict.get(testId+".changetomailid"));
		    //Thread.sleep(500);

		    //Edit Type
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.notificationsmngmtprop.get("Type_Dropdown_xpath")));
			Util.click(LoadProp.notificationsmngmtprop, "Type_Dropdown_xpath", test, driver);
			Util.reportPass(driver,test, "Click on Type dropdown");
			
			for(int type=1; type<=10; type++){
		       WebElement Edit_Email_Notification_Type = driver.findElement(By.xpath("//div[@id='context']/div["+ type +"]/input"));
	       
		       if(Edit_Email_Notification_Type.isSelected()){
		    	   driver.findElement(By.xpath("//div[@id='context']/div["+ type +"]/input")).click();
		    	   Util.reportPass(driver,test, "Notification Type["+ type +"]:Type is Uncheck");
		       }else{
		    	   driver.findElement(By.xpath("//div[@id='context']/div["+ type +"]/input")).click();
		    	   Util.reportPass(driver,test, "Notification Type["+ type +"]: Select");
		       }
			}
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.notificationsmngmtprop.get("Type_Dropdown_xpath")));
			Util.click(LoadProp.notificationsmngmtprop, "Type_Dropdown_xpath", test, driver);
			Util.reportPass(driver,test, "Close Type dropdown");
			
			
			//Edit Priority
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.notificationsmngmtprop.get("Priority_Dropdown_xpath")));
			Util.click(LoadProp.notificationsmngmtprop, "Priority_Dropdown_xpath", test, driver);
			Util.reportPass(driver,test, "Click on Priority dropdown");
			
					
			for(int priority=1; priority<=4; priority++){
				WebElement Edit_Email_Notification_Priority = driver.findElement(By.xpath("//div[@class='k-animation-container k-state-border-up']/div/ng-transclude/div[2]/div["+ priority +"]/input"));
			
				if(Edit_Email_Notification_Priority.isSelected()){
					driver.findElement(By.xpath("//div[@class='k-animation-container k-state-border-up']/div/ng-transclude/div[2]/div["+ priority +"]/input")).click();
					Util.reportPass(driver,test, "Notification Priority["+ priority +"]:Priority is Uncheck");
				}else{
					driver.findElement(By.xpath("//div[@class='k-animation-container k-state-border-up']/div/ng-transclude/div[2]/div["+ priority +"]/input")).click();
					Util.reportPass(driver,test, "Notification Priority["+ priority +"]: Select");
				}
			}
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.notificationsmngmtprop.get("Priority_Dropdown_xpath")));
			Util.click(LoadProp.notificationsmngmtprop, "Priority_Dropdown_xpath", test, driver);
			Util.reportPass(driver,test, "Close Priority dropdown");
			
			//Edit Frequency
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.notificationsmngmtprop.get("Email_freq_dropdown_xpath")));
			Util.click(LoadProp.notificationsmngmtprop, "Email_freq_dropdown_xpath", test, driver);
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.notificationsmngmtprop.get("Select_Freq_xpath")));
			Util.click(LoadProp.notificationsmngmtprop, "Select_Freq_xpath", test, driver);
			Util.reportPass(driver,test, "15 Minutes selected");
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.notificationsmngmtprop.get("Click_Update_Button_xpath")));
			Util.click(LoadProp.notificationsmngmtprop, "Click_Update_Button_xpath", test, driver);
			
			//Verify wrap message for notification updated
			WebElement element = driver.findElement(By.xpath(LoadProp.notificationsmngmtprop.get("wrap_msg_xpath")));
			String email_add_msg = element.getText();
			Assert.assertEquals(email_add_msg, "The notification was successfully updated.");
			Util.click(LoadProp.notificationsmngmtprop, "wrap_msg_close_xpath", test, driver);
			Util.reportPass(driver,test, "Notification updated");
			String changetoID=paramDict.get(testId+".changetomailid");
			String filtertext2 = driver.findElement(By.xpath("//tbody[@role='rowgroup']/tr/td[1]/span[contains(text(), '"+changetoID+"')]/../../td[3]")).getText();
			System.out.println(filtertext1);
			System.out.println(filtertext2);
			Assert.assertNotSame(filtertext2, filtertext1);
			
			int type = 1;
			int priority = 1;
			  
			while(type >= 1){
				String gettype = paramDict.get(testId+".type"+type);
				if(gettype!=null){
					if(!filtertext2.contains(gettype)){
						Util.reportPass(driver,test, "Verified");
					}else{
						Assert.assertEquals("", "none");
					}
					type++;
				}else{
					break;
				}
			}
			
			while(priority >= 1){
				String getpriority = paramDict.get(testId+".priority"+priority);
				if(getpriority!=null){
					if(!filtertext2.contains(getpriority)){
						Util.reportPass(driver,test, "Verified");
					}else{
						Assert.assertEquals("", "none");
					}
					priority++;
				}else{
					break;
				}
			}	
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
	//Delete Email Notification
	public static void delete_email_notification(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			//Navigate to 'Management >> Notification Management'
			//Util.clickManagement(driver, paramDict, test);
			//Util.click(LoadProp.notificationsmngmtprop, "Notification_Management_xpath", test, driver);
			
			//Select Email Id to be deleted
			String emailid = paramDict.get(testId+".deletemailid");
			driver.findElement(By.xpath("//div[@class='k-grid-content k-auto-scrollable']/table/tbody/tr/td/span[contains(text(), '" + emailid + "')]")).click();
			Util.reportPass(driver,test, "Email ID selected");
					
			//click delete button to delete notification
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.notificationsmngmtprop.get("Click_Delete_Button_xpath")));
			Util.click(LoadProp.notificationsmngmtprop, "Click_Delete_Button_xpath", test, driver);
			Util.reportPass(driver,test, "Clicked on Delete Button");
			Thread.sleep(500);
			
			//click OK confirmation
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.notificationsmngmtprop.get("Click_ok_Button_xpath")));
			Util.click(LoadProp.notificationsmngmtprop, "Click_ok_Button_xpath", test, driver);
			Thread.sleep(500);
			Util.reportPass(driver,test, "Clicked on OK Button");
			
			//Verify wrap message for notification deleted
			WebElement element = driver.findElement(By.xpath(LoadProp.notificationsmngmtprop.get("wrap_msg_xpath")));
			String email_add_msg = element.getText();
			Util.click(LoadProp.notificationsmngmtprop, "wrap_msg_close_xpath", test, driver);
			Assert.assertEquals(email_add_msg, "The notification was deleted.");
			Util.reportPass(driver,test, "Notification deleted");
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
}