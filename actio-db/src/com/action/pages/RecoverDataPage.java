package com.action.pages;

import java.util.Dictionary;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;

import com.properties.mapping.LoadProp;
import com.relevantcodes.extentreports.ExtentTest;
import com.utility.library.Util;

public class RecoverDataPage {
			
	public static void recover_ems(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			//Click on User tab
			Util.click(LoadProp.recoverdataprop, "click_users_tab_xpath", test, driver);
			Util.reportPass(driver,test, "Clicked on USer tab");
			
			//Click on Recover option
			driver.findElement(By.xpath("//*[contains(text(), '" + paramDict.get(testId+".user") + "')]/../../td[7]/a")).click();
			Util.reportPass(driver,test, "Clicked on Recover option");
			
			Util.waitUntilElementVisibility(driver,By.xpath(LoadProp.recoverdataprop.get("shield_generate_response_button_xpath")));
			
			//Enter Challenge code
			Util.sendKeys(LoadProp.recoverdataprop, "challege_ems_textbox_1_xpath", paramDict.get(testId+".code_1"), test, driver);
			Util.sendKeys(LoadProp.recoverdataprop, "challege_ems_textbox_2_xpath", paramDict.get(testId+".code_2"), test, driver);
			Util.sendKeys(LoadProp.recoverdataprop, "challege_ems_textbox_3_xpath", paramDict.get(testId+".code_3"), test, driver);
			Util.sendKeys(LoadProp.recoverdataprop, "challege_ems_textbox_4_xpath", paramDict.get(testId+".code_4"), test, driver);
			Util.sendKeys(LoadProp.recoverdataprop, "challege_ems_textbox_5_xpath", paramDict.get(testId+".code_5"), test, driver);
			Util.sendKeys(LoadProp.recoverdataprop, "challege_ems_textbox_6_xpath", paramDict.get(testId+".code_6"), test, driver);
			Util.sendKeys(LoadProp.recoverdataprop, "challege_ems_textbox_7_xpath", paramDict.get(testId+".code_7"), test, driver);
			Util.sendKeys(LoadProp.recoverdataprop, "challege_ems_textbox_8_xpath", paramDict.get(testId+".code_8"), test, driver);
			Util.reportPass(driver,test, "Entered challenge code");
			
			//Click on Generate Response button
			Util.click(LoadProp.recoverdataprop, "shield_generate_response_button_xpath", test, driver);
			Util.reportPass(driver,test, "Click on Generate Response button");
			
			WebElement element = driver.findElement(By.xpath(LoadProp.recoverdataprop.get("emsresponsecode_xpath")));
			Assert.assertNotNull(element);
			Util.reportPass(driver,test, "Response Code generated and verified successfully.");
			Util.reportPass(driver,test, "User Alias: " +paramDict.get(testId+".user"));
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
	
	public static void device_recovery_keys(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			String downloadPath = "C:\\Users\\pravina\\Downloads";
			String fileName = paramDict.get(testId+".keys");
	
			//Navigate to Population
			Util.clickPopulations(driver, paramDict, test);
			
			//Navigate to Endpoints page
			Util.waitUntilElementVisibility(driver,By.xpath(LoadProp.recoverdataprop.get("endpoint_link_xpath")));
			Util.click(LoadProp.recoverdataprop, "endpoint_link_xpath", test, driver);
			Util.reportPass(driver,test, "Endpoint link clicked!!");
		
			//Click on Activated Shield
			Util.click(LoadProp.recoverdataprop, "click_endpoint_xpath", test, driver);
			Util.reportPass(driver,test, "Click on activated Endpoint");
			
			//Click on Device Recovery Keys
			Util.click(LoadProp.recoverdataprop, "click_device_RecoveryKeys_xpath", test, driver);
			Util.reportPass(driver,test, "Click on Device Recovery Keys button");
			
			//Enter Device Recovery Keys password
			Util.sendKeys(LoadProp.recoverdataprop, "password_box_xpath", paramDict.get(testId+".password"), test, driver);
			Util.reportPass(driver,test, "Enter Device Recovery Keys password");
			
			//Click on Download button
			Util.click(LoadProp.recoverdataprop, "click_download_xpath", test, driver);
			Util.reportPass(driver,test, "Click on Download button");
			Thread.sleep(3000);
								
			
			Util.verifyFileDownloaded(driver, downloadPath, fileName, test);
			
			Util.deleteFileDownloaded(driver, downloadPath, fileName, test);
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}	
	}
	
	public static void recover_bitlocker(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			Util.click(LoadProp.recoverdataprop, "manager_tab_xpath", test, driver);
			Util.reportPass(driver,test, "Clicked on Manager tab.");
			
			Util.clear(LoadProp.recoverdataprop, "bitlocker_hostname_xpath", test, driver);
			Util.sendKeys(LoadProp.recoverdataprop, "bitlocker_hostname_xpath", paramDict.get(testId+".hostname"), test, driver);
			Util.reportPass(driver,test, "Bitlocker Hostname entered.");
			
			Util.click(LoadProp.recoverdataprop, "bitlocker_search_button_xpath", test, driver);
			Util.reportPass(driver,test, "Bitlocker endpoint searched.");
			
			Util.click(LoadProp.recoverdataprop, "select_bitlocker_dropdown_xpath", test, driver);
			Util.click(LoadProp.recoverdataprop, "select_bitlocker_volume_xpath", test, driver);
			Util.reportPass(driver,test, "Bitlocker Volume selected.");
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
	public static void recover_tpm(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			Util.click(LoadProp.recoverdataprop, "manager_tab_xpath", test, driver);
			Util.reportPass(driver,test, "Clicked on Manager tab.");
			
			Util.clear(LoadProp.recoverdataprop, "tpm_hostname_xpath", test, driver);
			Util.sendKeys(LoadProp.recoverdataprop, "tpm_hostname_xpath", paramDict.get(testId+".hostname"), test, driver);
			Util.reportPass(driver,test, "TPM Hostname entered.");
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
	public static void recover_sed_pba(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			Util.click(LoadProp.recoverdataprop, "pba_tab_xpath", test, driver);
			Util.waitUntilElementVisibility(driver,By.xpath(LoadProp.recoverdataprop.get("sed_hostname_xpath")));
			Util.reportPass(driver,test, "Clicked on PBA tab");
			
			Util.clear(LoadProp.recoverdataprop, "sed_hostname_xpath", test, driver);
			Util.sendKeys(LoadProp.recoverdataprop, "sed_hostname_xpath", paramDict.get(testId+".hostname"), test, driver);
			Util.reportPass(driver,test, "SED PBA Hostname entered.");
			
			Util.click(LoadProp.recoverdataprop, "sed_search_button_xpath", test, driver);
			Util.reportPass(driver,test, "SED PBA endpoint searched.");
			
			Util.click(LoadProp.recoverdataprop, "select_sed_pba_dropdown_xpath", test, driver);
			Util.reportPass(driver,test, "Clicked on Dropdown");
			
			Util.click(LoadProp.recoverdataprop, "select_sed_pba_xpath", test, driver);
			Util.reportPass(driver,test, "SED PBA Disk selected.");
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
	public static void recover_pba_user_access(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			Util.click(LoadProp.recoverdataprop, "pba_tab_xpath", test, driver);
			Util.waitUntilElementVisibility(driver,By.xpath(LoadProp.recoverdataprop.get("sed_hostname_xpath")));
			Util.reportPass(driver,test, "Clicked on PBA tab");
			
			Util.sendKeys(LoadProp.recoverdataprop, "response_code_hostname_xpath", paramDict.get(testId+".hostname"), test, driver);
			Util.reportPass(driver,test, "SED Hostname entered");
			
			Util.click(LoadProp.recoverdataprop, "response_code_search_button_xpath", test, driver);
			Util.reportPass(driver,test, "Clicked on Search button");
			
			Util.click(LoadProp.recoverdataprop, "response_code_dropdown_xpath", test, driver);
			Util.reportPass(driver,test, "Clicked on Username Dropdown");
			
			Util.waitUntilElementVisibility(driver,By.xpath(LoadProp.recoverdataprop.get("response_code_username_xpath")));
			Util.click(LoadProp.recoverdataprop, "response_code_username_xpath", test, driver);
			Util.reportPass(driver,test, "SED user selected");
			
			Util.sendKeys(LoadProp.recoverdataprop, "challenge_textbox_1_xpath", paramDict.get(testId+".code_1"), test, driver);
			Util.sendKeys(LoadProp.recoverdataprop, "challenge_textbox_2_xpath", paramDict.get(testId+".code_2"), test, driver);
			Util.sendKeys(LoadProp.recoverdataprop, "challenge_textbox_3_xpath", paramDict.get(testId+".code_3"), test, driver);
			Util.sendKeys(LoadProp.recoverdataprop, "challenge_textbox_4_xpath", paramDict.get(testId+".code_4"), test, driver);
			Util.sendKeys(LoadProp.recoverdataprop, "challenge_textbox_5_xpath", paramDict.get(testId+".code_5"), test, driver);
			Util.sendKeys(LoadProp.recoverdataprop, "challenge_textbox_6_xpath", paramDict.get(testId+".code_6"), test, driver);
			Util.sendKeys(LoadProp.recoverdataprop, "challenge_textbox_7_xpath", paramDict.get(testId+".code_7"), test, driver);
			Util.sendKeys(LoadProp.recoverdataprop, "challenge_textbox_8_xpath", paramDict.get(testId+".code_8"), test, driver);
			Util.reportPass(driver,test, "Challenge code entered");
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
	public static void genarate_pba_user_response(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			Thread.sleep(1000);
			Util.click(LoadProp.recoverdataprop, "generate_reponse_button_xpath", test, driver);
			Util.reportPass(driver,test, "Clicked on Generate Reponse button");
			WebElement element = driver.findElement(By.xpath(LoadProp.recoverdataprop.get("pbaresponsecode_xpath")));
			Assert.assertNotNull(element);
			Util.reportPass(driver,test, "Response Code generated successfully.");
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
	public static void create_recovery_file(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			String fileName = paramDict.get(paramDict.get(testId+".endpoint")+".key");
			
			driver.findElement(By.xpath("//button[@ng-click='"+paramDict.get(testId+".endpoint")+"CreateRecoveryFile()']")).click();
			Util.reportPass(driver,test, "Clicked on Create Recovery File button.");
			Thread.sleep(2000);
			Util.verifyFileDownloaded(driver, LoadProp.getProperty("key_downloadPath"), fileName, test);
			
			Util.deleteFileDownloaded(driver, LoadProp.getProperty("key_downloadPath"), fileName, test);
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
	public static void create_key_package(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			String fileName = paramDict.get(paramDict.get(testId+".endpoint")+".key");
			
			driver.findElement(By.xpath("//button[@ng-click='"+paramDict.get(testId+".endpoint")+"CreateKeyPackage()']")).click();
			Util.reportPass(driver,test, "Clicked on Create key Package button.");
			
			Util.verifyFileDownloaded(driver, LoadProp.getProperty("key_downloadPath"), fileName, test);
			
			Util.deleteFileDownloaded(driver, LoadProp.getProperty("key_downloadPath"), fileName, test);
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
	public static void get_recovery_password(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			driver.findElement(By.xpath("//button[@ng-click='"+paramDict.get(testId+".endpoint")+"GetRecoveryPassword()']")).click();
			Util.reportPass(driver,test, "Clicked on Get Recovery Password button.");
			
			WebElement element = driver.findElement(By.xpath(LoadProp.recoverdataprop.get(paramDict.get(testId+".endpoint")+"_password_xpath")));
			String rec_passwd = element.getText();
			Assert.assertEquals(rec_passwd, paramDict.get(testId+".passwd"));
			Util.reportPass(driver,test, "Recovery password generated and verified.");
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
}