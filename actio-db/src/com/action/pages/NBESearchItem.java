package com.action.pages;

import java.io.IOException;
import java.util.Dictionary;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.properties.mapping.LoadProp;
import com.relevantcodes.extentreports.ExtentTest;
import com.utility.library.Util;

public class NBESearchItem {

	/**
	 * Method for navigate to offers tab.
	 * @param driver Driver instance
	 * @param paramDict parameter dictionary
	 * @param test Extend report instance
	 * @throws IOException exception
	 * @throws InterruptedException exception
	 */
	public static void a_navigate_to_offers_page_buyer_nbe(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test)
		throws IOException, InterruptedException 
		{
				// click on the 'Activity' menu on left
			Util.click(LoadProp.nbeproxibidprop, "activity_btn_xpath", test, driver);
				// click on 'Offers' menu under 'Activity'
			Util.click(LoadProp.nbeproxibidprop, "offers_tab_xpath",test, driver);
			Thread.sleep(2000);
		}
	
	/**
	 * Method to search and select the searched item
	 * @param driver Driver instance
	 * @param paramDict parameter dictionary
	 * @param test Extend report instance
	 * @param filterOptionToSelect name of the option to select from the dropdown
	 * @param itemToSearch item name to search
	 * @throws IOException exception
	 * @throws InterruptedException exception
	 */
	 public static void s_search_select_item_nbe(RemoteWebDriver driver, Dictionary<String, String> paramDict, ExtentTest test, String filterOptionToSelect, String itemIdToSearch)
			throws IOException, InterruptedException 
		{
	/*	 		// select item status from dropdown
		 	Util.selectElementByVisibleText(driver, test, LoadProp.nbeproxibidprop, "filter_dropdown_xpath", filterOptionToSelect);
			   // focus on the search field
			Util.click(LoadProp.nbeproxibidprop, "search_offers_field_xpath",test, driver);
				// enter item to search
			Util.sendKeys(LoadProp.nbeproxibidprop, "search_offers_field_xpath", itemIdToSearch, test, driver);
			Thread.sleep(5000);
	*/		    
		 		// search the item
		 	s_search_item_nbe(driver, paramDict, test, filterOptionToSelect, itemIdToSearch);
		 		// click search
			Util.click(LoadProp.nbeproxibidprop , "searched_item_xpath", test, driver);
			Thread.sleep(5000);
		}
	 
		/**
		 * Method to search and select the searched item
		 * @param driver Driver instance
		 * @param paramDict parameter dictionary
		 * @param test Extend report instance
		 * @param filterOptionToSelect name of the option to select from the dropdown
		 * @param itemToSearch item name to search
		 * @throws IOException exception
		 * @throws InterruptedException exception
		 */
		 public static void s_search_item_nbe(RemoteWebDriver driver, Dictionary<String, String> paramDict, ExtentTest test, String filterOptionToSelect, String itemIdToSearch)
				throws IOException, InterruptedException 
			{
			 		// select item status from dropdown
			 	Util.selectElementByVisibleText(driver, test, LoadProp.nbeproxibidprop, "filter_dropdown_xpath", filterOptionToSelect);
				   // focus on the search field
				Util.click(LoadProp.nbeproxibidprop, "search_offers_field_xpath",test, driver);
					// enter item to search
				Util.sendKeys(LoadProp.nbeproxibidprop, "search_offers_field_xpath", itemIdToSearch, test, driver);
				Thread.sleep(5000);
			}

}
