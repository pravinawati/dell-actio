package com.action.pages;

import java.util.Dictionary;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;

import com.properties.mapping.LoadProp;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.utility.library.Util;

public class LoginPage {
	
	public static void login_to_webui(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			
			String weburl = LoadProp.getProperty("web_weburl");
			String username = paramDict.get(testId+".web_username");
			String password = paramDict.get(testId+".web_password");
			System.out.println(username+" - "+password);
			Util.access_link(weburl, test, driver);
			//Thread.sleep(2000);
			//driver.navigate ().to("javascript:document.getElementById('overridelink').click()");
			//Thread.sleep(2000);
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.loginpageprop.get("usernamefield_xpath")));
			//Util.clear(LoadProp.loginpageprop, "usernamefield_xpath", test, driver);
			Util.sendKeys(LoadProp.loginpageprop, "usernamefield_xpath", username, test, driver);
			test.log(LogStatus.INFO, "Username Entered");
			//Util.clear(LoadProp.loginpageprop, "passwordfield_xpath", test, driver);
			Util.sendKeys(LoadProp.loginpageprop, "passwordfield_xpath", password, test, driver);
			test.log(LogStatus.INFO, "Password Entered");
			Util.click(LoadProp.loginpageprop, "login_btn_xpath", test, driver);
			test.log(LogStatus.INFO, "Clicked on Sign in button");
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.loginpageprop.get("gear_icon_xpath")));
			Util.reportPass(driver,test, "==> Login successfully");
			Thread.sleep(2000);
			
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
	public static void logout_of_webui(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.loginpageprop.get("gear_icon_xpath")));
			Util.click(LoadProp.loginpageprop, "gear_icon_xpath", test, driver);
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.loginpageprop.get("logout_xpath")));
			Util.click(LoadProp.loginpageprop, "logout_xpath", test, driver);
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.loginpageprop.get("login_again_xpath")));
			Util.reportPass(driver,test, "==> Logout successfully");
			Thread.sleep(3000);
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
	public static void cr_login(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			String parentHandle = driver.getWindowHandle();
			driver.switchTo().window(parentHandle);
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.loginpageprop.get("Compliance_Rep_Username_xpath")));
			driver.findElement(By.xpath(LoadProp.loginpageprop.get("Compliance_Rep_Username_xpath"))).clear();
			Util.sendKeys(LoadProp.loginpageprop, "Compliance_Rep_Username_xpath", paramDict.get(testId+".rep_username"), test, driver);
			driver.findElement(By.xpath(LoadProp.loginpageprop.get("Compliance_Rep_Password_xpath"))).clear();
			Util.sendKeys(LoadProp.loginpageprop, "Compliance_Rep_Password_xpath", paramDict.get(testId+".rep_password"), test, driver);
			Util.click(LoadProp.loginpageprop, "crlogin_xpath", test, driver);
			Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.loginpageprop.get("CR_first_page_after_login_xpath")));
			
			WebElement element = driver.findElement(By.xpath(LoadProp.loginpageprop.get("CR_first_page_after_login_xpath")));
			String crafterlogintext = element.getText();
			Assert.assertEquals("Compliance Reporter", crafterlogintext);
			Util.reportPass(driver,test, "Window Title is - " + driver.getTitle());
			Util.reportPass(driver,test, "Logged in Successfully!");
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
}
