package com.action.pages;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Dictionary;

import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import com.properties.mapping.LoadProp;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.utility.library.AssertUtil;
import com.utility.library.Util;

public class NBEActionMethodsMobile {

	static DecimalFormat decimalFormat = new DecimalFormat("#,###.##");

	/**
	 * Method to accept the offer by buyer
	 * @param driver Driver instance
	 * @param paramDict parameter dictionary
	 * @param testId Current test case id
	 * @param test Extent report instance
	 * @throws IOException exception
	 * @throws InterruptedException exception
	 */
	public static void a_accept_offer_buyer_nbe_mobile(RemoteWebDriver driver,	Dictionary<String, String> paramDict, String testId, ExtentTest test)
			throws IOException, InterruptedException 
	{			
		try {
			// fetch the company item id
		String itemIdToBeSearch = paramDict.get(testId+".companyitemid");
			// search and navigate to item info
			// TODO
			// click on Options button
		Util.click(LoadProp.mnbeproxibidprop ,"m_item_options_btn_xpath", test, driver);
			// Click on Accept Offer
		Util.click(LoadProp.mnbeproxibidprop, "m_item_option_accept_offer_btn_xpath", test, driver);
			// confirm Accept Offer
		Util.click(LoadProp.mnbeproxibidprop, "m_item_option_accept_offer_confirm_btn_xpath", test, driver);
		Thread.sleep(5000);

		// Soft assert the Status, Status message, Original Asking Price, Current Offer Price, Transaction History Message and Amount
		SoftAssert sAssert= new SoftAssert();
		s_validate_status(LoadProp.mnbeproxibidprop.get("m_accept_offer_status"), test, driver, sAssert);
		
		// Click on What's this link
		Util.click(LoadProp.mnbeproxibidprop, "m_item_status_info_link_xpath", test, driver);
		
		s_validate_status_message_title (LoadProp.mnbeproxibidprop.get("m_accept_offer_message_title"), test, driver, sAssert);
		s_validate_status_message(LoadProp.mnbeproxibidprop.get("m_accept_offer_message"), test, driver, sAssert);
		
		// Close the Status Info
		Util.click(LoadProp.mnbeproxibidprop, "m_item_info_close_xpath", test, driver);
		
			
		s_validate_original_asking_price("$" + decimalFormat.format(Double.parseDouble(paramDict.get(testId+".listprice"))) + ".00", test, driver, sAssert);
		s_validate_current_offer_price("$" + decimalFormat.format(Double.parseDouble(paramDict.get(testId+".sellercounteramount"))) + ".00", test, driver, sAssert);

			//
		Util.click(LoadProp.mnbeproxibidprop, "m_item_history_menu_xpath", test, driver);
		s_validate_transaction_history_message(LoadProp.mnbeproxibidprop.get("m_accept_history_message") + " " + LoadProp.getProperty("company_name") + "." , test, driver, sAssert);
		s_validate_transaction_history_offer_price("$" + decimalFormat.format(Double.parseDouble(paramDict.get(testId+".sellercounteramount"))) + ".00", test, driver, sAssert);
		
		
		s_validate_item_in_filter_option(driver, paramDict, test, "Active Offers", itemIdToBeSearch, sAssert);
		
		try {
			sAssert.assertAll();
		}catch( Exception e) {
			test.log(LogStatus.FAIL, e.getMessage());
		}
			
	} catch(Exception|AssertionError e){
		// logout of NBE in case of exception for next test to continue.
		// LoginToNBE.s_logout_incase_of_failure_nbe(driver, test);
		Assert.fail("Failed in 'a_accept_offer_buyer_nbe_mobile' action. " + e.getMessage());

	}	
}
	
	/**
	 * Method to counter  the offer by buyer
	 * @param driver Driver instance
	 * @param paramDict parameter dictionary
	 * @param testId Current test case id
	 * @param test Extent report instance
	 * @throws IOException exception
	 * @throws InterruptedException exception
	 */
	public static void a_counter_offer_buyer_nbe_mobile(RemoteWebDriver driver,	Dictionary<String, String> paramDict, String testId, ExtentTest test)
			throws IOException, InterruptedException 
	{			
		try {
			// fetch the company item id
		String itemIdToBeSearch = paramDict.get(testId+".companyitemid");
			// search and navigate to item info
			// TODO
			// click on Options button
		Util.click(LoadProp.mnbeproxibidprop ,"m_item_options_btn_xpath", test, driver);
			// Click on Counter Offer
		Util.click(LoadProp.mnbeproxibidprop, "m_item_option_counter_offer_btn_xpath", test, driver);
			// Enter counter amount
		Util.sendKeys(LoadProp.mnbeproxibidprop, "m_item_option_counter_offer_amount_txt_xpath", paramDict.get(testId+".buyercounteramount"), test, driver);
			// Review Counter Offer
		Util.click(LoadProp.mnbeproxibidprop, "m_item_option_counter_offer_review_btn_xpath", test, driver);
			// confirm Counter Offer
		Util.click(LoadProp.mnbeproxibidprop, "m_item_option_counter_offer_confirm_btn_xpath", test, driver);
		Thread.sleep(5000);

		// Soft assert the Status, Status message, Original Asking Price, Current Offer Price, Transaction History Message and Amount
		SoftAssert sAssert= new SoftAssert();
		s_validate_status(LoadProp.mnbeproxibidprop.get("m_counter_offer_status"), test, driver, sAssert);
		
		// Click on What's this link
		Util.click(LoadProp.mnbeproxibidprop, "m_item_status_info_link_xpath", test, driver);
		
		s_validate_status_message_title (LoadProp.mnbeproxibidprop.get("m_counter_offer_message_title"), test, driver, sAssert);
		s_validate_status_message(LoadProp.mnbeproxibidprop.get("m_counter_offer_message"), test, driver, sAssert);
		
		// Close the Status Info
		Util.click(LoadProp.mnbeproxibidprop, "m_item_info_close_xpath", test, driver);
		
			
		s_validate_original_asking_price("$" + decimalFormat.format(Double.parseDouble(paramDict.get(testId+".listprice"))) + ".00", test, driver, sAssert);
		s_validate_current_offer_price("$" + decimalFormat.format(Double.parseDouble(paramDict.get(testId+".buyercounteramount"))) + ".00", test, driver, sAssert);

			//
		Util.click(LoadProp.mnbeproxibidprop, "m_item_history_menu_xpath", test, driver);
		s_validate_transaction_history_message(LoadProp.mnbeproxibidprop.get("m_counter_history_message") + " " + LoadProp.getProperty("company_name") + "." , test, driver, sAssert);
		s_validate_transaction_history_offer_price("$" + decimalFormat.format(Double.parseDouble(paramDict.get(testId+".buyercounteramount"))) + ".00", test, driver, sAssert);
		
		
		s_validate_item_in_filter_option(driver, paramDict, test, "Active Offers", itemIdToBeSearch, sAssert);
		
		try {
			sAssert.assertAll();
		}catch( Exception e) {
			test.log(LogStatus.FAIL, e.getMessage());
		}
			
	} catch(Exception|AssertionError e){
		// logout of NBE in case of exception for next test to continue.
		// LoginToNBE.s_logout_incase_of_failure_nbe(driver, test);
		Assert.fail("Failed in 'a_accept_offer_buyer_nbe_mobile' action. " + e.getMessage());

	}	
}	
	
	/**
	 * Method to decline the offer by buyer
	 * @param driver Driver instance
	 * @param paramDict parameter dictionary
	 * @param testId Current test case id
	 * @param test Extent report instance
	 * @throws IOException exception
	 * @throws InterruptedException exception
	 */
	public static void a_decline_offer_buyer_nbe_mobile(RemoteWebDriver driver,	Dictionary<String, String> paramDict, String testId, ExtentTest test)
			throws IOException, InterruptedException 
	{			
		try {
			// fetch the company item id
		String itemIdToBeSearch = paramDict.get(testId+".companyitemid");
			// search and navigate to item info
			// TODO
			// click on Options button
		Util.click(LoadProp.mnbeproxibidprop ,"m_item_options_btn_xpath", test, driver);
			// Click on Decline Offer
		Util.click(LoadProp.mnbeproxibidprop, "m_item_option_decline_offer_btn_xpath", test, driver);
			// Enter Decline optional message
		Util.sendKeys(LoadProp.mnbeproxibidprop, "m_item_option_decline_offer_message_txt_xpath", paramDict.get(testId+".declinemessage"), test, driver);
			// confirm Decline Offer
		Util.click(LoadProp.mnbeproxibidprop, "m_item_option_decline_offer_confirm_btn_xpath", test, driver);
		Thread.sleep(5000);

		// Soft assert the Status, Status message, Original Asking Price, Current Offer Price, Transaction History Message and Amount
		SoftAssert sAssert= new SoftAssert();
		s_validate_status(LoadProp.mnbeproxibidprop.get("m_decline_offer_status"), test, driver, sAssert);
		
		// Click on What's this link
		Util.click(LoadProp.mnbeproxibidprop, "m_item_status_info_link_xpath", test, driver);
		
		s_validate_status_message_title (LoadProp.mnbeproxibidprop.get("m_decline_offer_message_title"), test, driver, sAssert);
		s_validate_status_message(LoadProp.mnbeproxibidprop.get("m_decline_offer_message"), test, driver, sAssert);
		
		// Close the Status Info
		Util.click(LoadProp.mnbeproxibidprop, "m_item_info_close_xpath", test, driver);
		
			
		s_validate_original_asking_price("$" + decimalFormat.format(Double.parseDouble(paramDict.get(testId+".listprice"))) + ".00", test, driver, sAssert);
		s_validate_current_offer_price("$" + decimalFormat.format(Double.parseDouble(paramDict.get(testId+".sellercounteramount"))) + ".00", test, driver, sAssert);

			//
		Util.click(LoadProp.mnbeproxibidprop, "m_item_history_menu_xpath", test, driver);
		s_validate_transaction_history_message(LoadProp.mnbeproxibidprop.get("m_decline_history_message") + " " + LoadProp.getProperty("company_name") + "." , test, driver, sAssert);
		s_validate_transaction_history_offer_price("$" + decimalFormat.format(Double.parseDouble(paramDict.get(testId+".sellercounteramount"))) + ".00", test, driver, sAssert);
		
		
		s_validate_item_in_filter_option(driver, paramDict, test, "Declined Offers", itemIdToBeSearch, sAssert);
		
		try {
			sAssert.assertAll();
		}catch( Exception e) {
			test.log(LogStatus.FAIL, e.getMessage());
		}
			
	} catch(Exception|AssertionError e){
		// logout of NBE in case of exception for next test to continue.
		// LoginToNBE.s_logout_incase_of_failure_nbe(driver, test);
		Assert.fail("Failed in 'a_accept_offer_buyer_nbe_mobile' action. " + e.getMessage());

	}	
}	
	
	
	
	/*************************************************************/
	
	public static void s_validate_status(String expectedStatus, ExtentTest test, RemoteWebDriver driver, SoftAssert sAssert ) throws IOException{
		test.log(LogStatus.INFO, "Asserting Status");
		String actualStatus = Util.getText(LoadProp.mnbeproxibidprop , "m_item_status_xpath", test, driver);
		AssertUtil.performSoftAssertEquals(expectedStatus, actualStatus, "Status is incorrect.", driver, test, sAssert);
	}
	
	/**
	 * Method to soft assert the Action status message
	 * @param expectedStatusMessage status message
	 * @param test ExtentTest instance
	 * @param driver RemoteWebDriver instance
	 * @param sAssert SoftAssert instance
	 * @throws IOException exception
	 */
	private static void s_validate_status_message(String expectedStatusMessage, ExtentTest test, RemoteWebDriver driver, SoftAssert sAssert)  throws IOException{
		test.log(LogStatus.INFO, "Asserting Status Message");
		String actualStatusMessage = Util.getText(LoadProp.mnbeproxibidprop, "m_item_info_status_message_xpath", test, driver);
		AssertUtil.performSoftAssertEquals(expectedStatusMessage, actualStatusMessage, "Status message is incorrect.", driver, test, sAssert);
	}

	private static void s_validate_status_message_title (String expectedStatusMessage, ExtentTest test, RemoteWebDriver driver, SoftAssert sAssert)  throws IOException{
		test.log(LogStatus.INFO, "Asserting Status Message Title");
		String actualStatusMessage = Util.getText(LoadProp.mnbeproxibidprop, "m_item_info_status_title_xpath", test, driver);
		AssertUtil.performSoftAssertEquals(expectedStatusMessage, actualStatusMessage, "Status message title is incorrect.", driver, test, sAssert);
	}

	/**
	 * Method to soft assert the current offer price
	 * @param expectedCurrentOfferPrice current offer price
	 * @param test ExtentTest instance
	 * @param driver RemoteWebDriver instance
	 * @param sAssert SoftAssert instance
	 * @throws IOException exception
	 */
	private static void s_validate_current_offer_price(String expectedCurrentOfferPrice, ExtentTest test, RemoteWebDriver driver, SoftAssert sAssert)  throws IOException{
		test.log(LogStatus.INFO, "Asserting Current Offer price");
		String actualCurrentOfferPrice = Util.getText(LoadProp.mnbeproxibidprop , "m_current_offer_price_xpath", test, driver);
		AssertUtil.performSoftAssertEquals(expectedCurrentOfferPrice, actualCurrentOfferPrice, "Currnet Offer Price is incorrect.", driver, test, sAssert);
	}

	/**
	 * Method to soft assert the original asking price
	 * @param expectedOriginalAskingPrice original asking price
	 * @param test ExtentTest instance
	 * @param driver RemoteWebDriver instance
	 * @param sAssert SoftAssert instance
	 * @throws IOException exception
	 */
	private static void s_validate_original_asking_price(String expectedOriginalAskingPrice, ExtentTest test, RemoteWebDriver driver, SoftAssert sAssert)  throws IOException{
		test.log(LogStatus.INFO, "Asserting Original Asking price");
		String actualOriginalAskingPrice = Util.getText(LoadProp.mnbeproxibidprop , "m_original_asking_price_xpath", test, driver);
		AssertUtil.performSoftAssertEquals(expectedOriginalAskingPrice, actualOriginalAskingPrice, "Original Asking Price is incorrect.", driver, test, sAssert);
	}

	/**
	 * Method to soft assert the Offer price in Transaction history
	 * @param expectedTransactionHistoryOfferPrice transaction history price
	 * @param test ExtentTest instance
	 * @param driver RemoteWebDriver instance
	 * @param sAssert SoftAssert instance
	 * @throws IOException exception
	 */
	private static void s_validate_transaction_history_offer_price( String expectedTransactionHistoryOfferPrice, ExtentTest test, RemoteWebDriver driver, SoftAssert sAssert)  throws IOException{
		test.log(LogStatus.INFO, "Asserting Transaction history offer price");
		String actualTransactionHistoryOfferPrice =  Util.getText(LoadProp.mnbeproxibidprop , "m_item_transaction_history_price_xpath", test, driver);
		AssertUtil.performSoftAssertEquals(expectedTransactionHistoryOfferPrice, actualTransactionHistoryOfferPrice, "Offer Price in Transaction History is incorrect.", driver, test, sAssert);
	}
	
	/**
	 * Method to soft assert the Transaction history message
	 * @param expectedTransactionHistoryMessage transaction history message
	 * @param test ExtentTest instance
	 * @param driver RemoteWebDriver instance
	 * @param sAssert SoftAssert instance
	 * @throws IOException exception
	 */
	private static void s_validate_transaction_history_message(String expectedTransactionHistoryMessage, ExtentTest test, RemoteWebDriver driver, SoftAssert sAssert)  throws IOException{
		test.log(LogStatus.INFO, "Asserting Transaction history message");
		String actualTransactionHistoryMessage =  Util.getText(LoadProp.mnbeproxibidprop , "m_item_transaction_history_message_xpath", test, driver);
		AssertUtil.performSoftAssertEquals(expectedTransactionHistoryMessage, actualTransactionHistoryMessage, "Transaction History Message is incorrect.", driver, test, sAssert);
	}
	
	/**
	 * Method to validate the action performed on item from filter drop-down.
	 * @param driver Driver instance
	 * @param paramDict parameter dictionary
	 * @param test Extent report instance
	 * @param filter filter to pass xpath of element
	 * @param itemToBeSearch item name
	 * @throws Exception 
	 */
	public static void s_validate_item_in_filter_option (RemoteWebDriver driver, Dictionary<String, String> paramDict, ExtentTest test, String filter, String itemToBeSearch, SoftAssert  sAssert )
			throws Exception {

		test.log(LogStatus.INFO, "Asserting if the item is displayed under currect filter");
		Util.click(LoadProp.mnbeproxibidprop, "m_menu_left_back_xpath", test, driver);
	    Thread.sleep(2000);
	    LoginNavigateSearchNBEMobile.s_refine_search_item_mobile(driver, filter, itemToBeSearch, test);
		String actualItemId = Util.getText(LoadProp.mnbeproxibidprop , "m_item_id_on_search_xpath", test, driver);
		AssertUtil.performSoftAssertEquals("Item ID: "+ itemToBeSearch, actualItemId, "Item Id : " + itemToBeSearch + " is not found under filter : " + filter, driver, test, sAssert);
	}
}
