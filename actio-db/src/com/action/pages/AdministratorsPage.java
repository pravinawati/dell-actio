package com.action.pages;

import java.util.Dictionary;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;

import com.properties.mapping.LoadProp;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.utility.library.Util;

public class AdministratorsPage {
	
   private static final String SearchforUsersGroupsandEndpoint = null;

	//Administrators Page : single user
	/* prop xls file
	* 
	* addusergroup | user | kp1
	* addusergroup | domain | DDPS.LOCAL 
	* addusergroup | password | 1
	* 
	* 
	*/
	
	public static void add_user_administrators_rights_single_user(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			//Administrator Page
			String domain_name = paramDict.get("addusergroup.domain");
			String user_name = paramDict.get("addusergroup.user");
			String acc_user = user_name + " (" + user_name + "@" + domain_name +")";
			
			test.log(LogStatus.INFO, "ACC User Name: " + acc_user);
			
			//Assert User Name
			List<WebElement> page = driver.findElements(By.xpath("//div[@data-role='pager']/ul/li"));
			String page_size = String.valueOf(page.size());
			int ac_page_size = Integer.parseInt(page_size) - 1;
			
			for(int i=1; i<=ac_page_size; i++){
				int optUser = 1;
				String find_user = "false";
				test.log(LogStatus.INFO, "Find User: " + find_user);
				
				for(int j=1; j<=25; j++){
					String row = driver.findElement(By.xpath("//tbody[@role='rowgroup']/tr["+ optUser +"]/td[3]")).getText();
					test.log(LogStatus.INFO, "Row: " + row);
					//Find User
					if(row.contentEquals(acc_user)){
						find_user = "true";
						test.log(LogStatus.INFO, "Find User: " + find_user);
						
						//Give Admin Rights
						driver.findElement(By.xpath("//tbody[@role='rowgroup']/tr["+ optUser +"]/td[3]/a")).click();
						//userDetailsPage
						Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.adminsprop.get("userDetailsPage_xpath")));
						String user_title = driver.findElement(By.xpath("//div[@class='page-title']")).getText();
						String acc_user_title = "User Detail for: "+ acc_user;
						test.log(LogStatus.INFO, "User Page Title: " + user_title);
						test.log(LogStatus.INFO, "ACC User Page Title: " + acc_user_title);
						
						if(acc_user_title.contentEquals(user_title)){
							test.log(LogStatus.INFO, "Successfully open User Details page..");
							Util.click(LoadProp.adminsprop, "usersAdminTab_xpath", test, driver);
							//usersAdminTabView
							Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.adminsprop.get("usersAdminTabView_xpath")));
							
							//Check user admin all check box			
							for(int admin=1; admin<=10; admin++){
								WebElement admin_user_checkbox = driver.findElement(By.xpath("//div[@class='col-md-4  col-sm-6']/div["+ admin +"]/div/label/input[@type='checkbox']"));
								
								if(admin_user_checkbox.isSelected()){
									driver.findElement(By.xpath("//div[@class='col-md-4  col-sm-6']/div["+ admin +"]/div/label/input[@type='checkbox']")).click();
									test.log(LogStatus.INFO, "Admin Role["+ admin +"]: Unchecked");
								}else{
									test.log(LogStatus.INFO, "Admin Role["+ admin +"]: false");
								}
							}
							
							//give Admin Role
							String admin_role = paramDict.get("admin_role.role");
							test.log(LogStatus.INFO, "Admin Role: "+ admin_role);
							driver.findElement(By.xpath("//div[@class='col-md-4  col-sm-6']/div["+ admin_role +"]/div/label/input[@type='checkbox']")).click();
							
							//Save Button: userAdminSaveBtn
							Util.click(LoadProp.adminsprop, "userAdminSaveBtn_xpath", test, driver);
							Util.waitUntilElementVisibility(driver, By.xpath("//div[@role='dialog']"));
							String addAdminDialog = driver.findElement(By.xpath(LoadProp.adminsprop.get("ddpDialogTitle_xpath"))).getText();
							test.log(LogStatus.INFO, addAdminDialog);
							driver.findElement(By.xpath("//button[starts-with(@id,'resolveButton')]")).click();
							
							//Alert Message
							String add_usergroup_alert = driver.findElement(By.xpath("//div[@id='notificationPanel']")).getText();
							test.log(LogStatus.INFO, add_usergroup_alert);
							
						}else{
							test.log(LogStatus.INFO, "Failed to open User Details page ..");
							Assert.assertEquals("", "none");
						}
						
						break;
					}
					optUser++;
					}
				
				//Find User
				if(find_user == "true"){
					Util.reportPass(driver,test, "Find User: " + find_user);
					break;
				}else{
					Util.reportPass(driver,test, "Find User: " + find_user);
					test.log(LogStatus.INFO, "Pagination Next Page Users"+ i);
					Util.click(LoadProp.dashboardprop, "paginationNextPageUsers", test, driver);
				}
			}
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}		
	}
	
	//Get Admin Role: Help File
	public static void get_single_admin_role_as_per_help_file(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			test.log(LogStatus.INFO, "Get admin role as per Help File..");
			String admin_role = paramDict.get("admin_role.role");
			test.log(LogStatus.INFO, "Admin Role: "+ admin_role);
			
			//Search for Users, Groups, and Endpoints
			String SearchforUsersGroupsandEndpoint = get_Search_for_Users_Groups_and_Endpoint(driver, paramDict, testId, test);
			Util.reportPass(driver,test, "Searched for Users, Groups, and Endpoints: "+ SearchforUsersGroupsandEndpoint);
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
		}
	}
	
	
	//Search for Users, Groups, and Endpoints
	public static String get_Search_for_Users_Groups_and_Endpoint(RemoteWebDriver driver, Dictionary<String, String> paramDict, String testId, ExtentTest test) {
		try{
			WebElement users = driver.findElement(By.xpath(LoadProp.adminsprop.get("userNav_xpath")));
			WebElement usersGroup = driver.findElement(By.xpath(LoadProp.adminsprop.get("userGroupNav_xpath")));
			WebElement endpointGroup = driver.findElement(By.xpath(LoadProp.adminsprop.get("endpointGroupNav_xpath")));
			
			Util.clickPopulations(driver, paramDict, test);
			
			//Users
			if(users.isDisplayed()){
				test.log(LogStatus.INFO, "Successfully Search for Users");
				Util.click(LoadProp.adminsprop, "userNav_xpath", test, driver);
				Util.waitUntilElementVisibility(driver, By.xpath(LoadProp.adminsprop.get("userPage_xpath")));
				String users_title = driver.findElement(By.xpath("//div[@class='page-title']")).getText();
				String acc_user_title = "Users";
				
				if(acc_user_title.contentEquals(users_title)){
					Util.reportPass(driver,test, "Successfully open Users page..");
					return "true";
				}else{
					Util.reportPass(driver,test, "Failed to open Users page");
					return "false";
				}
			}else{
				Util.reportPass(driver,test, "Failed to Search for Users");
				return "false";
			}
		} catch(Exception|AssertionError e){
			Assert.fail(e.getMessage());
			return "false";
		}
		
	}
	
	
	
}