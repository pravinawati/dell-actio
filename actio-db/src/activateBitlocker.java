
import java.io.File;
import java.io.PrintStream;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.DefaultLogger;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.ProjectHelper;

public class activateBitlocker
{
  public activateBitlocker() {}
  
  public static void main(String[] args) throws java.io.IOException, InterruptedException
  {
    long start = System.currentTimeMillis();
    
    NumberFormat formatter = new DecimalFormat("#0.00000");
    
    String current = new File(".").getCanonicalPath();
    
    for (int i = 0; i < 15; i++)
    {
      File buildFile = new File(current + "//bitlocker.xml");
      

      Project p = new Project();
      p.setUserProperty("ant.file", buildFile.getAbsolutePath());
      DefaultLogger consoleLogger = new DefaultLogger();
      consoleLogger.setErrorPrintStream(System.err);
      consoleLogger.setOutputPrintStream(System.out);
      consoleLogger.setMessageOutputLevel(2);
      p.addBuildListener(consoleLogger);
      System.out.println(current);
      try
      {
        p.fireBuildStarted();
        p.init();
        ProjectHelper helper = ProjectHelper.getProjectHelper();
        p.addReference("ant.projectHelper", helper);
        helper.parse(p, buildFile);
        p.executeTarget(p.getDefaultTarget());
        p.fireBuildFinished(null);
      }
      catch (BuildException e) {
        p.fireBuildFinished(e);
      }
      Thread.sleep(60000L);
    }
    long end = System.currentTimeMillis();
    System.out.print("Execution time of bitlocker Simulator is " + formatter.format((end - start) / 1000.0D) + " seconds");
  }
}